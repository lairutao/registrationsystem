package com.registration.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.activity.MainActivity;
import com.registration.activity.ScanCodeActivity;
import com.registration.adapter.CheckInAdapter;
import com.registration.base.BaseFragment;
import com.registration.base.MyApplication;
import com.registration.base.StickHeaderDecoration;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.CheckInListMoudle;
import com.registration.moudle.DataPackMoudle;
import com.registration.moudle.HistoryEntity;
import com.registration.moudle.RemarkEntity;
import com.registration.net.ApiResponse;
import com.registration.net.CommonCallBack;
import com.registration.net.EncryptRequest;
import com.registration.net.IdaParams;
import com.registration.net.ServiceGenerager;
import com.registration.util.JsonHelper;
import com.registration.util.CollectionStream;
import com.registration.util.StringCompressUtils;
import com.registration.util.UrlUtil;
import com.registration.view.ClearEditText;
import com.registration.view.TicketInfoDialog;
import com.registration.base.BaseViewModel;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.registration.constants.Constants.SCAN_CODE;

/*
 * @创建者 huangxy
 * @创建时间  2021/1/20 15:08
 * @描述      ${TODO}   报到列表页
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class CheckInListFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.common_toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_search)
    ClearEditText searchEditText;
    @BindView(R.id.normal_view)
    SmartRefreshLayout mSmartRefresh;
    @BindView(R.id.user_info_recyclerview)
    RecyclerView mRecyclerview;

    private TextWatcher textWatcher;
    private InputMethodManager inputMethodManager;
    private TicketInfoDialog mTicketInfoDialog = new TicketInfoDialog();
    private List<CheckInListMoudle> wholeCheckInList = new ArrayList<>();   //全部数据
    private List<CheckInListMoudle> mCheckInList = new ArrayList<>();   //列表数据
    private CheckInAdapter mAdapter;
    private String randomCode;
    private String keyword;

    @Override
    protected int contentViewID() {
        return R.layout.fragment_check_in_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        EventBus.getDefault().register(this);

        //设置返回按钮监听
        setNavigationOnClickListener(this);

        //设置删除按钮监听
        setIconOnClickListener(this);

        //初始化列表
        initRecyclerview();

        //事件监听
        initListener();

        //初始化数据
        initData();
    }

    public void setNavigationOnClickListener(@Nullable View.OnClickListener l) {
        toolbar.setNavigationOnClickListener(l);
    }

    public void setIconOnClickListener(@Nullable View.OnClickListener l) {
        try {
            //用反射给toolbar控件里的mLogo设置点击事件
            Field field = toolbar.getClass().getDeclaredField("mLogoView");
            field.setAccessible(true);
            ImageView imageView = (ImageView) field.get(toolbar);
            imageView.setOnClickListener(l);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initRecyclerview() {

        mAdapter = new CheckInAdapter(mCheckInList);
        mRecyclerview.addItemDecoration(new StickHeaderDecoration(getContext()));
        mRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerview.setAdapter(mAdapter);
        //用户信息item 点击
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                hideKeyboard();
                EventBus.getDefault().post(adapter.getData().get(position));
                //BaseViewModel viewModel = new ViewModelProvider(getActivity(), new ViewModelProvider.NewInstanceFactory()).get(BaseViewModel.class);
                //viewModel.getLiveData().setValue(adapter.getData().get(position));
                ((CheckInFragment) getParentFragment()).nextPage();
            }
        });
    }

    public void initListener() {
        //搜索输入监听
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (MyApplication.getInstance().hasDataPick()) {
                    keyword = s.toString().trim();
                    getCheckInList();
                }
            }
        };
        searchEditText.addTextChangedListener(textWatcher);
//        searchEditText.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_ENTER) {
//                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                        if (MyApplication.getInstance().hasDataPick()) {
//                            keyword = searchEditText.getText().toString().trim();
//                            getCheckInList();
//                        } else {
//                            //mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.search_hint));
//                        }
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });
        //下拉刷新
        mSmartRefresh.setEnableRefresh(true);
        mSmartRefresh.setEnableLoadMore(false);
        mSmartRefresh.setEnableAutoLoadMore(false);
        mSmartRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mSmartRefresh.finishRefresh();
                hideKeyboard(); //收起键盘
                initData(); //刷新数据
            }
        });
        //上拉加载更多
//        mSmartRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
//                ToastUtils.showShort(getString(R.string.no_more));
//                mSmartRefresh.finishLoadMore();
//            }
//        });
    }

    private void initData() {
        //无本地数据则先同步后台数据
        if (MyApplication.getInstance().hasDataPick()) {
            //若存在扫码结果，则优先执行扫码查找
            if (!TextUtils.isEmpty(randomCode)) {
                searchCheckInList(false);
            } else {
                getCheckInList();
            }
        } else {
            syncDataPack(null);
        }
    }

    private void getCheckInList() {

        //showLoading();

        randomCode = null;

        //页面数据不存在
        if (wholeCheckInList == null || wholeCheckInList.size() == 0) {
            AppDatabase.getInstance(getActivity()).getDataPackDao().getAllCheckInList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<CheckInListMoudle>>() {
                    @Override
                    public void accept(List<CheckInListMoudle> list) throws Exception {
                        wholeCheckInList.clear();
                        if (list != null && list.size() > 0) {
                            wholeCheckInList.addAll(list);
                        }
                        //刷新界面
                        updateDataSet(wholeCheckInList);
//                        if (wholeCheckInList.size() == 0) {
//                            ToastUtils.showLong(R.string.no_data_hint);
//                        }
                    }
                });
        } else

        //搜索关键词
        if (!TextUtils.isEmpty(keyword)) {
            Observable.create((ObservableOnSubscribe) emitter -> { //模糊查询
                List<CheckInListMoudle> result = new CollectionStream<>(wholeCheckInList)
                    .filter(moudle -> moudle.getMobile().indexOf(keyword) > -1 ||
                        moudle.getDocumentNumber().indexOf(keyword) > -1 ||
                        moudle.getChineseName().indexOf(keyword) > -1 ||
                        moudle.getEmail().indexOf(keyword) > -1)
                    .collectors2List();
                emitter.onNext(result);
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List>() {
                @Override
                public void accept(List list) throws Exception {
                    updateDataSet(list);
                }
            });

        } else {
            updateDataSet(wholeCheckInList);
        }
    }

    private void updateDataSet(List<CheckInListMoudle> list) {
        mCheckInList.clear();
        if (list != null && list.size() > 0) {
            mCheckInList.addAll(list);
        }
        mAdapter.notifyDataSetChanged();
        mSmartRefresh.finishRefresh();
        hideLoading();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(HistoryEntity entity) {
        if (entity != null && entity.isSuccess() && entity.getCiTicketId() > 0) { //签到成功
            //刷新列表数据
            if (mCheckInList != null && mCheckInList.size() > 0) {
                if (entity.isTeamCheckIn()) { //团队报道
                    for (int i=0; i<mCheckInList.size(); i++) {
                        if (!mCheckInList.get(i).isTeam()) continue;
                        if ((mCheckInList.get(i).getGroupId() == entity.getSubId() && mCheckInList.get(i).getGroupId() > 0) ||
                            mCheckInList.get(i).getLeaderTicketId() == entity.getCiTicketId()) { //领队
                            mCheckInList.get(i).setLeaderCheckInFlag(1);
                            if (mCheckInList.get(i).isGroup()) { //团员
                                mCheckInList.get(i).setTicketCheckInFlag(1);
                            }
                        }
                    }
                } else { //个人报道
                    for (int i=0; i<mCheckInList.size(); i++) {
                        if (mCheckInList.get(i).isTeam()) continue;
                        if (mCheckInList.get(i).getTicketId() == entity.getCiTicketId()) {
                            mCheckInList.get(i).setTicketCheckInFlag(1);
                            break;
                        }
                    }
                }
                //刷新界面
                mAdapter.notifyDataSetChanged();
            }
            //刷新全部数据
            updateCheckInList();
        }
    }

    private void updateCheckInList() {
        AppDatabase.getInstance(getActivity()).getDataPackDao().getAllCheckInList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<CheckInListMoudle>>() {
                @Override
                public void accept(List<CheckInListMoudle> list) throws Exception {
                    wholeCheckInList.clear();
                    if (list != null && list.size() > 0) {
                        wholeCheckInList.addAll(list);
                    }
                }
            });
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        if (v instanceof AppCompatImageButton) { //同步按钮
            showLoading();
            clearEditText();
            if (MyApplication.getInstance().hasDataPick()) {
                //是否有未同步的签到数据
                List<HistoryEntity> list1 = ((MainActivity) getActivity()).getSyncHistory();
                if (list1 != null && list1.size() > 0) {
                    AppDatabase.getInstance(getActivity()).getTicketDao().getSyncRemarkInfo()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<List<RemarkEntity>>() {
                            @Override
                            public void accept(List<RemarkEntity> list2) throws Exception {
                                //上传需要同步的签到数据
                                EncryptRequest params = IdaParams.getDataPackParams(list1, list2);
                                syncDataPack(params);
                            }
                        });
                    return;
                }
            }
            syncDataPack(null);
        } else
        if (v instanceof AppCompatImageView) { //删除按钮
            mTicketInfoDialog.showConfirm(getActivity(), getString(R.string.registrations), "确定删除所有本地数据吗？\n*包括未同步的签到历史数据！", new TicketInfoDialog.actionListener() {
                @Override
                public void onConfirm() {
                    deleteData();
                }
            });
        }
    }

    @OnClick({R.id.toolbar_iv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_iv_right:
                hideKeyboard();
                initScan();
                break;
            default:
                break;
        }
    }

    //同步数据
    private void syncDataPack(EncryptRequest params) {

        showLoading();

        if (params == null) {
            List list = new ArrayList();
            params = IdaParams.getDataPackParams(list, list);
        }

        //初始化状态
        keyword = null;
        randomCode = null;
        ServiceGenerager.getInstance()
            .createService().getDataPack(params)
            .enqueue(new CommonCallBack<ApiResponse<String>>() {
                @Override
                public void onSuc(String code, String errMsg, Response<ApiResponse<String>> response) {
                    try {
                        String json = StringCompressUtils.unGzip(response.body().data);
                        DataPackMoudle dataPack = JsonHelper.getGson().fromJson(json, DataPackMoudle.class);
                        //合并数据库
                        AppDatabase.getInstance(getActivity()).getDataPackDao().mergeData(dataPack);
                        //更新数据库
                        AppDatabase.getInstance(getActivity()).getDataPackDao().syncDataPack();
                        //通知清除红点
                        EventBus.getDefault().post(new HistoryEntity());
                        //更新同步时间
                        Hawk.put(Constants.SYNCTIME, dataPack.syncTime);
                        ToastUtils.showShort(R.string.sync_success);
                    } catch (Exception e) {
                        ToastUtils.showShort(R.string.sync_fail + e.getMessage());
                        e.printStackTrace();
                    }
                    //刷新数据
                    getCheckInList();
                    //隐藏加载框
                    //hideLoading();
                }

                @Override
                public void onFail(String code, String errMsg, Response<ApiResponse<String>> response) {
                    hideLoading(); mTicketInfoDialog.registCodeHint(getActivity(), errMsg);
                }
            });
    }

    //删除数据
    private void deleteData() {

        if (Hawk.contains(Constants.SYNCTIME)) {
            Hawk.delete(Constants.SYNCTIME);
        }

        //清空数据库表
        AppDatabase.getInstance(getActivity()).getDataPackDao().clearData();

        //通知清除红点
        EventBus.getDefault().post(new HistoryEntity());
        ToastUtils.showLong(R.string.delete_success);

        clearEditText();
        mCheckInList.clear();
        wholeCheckInList.clear();
        mAdapter.notifyDataSetChanged();
        randomCode = null;
        keyword = null;
    }

    //扫码报到
    private void initScan() {
        if (MyApplication.getInstance().hasDataPick()) {
            startActivityForResult(new Intent(getActivity(), ScanCodeActivity.class), SCAN_CODE);
        } else {
            mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.scan_hint));
        }
    }

    private void searchCheckInList(final boolean isScan) {

        showLoading();
        keyword = null;
        clearEditText();
        if (wholeCheckInList != null && wholeCheckInList.size() > 0) {
            Observable.create((ObservableOnSubscribe) emitter -> { //匹配查找
                List<CheckInListMoudle> result = new CollectionStream<>(wholeCheckInList)
                        .filter(moudle -> randomCode.equals(moudle.getRandomCode()))
                        .collectors2List();
                emitter.onNext(result);
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List>() {
                @Override
                public void accept(List list) throws Exception {
                    updateDataSet(list);
                    scanResult(isScan);
                }
            });
        } else {
            AppDatabase.getInstance(getActivity()).getDataPackDao().queryAllCheckInList(randomCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<CheckInListMoudle>>() {
                    @Override
                    public void accept(List<CheckInListMoudle> list) throws Exception {
                        updateDataSet(list);
                        scanResult(isScan);
                    }
                });
        }
    }

    //扫码结果
    private void scanResult(boolean isScan) {
        if (mCheckInList != null && mCheckInList.size() > 0) {
            if (isScan && mCheckInList.size() == 1) { //如果只有一条记录，直接跳转到详情页
                EventBus.getDefault().post(mCheckInList.get(0));
                ((CheckInFragment) getParentFragment()).nextPage();
            }
        } else {
            mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.sacn_no_result) + randomCode);
        }
    }

    //隐藏软键盘
    private void hideKeyboard() {
        if (inputMethodManager == null) {
            inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        if (searchEditText != null && searchEditText.getWindowToken() != null) {
            inputMethodManager.hideSoftInputFromWindow(searchEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            searchEditText.clearFocus();
        }
    }

    //清空输入框
    private void clearEditText() {
        if (searchEditText != null) {
            searchEditText.removeTextChangedListener(textWatcher);
            searchEditText.setText(null);
            searchEditText.addTextChangedListener(textWatcher);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == Activity.RESULT_OK && requestCode == SCAN_CODE) {
            String result = intent.getStringExtra(CodeUtils.RESULT_STRING);
            //result = "https://app.idaonline.cn/#/appdown?code=OwrowboblhYz";
            if (!TextUtils.isEmpty(result) && result.contains("app.idaonline.cn")) {
                randomCode = UrlUtil.getValueByName(result, "code");
                if (!TextUtils.isEmpty(randomCode)) {
                    searchCheckInList(true);
                    return;
                }
            }
            ToastUtils.showLong(R.string.code_fail_hint);
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
}
