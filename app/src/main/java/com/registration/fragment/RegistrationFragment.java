package com.registration.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.activity.SecondActivity;
import com.registration.activity.TeamRegistionctivity;
import com.registration.adapter.RegistrationAdapter;
import com.registration.base.BaseFragment;
import com.registration.base.MyApplication;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.RecordsBean;
import com.registration.moudle.SignInMoudle;
import com.registration.moudle.TicketInfo;
import com.registration.moudle.TransferPictureMoudle;
import com.registration.net.ApiResponse;
import com.registration.net.CommonCallBack;
import com.registration.net.IdaParams;
import com.registration.net.ServiceGenerager;
import com.registration.util.BitmapUtils;
import com.registration.util.GsonUtil;
import com.registration.util.StringUtils;
import com.registration.view.ClearEditText;
import com.registration.view.TicketInfoDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.registration.constants.Constants.TAKE_PHOTO;
import static com.registration.util.BitmapUtils.getAlbumStorageDir;
import static me.yokeyword.fragmentation.ISupportFragment.RESULT_OK;

/*
 * @创建者 lai
 * @创建时间  2019/6/28 20:08
 * @描述      ${TODO}         报到
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class RegistrationFragment extends BaseFragment {
    @BindView(R.id.iv_refresh)
    ImageView     mIRefresh;
    @BindView(R.id.iv_delete)
    ImageView     mIDelete;
    @BindView(R.id.iv_scan)
    ImageView     mIvScan;
    @BindView(R.id.et_search)
    ClearEditText mEtSearch;
    @BindView(R.id.user_info_recyclerview)
    RecyclerView  mRecyclerview;
    Unbinder unbinder;
    @BindView(R.id.normal_view)
    SmartRefreshLayout mSmartRefresh;
    @BindView(R.id.tv_registration_number)
    TextView           mTvRegistrationNumber;
    @BindView(R.id.tv_before_the_number)
    TextView           mTvBeforeTheNumber;
    @BindView(R.id.tv_headcount)
    TextView           mTvHeadcount;
    @BindView(R.id.tv_checkpoint_name)
    TextView           mTvCheckpointName;
    @BindView(R.id.tv_checkpoint_name_count)
    TextView           mTvCheckpointNameCount;
    private List<TicketInfo>    mTicketInfos;
    private RegistrationAdapter mAdapter;
    private List<TicketInfo>    mTicketInfoList;
    private TicketInfoDialog    mTicketInfoDialog;
    private TicketInfo          mTicketInfo1;
    private Uri                 mPhotoUri;
    private String              mPhotoName;
    private List<TicketInfo>    mCheckPointTicketList;
    private List<TicketInfo>    mRegistrationList;
    private List<TicketInfo> mAttendanceList;

    @Override
    protected int contentViewID() {
        return R.layout.fragment_registration;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        //数据集合
        mTicketInfos = new ArrayList<>();
        //所有门票信息
        mTicketInfoList = new ArrayList<>();
        //报到台信息
        mCheckPointTicketList = new ArrayList<>();
        //团队参会人数
        mAttendanceList = new ArrayList<>();
        //已签到信息
        mRegistrationList = new ArrayList<>();
        //dialog
        mTicketInfoDialog = new TicketInfoDialog();
        //排列方式
        mRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        //设置adapter
        mAdapter = new RegistrationAdapter(mTicketInfos, null);
        mRecyclerview.setAdapter(mAdapter);
        mTicketInfo1 = new TicketInfo();
        //初始化数据
        getAlldata();
        if (Hawk.contains(Constants.ISCHECKPOINT)) {
            mTvCheckpointName.setText("跨报到台报到");
            getAlldata();
        } else if (Hawk.contains(Constants.CHECKPINT)) {
            CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
            String checkPointName = checkPointEntity.getCheckpointName();
            String checkname = null;
            if (checkPointName.contains("【")) {
                checkname = checkPointName.substring(checkPointName.indexOf("【") + 1, checkPointName.indexOf("】"));
            } else {
                checkname = checkPointName.substring(2, checkPointName.length());
            }
            mTvCheckpointName.setText(checkname);
            //            obser(checkname);
            getCheckPointdataByid(checkPointEntity.getId());
        } else {
            mTvCheckpointName.setText("未设置报到台");
        }

        //事件监听
        initListener();
    }

    //事件监听
    private void initListener() {
        //搜索输入监听
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //搜索关键字
                String searchContent = mEtSearch.getText().toString().trim();

                if (StringUtils.isNotEmpty(searchContent)) {            //不为空 搜索关键字
                    obser(searchContent);
                } else {
                    setDate();
                }
            }
        });
        //下拉刷新
        mSmartRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                setDate();
                mSmartRefresh.finishRefresh();
            }
        });
        //上来加载更多
        mSmartRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mSmartRefresh.finishLoadMore();
                ToastUtils.showShort(getString(R.string.no_more));
            }
        });

        //用户信息item 点击
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                TicketInfo ticketInfo = (TicketInfo) adapter.getData().get(position);
                if (ticketInfo.getReportTeamId() > 0) {
                    if (ticketInfo.getLeaderFlag() == 1) {   //是否是团队长，领队
                        if (Hawk.contains(Constants.DEVICE)) {
                            if (Hawk.contains(Constants.CHECKPINT)) {   //是否有设置报到台
                                CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
                                if (checkPointEntity.getId() == ticketInfo.getCheckpointId() || Hawk.contains(Constants.ISCHECKPOINT)) {
                                    Intent intent = new Intent(getActivity(), TeamRegistionctivity.class);
                                    intent.putExtra("teamticketInfo", ticketInfo);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                } else if (StringUtils.isNotEmpty(ticketInfo.getCheckpointIdList2())) {
                                    List<Integer> checkpointidList = GsonUtil.fromString(ticketInfo.getCheckpointIdList2());
                                    boolean ischeckPointId = false;
                                    for (int i = 0; i < checkpointidList.size(); i++) {
                                        if (checkpointidList.get(i) == checkPointEntity.getId()) {
                                            ischeckPointId = true;
                                        }
                                    }
                                    if (ischeckPointId == true) {
                                        Intent intent = new Intent(getActivity(), TeamRegistionctivity.class);
                                        intent.putExtra("teamticketInfo", ticketInfo);
                                        getActivity().startActivity(intent);
                                    } else {
                                        mTicketInfoDialog.registHint(getActivity(), ticketInfo, getString(R.string.team_hint), true);
                                    }
                                } else {
                                    mTicketInfoDialog.registHint(getActivity(), ticketInfo, null, true);
                                }

                            } else {  //提示领队 报到台
                                mTicketInfoDialog.registHint(getActivity(), ticketInfo, null, true);
                            }
                        } else {
                            //设置报到设备dialog
                            mTicketInfoDialog.showServer(getActivity(), null, getString(R.string.device_hint), null, false);
                        }
                    } else {//查询 团队长信息展示
                        getTeamListByid(ticketInfo.getReportTeamId());
                    }
                } else { //个人报到  dialog
                    mTicketInfoDialog.registHint(getActivity(), ticketInfo, null, false);
                }
            }
        });
    }

    //设置显示数据
    private void setDate() {
        if (!Hawk.contains(Constants.ISCHECKPOINT) && mCheckPointTicketList != null && mCheckPointTicketList.size() > 0) {          //设置报到台中 全部门票信息
            //清除原先数据
            mTicketInfos.clear();
            //添加数据
            mTicketInfos.addAll(mCheckPointTicketList);
            //设置总人数
            mTvCheckpointNameCount.setText("："+mAttendanceList.size());
            //刷新界面
            mAdapter.notifyDataSetChanged();

        } else if (Hawk.contains(Constants.ISCHECKPOINT) && mTicketInfoList != null && mTicketInfoList.size() > 0) {          //跨平台报到  门票信息
            //清除原先数据
            mTicketInfos.clear();
            //添加数据
            mTicketInfos.addAll(mTicketInfoList);
            //设置总人数
            mTvCheckpointNameCount.setText("："+mTicketInfoList.size());
            //刷新界面
            mAdapter.notifyDataSetChanged();
        } else {
            getAlldata();
        }
    }

    //查询全部数据
    private void getAlldata() {

        AppDatabase.getInstance(getActivity()).getTicketInfoDao().loadAllTicket()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<TicketInfo>>() {
                @Override
                public void accept(List<TicketInfo> userEntities) throws Exception {

                    hideLoading();
                    //清除原先数据
                    mTicketInfos.clear();
                    mTicketInfoList.clear();
                    //全部门票信息
                    mTicketInfoList.addAll(userEntities);

                    //已报到
                    if (Hawk.contains(Constants.ISCHECKPOINT)) {
                        setNumber(userEntities);
                    } else if (mCheckPointTicketList != null && mCheckPointTicketList.size() > 0) {
                        //已报到人数
                        mTvRegistrationNumber.setText(String.valueOf(mRegistrationList.size()));

                        //未报到人数
                        if (mRegistrationList.size() == mAttendanceList.size()) {
                            mTvBeforeTheNumber.setText("0");
                        } else {
                            String size = String.valueOf(mAttendanceList.size() - mRegistrationList.size());
                            mTvBeforeTheNumber.setText(size);
                        }
                        mTvCheckpointNameCount.setText("："+mAttendanceList.size());

                        //添加数据
                        mTicketInfos.addAll(mCheckPointTicketList);
                    } else {
                        mTvRegistrationNumber.setText("0");
                        mTvBeforeTheNumber.setText("0");
                    }

                    //刷新界面
                    mAdapter.notifyDataSetChanged();
                    //取消下拉刷新效果
                    mSmartRefresh.finishRefresh();
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {
                    //                        ToastUtils.showShort(throwable.toString());
                    if (mSmartRefresh != null) {
                        mSmartRefresh.finishRefresh();
                    }

                    Log.d("test", "查询---" + throwable.toString());
                }
            });
    }

    //设置报到人数  未报到人数  总人数
    private void setNumber(List<TicketInfo> userEntities) {
        if (userEntities != null && userEntities.size() > 0) {
            for (int i = 0; i < userEntities.size(); i++) {
                TicketInfo ticketInfo = userEntities.get(i);
                if (ticketInfo.getCheckedFlag() == 1) {
                    mRegistrationList.add(ticketInfo);
                }
            }
            //已报到人数
            mTvRegistrationNumber.setText(String.valueOf(mRegistrationList.size()));

            //未报到人数
            if (mRegistrationList.size() == userEntities.size()) {
                mTvBeforeTheNumber.setText("0");
            } else {
                String size = String.valueOf(userEntities.size() - mRegistrationList.size());
                mTvBeforeTheNumber.setText(size);
            }

        }
        //总人数
        mTvCheckpointNameCount.setText("："+userEntities.size());
        //添加数据
        mTicketInfos.addAll(userEntities);
    }

    //查询当前报到台全部数据
    private void getCheckPointdataByid(int checkPontId) {
        AppDatabase.getInstance(getActivity()).getTicketInfoDao().queryCheckPointIds(checkPontId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<TicketInfo>>() {
                    @Override
                    public void accept(List<TicketInfo> userEntities) throws Exception {
                        hideLoading();
                        //清除原先数据
                        mTicketInfos.clear();
                        mRegistrationList.clear();
                        mAttendanceList.clear();
                        //已报到
                        if (userEntities != null && userEntities.size() > 0) {
                            for (int i = 0; i < userEntities.size(); i++) {
                                TicketInfo ticketInfo = userEntities.get(i);
                                if (ticketInfo.getCheckedFlag() == 1) {
                                    mRegistrationList.add(ticketInfo);
                                }
                                if (ticketInfo.getParticipationFlag() == 0||StringUtils.isNotEmpty(ticketInfo.getIdentityName())) {
//                                    if (StringUtils.isEmpty(ticketInfo.getIdentityName())) {
//                                        attendanceList.add(ticketInfo);
//                                    }
                                } else {
                                    mAttendanceList.add(ticketInfo);
                                }
                            }
                            //已报到人数
                            mTvRegistrationNumber.setText(String.valueOf(mRegistrationList.size()));

                            //未报到人数
                            if (mRegistrationList.size() == mAttendanceList.size()) {
                                mTvBeforeTheNumber.setText("0");
                            } else {
                                String size = String.valueOf(mAttendanceList.size() - mRegistrationList.size());
                                mTvBeforeTheNumber.setText(size);
                            }

                        } else {
                            mTvRegistrationNumber.setText("0");
                            mTvBeforeTheNumber.setText("0");
                        }
                        //总人数
                        mTvCheckpointNameCount.setText("：" + mAttendanceList.size());

                        mCheckPointTicketList.clear();
                        //报到台门票信息
                        mCheckPointTicketList.addAll(userEntities);
                        //添加数据
                        mTicketInfos.addAll(userEntities);
                        //刷新界面
                        mAdapter.notifyDataSetChanged();
                        //取消下拉刷新效果
                        mSmartRefresh.finishRefresh();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //                        ToastUtils.showShort(throwable.toString());
                        if (mSmartRefresh != null) {
                            mSmartRefresh.finishRefresh();
                        }

                        Log.d("test", "查询报到台全部数据---" + throwable.toString());
                    }
                });

    }

    //获取为团队id 用户门票数据集合
    private void getTeamListByid(int reportTeamId) {
        AppDatabase.getInstance(getActivity())
                .getTicketInfoDao()
                .queryReportTeamIds(reportTeamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<TicketInfo>>() {
                    @Override
                    public void accept(List<TicketInfo> ticketInfos) throws Exception {
                        if (ticketInfos != null && ticketInfos.size() > 0) {
                            TicketInfo LeaderticketInfo = new TicketInfo();
                            for (int i = 0; i < ticketInfos.size(); i++) {
                                TicketInfo ticketInfo = ticketInfos.get(i);
                                if (ticketInfo.getLeaderFlag() == 1) {
                                    LeaderticketInfo = ticketInfo;
                                    mTicketInfoDialog.registHint(getActivity(), ticketInfo, "您属于" + ticketInfo.getReportTeamName() + "团队，请提醒您的团队长/领队  " + ticketInfo.getUsername() + "  前来报到", true);
                                }
                            }
                            if (LeaderticketInfo == null || LeaderticketInfo.getUsername() == null) {
                                mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.team_hint2));
                            }

                        }
                    }
                });
    }


    //异步查询操作 搜索
    private void obser(String str) {
        //创建观察者对象
        Observable.create(new ObservableOnSubscribe<List<TicketInfo>>() {
            @Override
            public void subscribe(ObservableEmitter<List<TicketInfo>> emitter) throws Exception {
                //新建list集合用于保存符合条件的数据
                List<TicketInfo> mSearchSurverList = new ArrayList<TicketInfo>();
                //搜索内容不区分大小写
                String searchText = str.toLowerCase();
                if (!searchText.equals("") || searchText.length() != 0) {
                    //遍历数据
                    for (TicketInfo info : mTicketInfoList) {
                        String infoString = "";
                        try {
                            //对象实体数据 不区分大小写
                            infoString = info.toString().toLowerCase();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //添加符合条件的数据
                        if (!infoString.equals("") && infoString.contains(searchText) || infoString.startsWith(searchText)) {
                            mSearchSurverList.add(info);
                        }
                    }
                    //设置最新数据
                    emitter.onNext(mSearchSurverList);
                } else {
                    //设置数据
                    emitter.onNext(mTicketInfos);
                }

            }
        }).subscribeOn(Schedulers.io())             //指定subscribe（）发生再io线程
                .observeOn(AndroidSchedulers.mainThread())      //指定subscribe 的回调发生在主线程
                .subscribe(new Observer<List<TicketInfo>>() {       //订阅事件
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(List<TicketInfo> ticketInfos) {           //相当于onclick()/onevent()
                        //清除原先数据
                        mTicketInfos.clear();
                        //添加最新数据
                        mTicketInfos.addAll(ticketInfos);
                        //刷新数据
                        mAdapter.notifyDataSetChanged();

//                        mTvCheckpointNameCount.setText("：" + ticketInfos.size());

                    }

                    @Override
                    public void onError(Throwable e) {          //事件队列异常
                        //                        Toast.makeText(getActivity(), "Error!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {                  //事件队列完结  标记

                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.iv_refresh, R.id.iv_delete, R.id.iv_scan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_refresh:       //刷新
                if (Hawk.contains(Constants.DIVSION)) {
                    if (mTicketInfoList!=null&& mTicketInfoList.size()>0){
                        mTicketInfoDialog.syncTicketData(getActivity());
                    }else {
                        getTicketInfo();
                    }

                } else {
                    mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.checkpoint_hint));
                }
                break;
            case R.id.iv_delete:        //删除
                deleteData();
                break;
            case R.id.iv_scan:          //扫码
                initScan();
                break;
        }
    }

    //删除签到数据
    public void deleteData() {
        if (mTicketInfoList != null && mTicketInfoList.size() > 0) {
            showLoading();
            List<TicketInfo> infoList = new ArrayList<>();
            for (int i = 0; i < mTicketInfoList.size(); i++) {
                TicketInfo ticketInfo = mTicketInfoList.get(i);
                ticketInfo.setCheckedFlag(0);
                infoList.add(ticketInfo);
            }
            //更新数据 数据库
            AppDatabase.getInstance(getActivity()).runInTransaction(new Runnable() {
                @Override
                public void run() {
                    AppDatabase.getInstance(getActivity()).getTicketInfoDao().updateListTicket(infoList);
                }
            });
            //获取全部的签到记录信息
            AppDatabase.getInstance(getActivity()).getSignInDao().getAllSign()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<List<SignInMoudle>>() {
                        @Override
                        public void accept(List<SignInMoudle> signInMoudleList) throws Exception {
                            //删除全部签到记录
                            if (signInMoudleList != null && signInMoudleList.size() > 0) {
                                AppDatabase.getInstance(getActivity()).getSignInDao().deleteAll(signInMoudleList);
                            }
                        }
                    });

            mTicketInfoList.clear();
            mTicketInfos.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    //用户门票信息
    private void getTicketInfo() {
        //显示加载loading
        RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
        showLoading();
        ServiceGenerager
                .getInstance()
                .createService()
                .getTicketInfo(IdaParams.getTicketInfo(recordsBean.getMeetingId()))
                .enqueue(new CommonCallBack<ApiResponse<List<TicketInfo>>>() {
                    @Override
                    public void onSuc(String code, String errMsg, Response<ApiResponse<List<TicketInfo>>> response) {

                        List<TicketInfo> ticketInfoList = response.body().data;
                        if (ticketInfoList != null && ticketInfoList.size() > 0) {
                            List<TicketInfo> infoList = new ArrayList<>();
                            for (int i = 0; i < ticketInfoList.size(); i++) {
                                TicketInfo ticketInfo = ticketInfoList.get(i);
                                if (ticketInfo.getCheckpointIdList() != null) {
                                    String checkpoint = GsonUtil.fromArrayList(ticketInfo.getCheckpointIdList());
                                    ticketInfo.setCheckpointIdList2(checkpoint);
                                }
                                infoList.add(ticketInfo);
                            }
                            mTicketInfos.clear();
                            mTicketInfos.addAll(infoList);
                            mAdapter.notifyDataSetChanged();
                            //插入数据 数据库
                            AppDatabase.getInstance(getActivity())
                                    .getTicketInfoDao()
                                    .insertAll(infoList);
                        } else {
                            mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.no_data_hint2));
                        }

                        //隐藏加载loading
                        hideLoading();

                    }

                    @Override
                    public void onFail(String code, String errMsg, Response<ApiResponse<List<TicketInfo>>> response) {
                        hideLoading();
                        if (code.equals("500")) {        //cookie值失效
                            mTicketInfoDialog.registLoginHint(getActivity(), getString(R.string.login_hint2));
                        } else {
                            mTicketInfoDialog.registCodeHint(getActivity(), errMsg);
                        }

                    }
                });
    }

    /**
     * 跳转扫码
     */
    private void initScan() {
        if (mTicketInfoList != null && mTicketInfoList.size() > 0) {
            if (Hawk.contains(Constants.CHECKPINT) && Hawk.contains(Constants.DEVICE)) {
                Intent intent = new Intent(getActivity(), SecondActivity.class);
                MyApplication.getInstance().setTicketList(mTicketInfoList);
                getActivity().startActivity(intent);
                getActivity().finish();
            } else {
                mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.setting_hint2));
            }
        } else {
            mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.scan_hint));
        }
        //            getActivity().startActivity(intent);
        //            startActivityForResult(new Intent(getActivity(), SecondActivity.class), REQUEST_CODE);
    }


    //Fragment 是否处于可见状态
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {    //相当于onResume()
            if (Hawk.contains(Constants.ISCHECKPOINT) && mTvCheckpointName != null) {
                mTvCheckpointName.setText("跨报到台报到");
                getAlldata();
            } else if (Hawk.contains(Constants.CHECKPINT) && mTvCheckpointName != null) {
                CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
                String checkPointName = checkPointEntity.getCheckpointName();
                String checkname = null;
                if (checkPointName.contains("【")) {
                    checkname = checkPointName.substring(checkPointName.indexOf("【") + 1, checkPointName.indexOf("】"));
                } else {
                    checkname = checkPointName.substring(2, checkPointName.length());
                }

                mTvCheckpointName.setText(checkname);
                getCheckPointdataByid(checkPointEntity.getId());
            } else if (mTvCheckpointName != null) {
                mTvCheckpointName.setText("未设置报到台");
            }
        }
    }

    /**********************************************************相机拍照功能*******************************************************************************/
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(TicketInfo ticketInfo) {
        mTicketInfo1 = null;
        mTicketInfo1 = ticketInfo;
        if (StringUtils.isNotEmpty(ticketInfo.getUsername()))
            //拍照
            takePhoto();

    }

    //调用系统相机拍照
    private void takePhoto() {
        //指定拍照
        Intent getImageByCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //拍照图片名字
        mPhotoName = mTicketInfo1.getId() + ".jpg";
        //路径
        File photoFile = new File(getAlbumStorageDir(Constants.APPPICTUREPATH), mPhotoName);
        //拍照的uri
        if (Build.VERSION.SDK_INT >= 24) {
            mPhotoUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", photoFile);
        } else {
            mPhotoUri = Uri.fromFile(photoFile);
        }
        //启动意图
        getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
        startActivityForResult(getImageByCamera, TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //        Log.d("test","-------"+data.toString());
        //打印log 方便测试
        Log.i("test", "onActivityResult-------" + mPhotoUri.toString());
        //如果回传码为0和回传码生成
        if (requestCode == TAKE_PHOTO && resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            try {
                //获取原图片
                bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(mPhotoUri));
                //压缩图片
                Bitmap bitmaps = BitmapUtils.compressScale(bitmap);
                //更新相册图片
                boolean hasbitmap = BitmapUtils.addSignatureToGallery(getActivity(), bitmaps, mPhotoName);
                if (hasbitmap) {   //是否保存成功
                    //回收bitmap
                    bitmap.recycle();
                    //会议届数
                    RecordsBean RecordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
                    //原先的dialog销毁
                    mTicketInfoDialog.dialogDismiss();
                    //设置用户门票的图片
                    mTicketInfo1.setTransferPicture(mPhotoName);
                    //更新门票信息
                    AppDatabase.getInstance(getActivity()).getTicketInfoDao().updateUser(mTicketInfo1);
                    //重新加载用户门票信息
                    mTicketInfoDialog.registHint(getActivity(), mTicketInfo1, null, true);
                    //上传的实体bean
                    TransferPictureMoudle transferPictureMoudle = new TransferPictureMoudle();
                    //图片转换成base64
                    String base64 = BitmapUtils.bitmapToBase64(bitmaps);
                    transferPictureMoudle.setBase64Image(base64);
                    //回收压缩的图片
                    bitmaps.recycle();
                    //门票
                    transferPictureMoudle.setTicketCode(mTicketInfo1.getTicketCode());
                    //会议id
                    transferPictureMoudle.setMeetingId(RecordsBean.getMeetingId());
                    //网络上传用户门票图片
                    ServiceGenerager.getInstance().createService().transferPicture(transferPictureMoudle)
                            .enqueue(new CommonCallBack<ApiResponse<String>>() {
                                @Override
                                public void onSuc(String code, String errMsg, Response<ApiResponse<String>> response) {

                                }
                            });
                } else {
                    ToastUtils.showShort("图片保存失败");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /****************************************************end*******************************************************************************************/
}
