package com.registration.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.base.BaseFragment;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.CheckInListMoudle;
import com.registration.moudle.CheckPointEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/*
 * @创建者 huangxy
 * @创建时间  2021/1/20 15:08
 * @描述      ${TODO}   统计
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class StatisticsFragment extends BaseFragment {

    @BindView(R.id.webview)
    WebView mWebView;

    private CheckPointEntity checkPointEntity = null;
    private List<CheckInListMoudle> datalist = new ArrayList<>();

    @Override
    protected int contentViewID() {
        return R.layout.fragment_statistics;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWebView();
    }

    private void initWebView() {

        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //得到webview设置
        WebSettings webSettings = mWebView.getSettings();
        //允许使用javascript
        webSettings.setJavaScriptEnabled(true);
        //设置字符编码
        webSettings.setDefaultTextEncodingName("UTF-8");
        //支持缩放
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setUseWideViewPort(true);
        //不显示webview缩放按钮
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //将WebAppInterface与javascript绑定
        //webview.addJavascriptInterface(new JavaScriptInterface(), "Android");
        //android assets目录下html文件路径url为 file:///android_asset/index.html
        mWebView.loadUrl("file:///android_asset/analysis.html");
    }

    @Override
    protected void onActivate() {

        checkPointEntity = new CheckPointEntity();

        if (Hawk.contains(Constants.CHECKPINT)) {
            checkPointEntity = Hawk.get(Constants.CHECKPINT);
        }

        int mCheckpointId = (checkPointEntity != null)? checkPointEntity.getId(): -1;
        String mCheckpointName = (checkPointEntity != null)? checkPointEntity.getCheckpointName(): "无";

        AppDatabase.getInstance(getActivity()).getDataPackDao().getCheckInList(mCheckpointId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<CheckInListMoudle>>() {
                @Override
                public void accept(List<CheckInListMoudle> list) throws Exception {
                    datalist.clear();
                    if (list != null && list.size() > 0) {
                        datalist.addAll(list);
                    }
                    int checkInCount = 0;
                    for(CheckInListMoudle moudle : datalist) {
                        if (moudle.isCheckIn()) checkInCount ++;
                    }

                    Log.e(TAG, mCheckpointName + " >>> " + checkInCount + " + " + (datalist.size() - checkInCount) + " = " + datalist.size());

                    //刷新界面
                    mWebView.loadUrl("file:///android_asset/analysis.html?Checkpoint=" + mCheckpointName + "&Tableau=" + checkInCount + "," + (datalist.size() - checkInCount));
                }
            });
    }
}
