package com.registration.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.registration.R;
import com.registration.adapter.HistoryAdapter;
import com.registration.base.BaseFragment;
import com.registration.database.AppDatabase;
import com.registration.moudle.HistoryEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/*
 * @创建者 huangxy
 * @创建时间  2021/1/20 15:08
 * @描述      ${TODO}   签到历史
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class HistoryFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerview;

    private HistoryAdapter mAdapter = null;
    private List<HistoryEntity> historyList = new ArrayList<>();

    @Override
    protected int contentViewID() {
        return R.layout.fragment_history;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        collapsingToolbar.setExpandedTitleTypeface(Typeface.DEFAULT_BOLD);
        collapsingToolbar.setCollapsedTitleTextColor(Color.BLACK);
        collapsingToolbar.setExpandedTitleColor(Color.BLACK);

        mAdapter = new HistoryAdapter(historyList);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerview.setAdapter(mAdapter);
        //用户信息item 点击
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //HistoryEntity item = (HistoryEntity) adapter.getData().get(position);
            }
        });
    }

    @Override
    protected void onActivate() {

        //只显示倒数一百条记录
        AppDatabase.getInstance(getActivity()).getHistoryDao().queryOrderByDesc(100)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<HistoryEntity>>() {
                @Override
                public void accept(List<HistoryEntity> list) throws Exception {
                    historyList.clear();
                    if (list != null && list.size() > 0) {
                        historyList.addAll(list);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
    }
}
