package com.registration.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.registration.R;
import com.registration.adapter.BottomViewAdapter;
import com.registration.base.BaseFragment;
import com.registration.moudle.TicketInfo;
import com.registration.view.ClearEditText;
import com.registration.view.CustomViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/*
 * @创建者 huangxy
 * @创建时间  2021/1/20 15:08
 * @描述      ${TODO}   报到
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class CheckInFragment extends BaseFragment {

    @BindView(R.id.viewpager_check_in)
    CustomViewPager mViewpager;

    private List<BaseFragment> mFragments;
    private CheckInListFragment listFragment;
    private CheckInDetailFragment detailFragment;

    @Override
    protected int contentViewID() {
        return R.layout.fragment_check_in;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        listFragment = new CheckInListFragment();
        detailFragment = new CheckInDetailFragment();

        mFragments = new ArrayList<>();
        mFragments.add(listFragment);
        mFragments.add(detailFragment);

        BottomViewAdapter adapter = new BottomViewAdapter(getChildFragmentManager(), mFragments);
        mViewpager.setOffscreenPageLimit(mFragments.size());
        mViewpager.setAdapter(adapter);
    }

    //下一页
    public void nextPage() {
        mViewpager.setCurrentItem(1);
    }

    //返回上一页
    public void back() {
        mViewpager.setCurrentItem(0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
