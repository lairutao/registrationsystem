package com.registration.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.huangxy.actionsheet.ActionSheet;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.activity.HistoryActivity;
import com.registration.base.BaseFragment;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.CheckInDetailMoudle;
import com.registration.moudle.CheckInListMoudle;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.HistoryEntity;
import com.registration.moudle.RemarkEntity;
import com.registration.util.BitmapUtils;
import com.registration.util.DrawableUtils;
import com.registration.util.IntentUtil;
import com.registration.util.StringUtils;
import com.registration.view.TicketInfoDialog;
import com.registration.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/*
 * @创建者 huangxy
 * @创建时间  2021/1/20 15:08
 * @描述      ${TODO}   报到详情页
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class CheckInDetailFragment extends BaseFragment {

    @BindView(R.id.common_toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title_tv)
    TextView tvTitle;
    @BindView(R.id.toolbar_tv_right)
    TextView tvRight;

    @BindView(R.id.nested_scrollview)
    NestedScrollView scrollView;

    @BindView(R.id.ll_teaminfo)
    LinearLayout llTeamInfo;
    @BindView(R.id.tv_leader)
    TextView tvLeaderName;
    @BindView(R.id.tv_leader_and_member)
    TextView tvLeaderAndMember;
    @BindView(R.id.tv_team_name)
    TextView tvTeamName;
    @BindView(R.id.tv_members)
    TextView tvMembers;

    @BindView(R.id.ll_userinfo)
    LinearLayout llUserInfo;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_user_type)
    TextView tvUserType;
    @BindView(R.id.tv_idcard)
    TextView tvIdCard;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_checkpoint)
    TextView tvCheckpoint;
    @BindView(R.id.tv_company)
    TextView tvCompany;
    @BindView(R.id.tv_userid)
    TextView tvUserId;
    @BindView(R.id.tv_ticketid)
    TextView tvTicketId;
    @BindView(R.id.tv_qrcode)
    TextView tvQrCode;

    @BindView(R.id.ll_remarkinfo)
    LinearLayout llRemarkInfo;
    @BindView(R.id.edit_text)
    EditText mEditText;

    @BindView(R.id.ll_authorizeinfo)
    LinearLayout llAuthorizeInfo;
    @BindView(R.id.authorize_pic)
    ImageView authorizeView;

    @BindView(R.id.ll_checkin_Info)
    LinearLayout llCheckinInfo;
    @BindView(R.id.tv_checkin_Info)
    TextView tvCheckinInfo;
    @BindView(R.id.ll_helpInfo)
    LinearLayout llhelpInfo;

    private TicketInfoDialog mTicketInfoDialog = new TicketInfoDialog();

    private CheckInListMoudle listMoudle;
    private CheckInDetailMoudle detailMoudle;
    private ActionSheet actionSheet;
    private String transferProfile;
    private String mPhotoName;
    private Uri mPhotoUri;
    private int mCount;

    private boolean canCheckIn = false;

    @Override
    protected int contentViewID() {
        return R.layout.fragment_check_in_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        EventBus.getDefault().register(this);

        //BaseViewModel viewModel = new ViewModelProvider(getActivity(), new ViewModelProvider.NewInstanceFactory()).get(BaseViewModel.class);
        //viewModel.getLiveData().observe(getViewLifecycleOwner(), moudle -> onItemClick((CheckInListMoudle) moudle));

        //((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(DrawableUtils.TintWithRes(getContext(), R.drawable.back, R.color.icon_color));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listMoudle == null || listMoudle.isCheckIn() || !canCheckIn) {
                    goBack(); //已报到或者为团员(非领队)时直接退出
                    return;
                }
                mTicketInfoDialog.showConfirm(getActivity(), getString(R.string.registrations), "还未签到，确定要退出吗？", new TicketInfoDialog.actionListener() {
                    @Override
                    public void onConfirm() {
                        goBack();
                    }
                });
            }
        });
        if(getUserVisibleHint()) {
            onActivate();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClick(CheckInListMoudle moudle) {
        listMoudle = moudle; //更新参数
        if (moudle != null) {
            String title = moudle.getGroupName();
            if (!TextUtils.isEmpty(title)) {
                tvTitle.setText(title);
                return;
            }
        }
        tvTitle.setText(R.string.registrations);
    }

    @Override
    protected void onActivate() {

        mCount = 0; //计数

        if (listMoudle != null && listMoudle.getTicketId() > 0) {

            showLoading();
            AppDatabase.getInstance(getActivity()).getDataPackDao().queryCheckInDetailById(listMoudle.getTicketId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CheckInDetailMoudle>() {
                        @Override
                        public void accept(CheckInDetailMoudle data) throws Exception {
                            detailMoudle = data;
                            //刷新界面
                            initView();
                            //隐藏加载框
                            hideLoading();
                        }
                    });
        } else {
            ToastUtils.showLong(R.string.init_error_hint);
        }
    }

    private void initView() {

        if (listMoudle != null && detailMoudle != null) {

            canCheckIn = !listMoudle.isTeam() || listMoudle.isLeader();
            tvRight.setVisibility((canCheckIn || !listMoudle.isCheckIn())? View.VISIBLE: View.GONE);

            tvLeaderName.setHint(detailMoudle.getLeaderName());
            tvLeaderAndMember.setHint((listMoudle.isLeader()&&listMoudle.isGroup())? "是": "否");
            tvTeamName.setHint(detailMoudle.getGroupName());
            tvMembers.setHint(""+detailMoudle.getGroupMemberNum());
            llTeamInfo.setVisibility((listMoudle.isTeam())? View.VISIBLE: View.GONE);

            tvUserName.setHint(listMoudle.getChineseName());
            tvUserType.setHint(detailMoudle.getIdentityName());
            tvIdCard.setHint(listMoudle.getDocumentNumber());
            tvEmail.setHint(listMoudle.getEmail());
            tvPhone.setHint(listMoudle.getMobile());
            String checkpointName = detailMoudle.getCheckpointName(listMoudle.isTeam());
            tvCheckpoint.setHint(StringUtils.notEmpty(checkpointName));
            tvCompany.setHint(listMoudle.getCompanyName());
            tvUserId.setHint(""+listMoudle.getUserId());
            tvTicketId.setHint(""+listMoudle.getTicketId());
            tvQrCode.setHint(detailMoudle.getRandomCode());
            llUserInfo.setVisibility(View.VISIBLE);

            mEditText.setText(detailMoudle.getRemark());
            llRemarkInfo.setVisibility((canCheckIn)? View.VISIBLE: View.GONE);

            authorizeView.setImageDrawable(null);
            transferProfile = detailMoudle.getTransferProfile();
            if (!TextUtils.isEmpty(transferProfile)) {
                try {
                    Bitmap bitmap = BitmapUtils.base64ToBitmap(transferProfile);
                    if (bitmap != null) {
                        Glide.with(getContext()).load(bitmap).into(authorizeView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            llAuthorizeInfo.setVisibility((canCheckIn)? View.VISIBLE: View.GONE);

            //团员且未报到才显示提示
            //llhelpInfo.setVisibility((canCheckIn || listMoudle.isCheckIn())? View.GONE: View.VISIBLE);

            tvCheckinInfo.setText(listMoudle.getCheckInStatus2() + "，" + getString(R.string.checkin_confirm));
            llCheckinInfo.setVisibility(listMoudle.isCheckIn()? View.VISIBLE: View.GONE);

        } else {
            ToastUtils.showLong(R.string.init_fail_hint);
        }
    }

    @OnClick({R.id.toolbar_tv_right, R.id.ll_checkin_Info, R.id.rl_authorize})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_tv_right:
                if (!canCheckIn) {
                    //if (mCount > 0) {
                        ToastUtils.showShort(R.string.checkin_hint);
                    //} else
//                        new Handler().postDelayed(new Runnable() {
//                            public void run() {
//                                scrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
//                            }
//                        }, 200);
                    //mCount ++;
                    return;
                }
                mTicketInfoDialog.showConfirm(getActivity(), getString(R.string.registrations), "确定要签到吗？", new TicketInfoDialog.actionListener() {
                    @Override
                    public void onConfirm() {
                        CheckPointEntity checkPointEntity = null;
                        if (Hawk.contains(Constants.CHECKPINT)) {
                            checkPointEntity = Hawk.get(Constants.CHECKPINT);
                        }
                        boolean isSuccess = true; //签到成功标志
                        boolean isCheckPoint =  Hawk.get(Constants.ISCHECKPOINT, true);
                        if (!isCheckPoint && checkPointEntity != null && checkPointEntity.getId() > 0) {
                            isSuccess = (detailMoudle.getCheckpointId()==checkPointEntity.getId());
                        }
                        String resultMsg = (isSuccess)? getString(R.string.check_in_success): getString(R.string.check_in_fail);
                        String operator = Hawk.get((Constants.ACCOUNT +"_"+ Hawk.get(Constants.ACCOUNT)), "");
                        String remark = resultMsg + getString(R.string.operator, operator);
                        RemarkEntity remarkEntity = new RemarkEntity(listMoudle.getTicketId(), transferProfile, mEditText.getText().toString().trim());
                        HistoryEntity historyEntity = new HistoryEntity(detailMoudle.getSubId(listMoudle.isTeam()), listMoudle.getTicketId(), listMoudle.getCiType(), listMoudle.getChineseName(), remark, ((isSuccess)? 1: 0), System.currentTimeMillis());
                        AppDatabase.getInstance(getActivity()).getDataPackDao().userCheckIn(remarkEntity, historyEntity);
                        EventBus.getDefault().post(historyEntity);
                        ToastUtils.showLong(resultMsg);
                        if (isSuccess) goBack();
                    }
                });
                break;
            case R.id.ll_checkin_Info:
                if (listMoudle != null && listMoudle.isCheckIn()) {
                    int ticketId = (listMoudle.isTeam())? listMoudle.getLeaderTicketId(): listMoudle.getTicketId();
                    IntentUtil.startActivity(HistoryActivity.class, listMoudle.getCiType(), ticketId);
                }
                break;
            case R.id.rl_authorize:
                ActionSheet.DialogBuilder dialogBuilder = new ActionSheet.DialogBuilder(getActivity());
                dialogBuilder.addSheet("拍照", v -> {
                    actionSheet.dismiss();
                    takePhoto();
                })
                .addSheet("打开相册", v -> {
                    actionSheet.dismiss();
                    selectPhoto();
                });

                if (!TextUtils.isEmpty(transferProfile)) {
                    dialogBuilder.addSheet("保存到相册", v -> {
                        actionSheet.dismiss();
                        try {
                            Bitmap bitmap = BitmapUtils.base64ToBitmap(transferProfile);
                            if (bitmap != null) {
                                mPhotoName = listMoudle.getTicketId() + "_" + System.currentTimeMillis() + ".png";
                                boolean isSuccess = BitmapUtils.addSignatureToGallery(getContext(), bitmap, mPhotoName);
                                if (isSuccess) {
                                    ToastUtils.showLong(R.string.save_success);
                                } else {
                                    ToastUtils.showLong(R.string.save_fail);
                                }
                            }
                        } catch (Exception e) {
                            ToastUtils.showLong(getString(R.string.save_fail)+"："+e.getMessage());
                            e.printStackTrace();
                        }
                    })
                    .addSheet("删除照片", Color.parseColor("#FF3B30"), v -> {
                        authorizeView.setImageDrawable(null);
                        transferProfile = null;
                        actionSheet.dismiss();
                    });
                }

                actionSheet = dialogBuilder.addCancelListener(v -> actionSheet.dismiss()).create();
                actionSheet.show();

                break;
            default:
                break;
        }
    }

    private void goBack() {
        ((CheckInFragment) getParentFragment()).back();
        tvRight.setVisibility(View.INVISIBLE); //恢复初始化
        llCheckinInfo.setVisibility(View.GONE); //隐藏悬浮框
        llAuthorizeInfo.setVisibility(View.GONE);
        llRemarkInfo.setVisibility(View.GONE);
        llTeamInfo.setVisibility(View.GONE);
        llUserInfo.setVisibility(View.GONE);
        transferProfile = null;
        detailMoudle = null;
        listMoudle = null;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    /**********************************************************相机拍照功能*******************************************************************************/

    //从系统相册选取
    private void selectPhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "选择图片"), Constants.SELECT_PHOTO);
    }

    //调用系统相机拍照
    private void takePhoto() {
        //指定拍照
        Intent getImageByCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //图片名称
        mPhotoName = listMoudle.getTicketId() + "_" + System.currentTimeMillis() + ".jpg";
        //图片路径
        File photoFile = new File(BitmapUtils.getAlbumStorageDir(Constants.APPPICTUREPATH), mPhotoName);
        //拍照的uri
        if (Build.VERSION.SDK_INT >= 24) {
            mPhotoUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", photoFile);
        } else {
            mPhotoUri = Uri.fromFile(photoFile);
        }
        //启动意图
        getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
        startActivityForResult(getImageByCamera, Constants.TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = null;
            try {
                if (requestCode == Constants.SELECT_PHOTO) {
                    bitmap = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(intent.getData()));
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), intent.getData());
                } else {
                    bitmap = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(mPhotoUri));
                }
                //压缩图片
                Bitmap bitmaps = BitmapUtils.compressScale(bitmap);
                //成功显示图片
                Glide.with(getContext()).load(bitmaps).into(authorizeView);
                //图片转换成base64
                transferProfile = BitmapUtils.bitmapToBase64(bitmaps);
            } catch (Exception e){
                ToastUtils.showShort("图片保存失败");
                Log.e(TAG, e.getMessage());
            } finally {
                if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        }
    }

    /****************************************************end*******************************************************************************************/

}
