package com.registration.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.base.BaseFragment;
import com.registration.constants.Constants;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.RecordsBean;
import com.registration.util.StringUtils;
import com.registration.view.TicketInfoDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.OnClick;

/*
 * @创建者 lai
 * @创建时间  2019/6/28 20:08
 * @描述      ${TODO}         设置
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class SettingFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.tv_operator)
    TextView     mTvOperator;
    @BindView(R.id.ll_metting)
    LinearLayout mLlMetting;
    @BindView(R.id.tv_period)
    TextView     mTvPeriod;
    @BindView(R.id.ll_period)
    LinearLayout mLlPeriod;
    @BindView(R.id.tv_checkpoint)
    TextView     mTvCheckpoint;
    @BindView(R.id.rl_checkpoint)
    LinearLayout mLlCheckpoint;
    @BindView(R.id.switch_register)
    Switch       mSwitchRegister;
    @BindView(R.id.switch_server)
    Switch       mSwitchServer;
    @BindView(R.id.tv_server)
    TextView     mTVServer;
    @BindView(R.id.btn_save)
    Button       mBtnSave;
    @BindView(R.id.tv_device)
    TextView     mTvDevice;
    @BindView(R.id.tv_metting)
    TextView     mTvMetting;
    @BindView(R.id.tv_ali_server)
    TextView     mTvAliServer;

    private TicketInfoDialog mTicketInfoDialog;

    @Override
    protected int contentViewID() {
        return R.layout.fragment_setting2;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        mTicketInfoDialog = new TicketInfoDialog();

        //设置标题，TextAppearance设置textStyle:bold无效
        collapsingToolbar.setExpandedTitleTypeface(Typeface.DEFAULT_BOLD);
        String account = Hawk.get(Constants.ACCOUNT);
        if (!TextUtils.isEmpty(account)) {
            collapsingToolbar.setTitle(account);
        }

        //设置操作人
        String operator = Hawk.get(Constants.ACCOUNT +"_"+ account);
        if (!TextUtils.isEmpty(operator)) {
            mTvOperator.setText(operator);
        }

        //设置报到台
        if (Hawk.contains(Constants.CHECKPINT)) {
            CheckPointEntity checkPointEntity = Hawk.get(Constants.CHECKPINT);
            mTvCheckpoint.setText(checkPointEntity.getCheckpointName());
        }
        //设置会议
//        if (Hawk.contains(Constants.MEETING)) {
//            RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.MEETING);
//            mTvMetting.setText(recordsBean.getAbbreviate());
//        }
        //设置界别
//        if (Hawk.contains(Constants.DIVSION)) {
//            RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
//            mTvPeriod.setText(String.valueOf(recordsBean.getDivisionYear()));
//        }
        //是否使用其他服务器
        String server = Hawk.get(Constants.URL, "");
        if (!TextUtils.isEmpty(server)) {
            mSwitchServer.setChecked(true);
            mTVServer.setText(server);
        } else {
            mSwitchServer.setChecked(false);
            mTVServer.setText(null);
        }

        //报到设备
//        if (Hawk.contains(Constants.DEVICE)) {
//            String device = (String) Hawk.get(Constants.DEVICE);
//            mTvDevice.setText(device);
//        }

        //是否允许不同报到台报到
        if (Hawk.contains(Constants.ISCHECKPOINT)) {
            mSwitchRegister.setChecked(true);
        } else {
            mSwitchRegister.setChecked(false);
        }

        mTvAliServer.setText(Constants.BASE_URL);
        //事件监听
        initSwitchListener();
    }

    private void initSwitchListener() {

        //是否允许不同报到台报到 监听
        mSwitchRegister.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Hawk.put(Constants.ISCHECKPOINT, true);
                } else {
                    if (Hawk.contains(Constants.ISCHECKPOINT)) {
                        Hawk.delete(Constants.ISCHECKPOINT);
                    }
                }
            }
        });

        //是否使用其他服务器 监听
        mSwitchServer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {  //使用外部服务器
                    String server = mTVServer.getText().toString().trim();
                    //外部服务器是否设置
                    if (StringUtils.isNotEmpty(server)) {
                        //保存外部服务器
                        Hawk.put(Constants.URL, server);
                    } else {
                        //设置为未选中
                        //mSwitchServer.setChecked(false);
                        if (!Hawk.contains(Constants.URL)) {
                            mTicketInfoDialog.showServer(getActivity(), mTVServer, getString(R.string.server_hint), mSwitchServer, true);
                        }
                    }
                } else {        //使用默认服务器
                    //删除外部服务器地址
                    if (Hawk.contains(Constants.URL)) {
                        Hawk.delete(Constants.URL);
                    }
                    //外部服务地址为空
                    mTVServer.setText(null);
                }
            }
        });
    }

    @OnClick({R.id.ll_metting, R.id.ll_period, R.id.ll_operator, R.id.rl_checkpoint, R.id.ll_server, R.id.ll_device, R.id.ll_sync_server})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            case R.id.ll_metting:           //会议
//                mTicketInfoDialog.showMettingList(getActivity(), mTvMetting, mTvPeriod, false);
//                break;
//            case R.id.ll_period:            //界别
//                mTicketInfoDialog.showMettingList(getActivity(), mTvMetting, mTvPeriod, true);
//                break;
            case R.id.ll_operator:
                mTicketInfoDialog.showOperator(getActivity(), mTvOperator, getString(R.string.operator_hint));
                break;
            case R.id.rl_checkpoint:        //报到台
                //if (Hawk.contains(Constants.DIVSION)) {
                    mTicketInfoDialog.showCheckPoint(getActivity(), mTvCheckpoint);
                //} else {
                //    mTicketInfoDialog.registCodeHint(getActivity(), getString(R.string.meting_division_hint));
                //}
                break;
            case R.id.ll_server:        //设置其他服务器
                mTicketInfoDialog.showServer(getActivity(), mTVServer, getString(R.string.server_hint), mSwitchServer, true);
                break;
//            case R.id.ll_device:        //设备名称
//                mTicketInfoDialog.showServer(getActivity(), mTvDevice, getString(R.string.device_hint), null, false);
//                break;
//            case R.id.ll_sync_server:        //同步数据
//                mTicketInfoDialog.syncTicketData(getActivity());
//                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(String message) {
        //更新报到设备信息
        mTvDevice.setText(message);
    }
}
