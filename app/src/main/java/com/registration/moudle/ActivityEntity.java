package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "activity")
public class ActivityEntity {
    /**
     * deleteFlag : 0
     * createTime : 1585719730404
     * activityName : IDA_2021
     * startTime : 1585699200000
     * updateTime : 1585719730404
     * divisionId : 26
     * endTime : 1588118400000
     * id : 8
     */
    @PrimaryKey
    public int id;
    public int deleteFlag;
    public String createTime;
    public String activityName;
    public String startTime;
    public String updateTime;
    public int divisionId;
    public String endTime;
}
