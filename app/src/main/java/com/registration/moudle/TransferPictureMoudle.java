package com.registration.moudle;

/*
 * @创建者 lai
 * @创建时间  2019/7/30 9:47
 * @描述      ${TODO}    门票图片
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class TransferPictureMoudle {
    //图片base 64
    private String base64Image;
    //meeting id
    private int    meetingId;
    //门票二维码
    private String    ticketCode;

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }
}
