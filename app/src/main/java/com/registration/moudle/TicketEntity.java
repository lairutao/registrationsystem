package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ticket")
public class TicketEntity {
    /**
     * transferFlag : 0
     * roomNumber : 安抚
     * transferProfile : null
     * boxNumber : 阿道夫
     * remark :  阿道夫
     * selfUseFlag : 1
     * delayFlag : 0
     * memberNum : D21T00001Y
     * randomCode : 3nHwAqBKN6na
     * checkInFlag : 1
     * courseFreeFlag : 0
     * tableNumber : 是v
     * deleteFlag : 0
     * lecturerId : null
     * repairFlag : 0
     * divisionId : 26
     * id : 3576
     * ciCheckpointId : 10
     * proxyUserInfoId : null
     * ownerUserInfoId : 58120
     * participationFlag : 1
     * updateTime : 1608102738142
     * ticketOrderId : 2580
     * abstainedFlag : 0
     * lastCheckTime : null
     * createTime : 1585721655970
     * passwd : 867381
     * identityId : 20
     * refundFlag : 0
     * ciGroupId : 0
     * ciActivityId : 8
     */
    @PrimaryKey
    public int id;
    public int transferFlag;
    public String roomNumber;
    public String transferProfile;
    public String boxNumber;
    public String remark;
    public int selfUseFlag;
    public int delayFlag;
    public String memberNum;
    public String randomCode;
    public int checkInFlag;
    public int courseFreeFlag;
    public String tableNumber;
    public int deleteFlag;
    public String lecturerId;
    public int repairFlag;
    public int divisionId;
    public int ciCheckpointId;
    public String proxyUserInfoId;
    public int ownerUserInfoId;
    public int participationFlag;
    public String updateTime;
    public int ticketOrderId;
    public int abstainedFlag;
    public String lastCheckTime;
    public String createTime;
    public String passwd;
    public int identityId;
    public int refundFlag;
    public int ciGroupId;
    public int ciActivityId;
    public int syncFlag;
}
