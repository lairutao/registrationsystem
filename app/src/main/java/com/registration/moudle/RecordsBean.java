package com.registration.moudle;

/*
 * @创建者 lai
 * @创建时间  2019/7/24 15:04
 * @描述      ${TODO}         会议
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class RecordsBean {
    /**
     * abbreviate : WCLIC
     * description : null
     * id : 1
     * meetingName : 世界华人保险大会
     * meetingUrl : null
     * num : 100
     * picture : https://ida-app.oss-cn-hongkong.aliyuncs.com/static/meeting/meetingTheme/2020.jpg
     * topFlag : 0
     * "createTime": "1",
     * "description": null,
     * "divisionCode": "D2",
     * "divisionYear": 2001,
     * "endTime": "1",
     * "id": 6,
     * "localTimeZone": null,
     * "location": "澳门",
     * "meetingId": 2,
     * "meetingUrl": null,
     * "picture": "https://ida-app.oss-cn-hongkong.aliyuncs.com/static/meeting/meetingTheme/2001.jpg",
     * "startTime": "1",
     * "title": "2001国际龙奖IDA年会",
     * "updateTime": "1"
     */
    //类别
    private String  abbreviate;
    //描述
    private String  description;
    //id
    private int     id;
    //会议名字
    private String  meetingName;
    //会议的 网址
    private String  meetingUrl;
    private int     num;
    //图片
    private String  picture;
    private int     topFlag;
    //开始时间
    private String  createTime;
    //届数code
    private String  divisionCode;
    //届数年份
    private Integer divisionYear;
    //结束时间
    private String  endTime;
    private String  localTimeZone;
    //地区
    private String  location;
    //会议id
    private int     meetingId;
    //开始时间
    private String  startTime;
    //标题
    private String  title;
    //更新时间
    private String  updateTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public Integer getDivisionYear() {
        return divisionYear;
    }

    public void setDivisionYear(Integer divisionYear) {
        this.divisionYear = divisionYear;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLocalTimeZone() {
        return localTimeZone;
    }

    public void setLocalTimeZone(String localTimeZone) {
        this.localTimeZone = localTimeZone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getAbbreviate() {
        return abbreviate;
    }

    public void setAbbreviate(String abbreviate) {
        this.abbreviate = abbreviate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public String getMeetingUrl() {
        return meetingUrl;
    }

    public void setMeetingUrl(String meetingUrl) {
        this.meetingUrl = meetingUrl;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getTopFlag() {
        return topFlag;
    }

    public void setTopFlag(int topFlag) {
        this.topFlag = topFlag;
    }
}
