package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "division")
public class DivisionEntity {
    /**
     * meetingId : 1
     * id : 3
     * title : 第一届世界华人保险大会
     * divisionYear : 1996
     */
    @PrimaryKey
    public int id;
    public int meetingId;
    public String title;
    public int divisionYear;
}
