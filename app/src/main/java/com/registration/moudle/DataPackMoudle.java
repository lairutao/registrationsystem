package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.registration.database.EntityConverter;

import java.util.List;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/26 9:40
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Entity(tableName = "datapack")
public class DataPackMoudle {

    @PrimaryKey(autoGenerate = true)
    public int id; //自增主键

    @TypeConverters(EntityConverter.class)
    public List<CheckPointEntity> checkpointList; //报道台列表
    @TypeConverters(EntityConverter.class)
    public List<AppUserPrizeRelationEntity> appUserPrizeRelationList; //用户与奖项关系列表
    @TypeConverters(EntityConverter.class)
    public List<TicketEntity> ticketList; //门票列表
    @TypeConverters(EntityConverter.class)
    public List<DivisionEntity> divisionList; //界别
    @TypeConverters(EntityConverter.class)
    public List<CompanyEntity> companyList; //公司列表
    @TypeConverters(EntityConverter.class)
    public List<IdentityEntity> identityList; //身份列表
    @TypeConverters(EntityConverter.class)
    public List<UserInfoEntity> userInfoList; //用户信息
    @TypeConverters(EntityConverter.class)
    public List<AreaEntity> areaList; //地区列表
    @TypeConverters(EntityConverter.class)
    public List<PrizeEntity> prizeList; //奖项列表
    @TypeConverters(EntityConverter.class)
    public List<GroupEntity> groupList; //报道团队列表
    @TypeConverters(EntityConverter.class)
    public List<ActivityEntity> activityList; //活动列表

    public String syncTime; //(服务器)更新时间

    public void setSyncTime(String syncTime) {
        this.syncTime = syncTime;
    }

    public String getSyncTime() {
        return syncTime;
    }
}
