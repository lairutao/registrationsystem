package com.registration.moudle;

/*
 * @创建者 huangxy
 * @创建时间  2020/2/1 9:40
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class CheckInDetailMoudle {

    public int ticketId; //@Table = "ticket".id
    public String remark; //@Table = "ticket"
    public String randomCode; //@Table = "ticket"
    public String transferProfile; //@Table = "ticket"

    public int identityId; //@Table = "ticket".identityId -- @Table = "identity".id
    public String identityName; //@Table = "identity"

    public int checkpointId; //@Table = "ticket".ciCheckpointId -- @Table = "checkpoint".id
    public String checkpointName; //@Table = "checkpoint"

    public int groupMemberNum; //COUNT(@Table = "group")
    public int groupCheckpointId; //@Table = "group".ciCheckpointId
    public String groupCheckpointName; //@Table = "checkpoint".checkpointName
    public String groupName; //@Table = "checkpoint"

    public int teamGroupId; //@Table = "group".id
    public int leaderGroupId; //@Table = "ticket".ciGroupId
    public String leaderName; //@Table = "userInfo".chineseName

//    public int userId; //@Table = "ticket".ownerUserInfoId -- @Table = "userInfo".id

//    public String documentNumber; //@Table = "userInfo"
//    public String chineseName; //@Table = "userInfo"
//    public String mobile; //@Table = "userInfo"
//    public String email; //@Table = "userInfo"

//    public int groupId; //@Table = "ticket".ciGroupId -- @Table = "group".id

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRandomCode() {
        return randomCode;
    }

    public void setRandomCode(String randomCode) {
        this.randomCode = randomCode;
    }

    public String getTransferProfile() {
        return transferProfile;
    }

    public void setTransferProfile(String transferProfile) {
        this.transferProfile = transferProfile;
    }

    public int getIdentityId() {
        return identityId;
    }

    public void setIdentityId(int identityId) {
        this.identityId = identityId;
    }

    public String getIdentityName() {
        return identityName;
    }

    public void setIdentityName(String identityName) {
        this.identityName = identityName;
    }

    public int getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(int checkpointId) {
        this.checkpointId = checkpointId;
    }

    public String getCheckpointName() {
        return checkpointName;
    }

    public void setCheckpointName(String checkpointName) {
        this.checkpointName = checkpointName;
    }

    public int getGroupMemberNum() {
        return groupMemberNum;
    }

    public void setGroupMemberNum(int groupMemberNum) {
        this.groupMemberNum = groupMemberNum;
    }

    public int getGroupCheckpointId() {
        return groupCheckpointId;
    }

    public void setGroupCheckpointId(int groupCheckpointId) {
        this.groupCheckpointId = groupCheckpointId;
    }

    public String getGroupCheckpointName() {
        return groupCheckpointName;
    }

    public void setGroupCheckpointName(String groupCheckpointName) {
        this.groupCheckpointName = groupCheckpointName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getTeamGroupId() {
        return teamGroupId;
    }

    public void setTeamGroupId(int teamGroupId) {
        this.teamGroupId = teamGroupId;
    }

    public int getLeaderGroupId() {
        return leaderGroupId;
    }

    public void setLeaderGroupId(int leaderGroupId) {
        this.leaderGroupId = leaderGroupId;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public int getSubId(boolean isTeam) {
        if (isTeam) {
            return teamGroupId;
        } else {
            return ticketId;
        }
    }

    public int getCheckpointId(boolean isTeam) {
        if (isTeam) {
            return groupCheckpointId;
        } else {
            return checkpointId;
        }
    }

    public String getCheckpointName(boolean isTeam) {
        if (isTeam) {
            return groupCheckpointName;
        } else {
            return checkpointName;
        }
    }

//    public boolean isLeaderAndMember(int ciGroupId) {
//        return (leaderGroupId > 0) && (leaderGroupId == ciGroupId);
//    }
}
