package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "userInfo")
public class UserInfoEntity {
    /**
     * englishName : Gail Peng
     * agency : null
     * unitName : 宏寶
     * documentType : 1
     * agentCode :
     * documentNumber : E220442921
     * foodType : null
     * sex : 2
     * mobile : 0928108569
     * updateTime : 1
     * deleteFlag : 0
     * companyId : 32
     * areaId : 15
     * createTime : 0
     * rankName : 業務區經理
     * chineseName : 彭起端
     * id : 58120
     * telephoneCode : 886
     * email : gailpeng@gmail.com
     */
    @PrimaryKey
    public int id;
    public String englishName;
    public String agency;
    public String unitName;
    public int documentType;
    public String agentCode;
    public String documentNumber;
    public String foodType;
    public int sex;
    public String mobile;
    public String updateTime;
    public int deleteFlag;
    public int companyId;
    public int areaId;
    public String createTime;
    public String rankName;
    public String chineseName;
    public String telephoneCode;
    public String email;
}
