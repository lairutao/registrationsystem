package com.registration.moudle;

import android.text.TextUtils;

import androidx.room.Ignore;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/26 9:40
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class RemarkEntity {

    /**
     * transferProfile : string
     * remark : string
     * ticketId : 0
     */

    public int ticketId;
    public String remark;
    public String transferProfile; //base64Str

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getRemark() {
        return !TextUtils.isEmpty(remark)? remark: "";
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTransferProfile() {
        return !TextUtils.isEmpty(transferProfile)? transferProfile: "";
    }

    public void setTransferProfile(String transferProfile) {
        this.transferProfile = transferProfile;
    }

    public RemarkEntity() {

    }

    @Ignore
    public RemarkEntity(int ticketId, String transferProfile, String remark) {
        this.remark = remark;
        this.ticketId = ticketId;
        this.transferProfile = transferProfile;
    }
}
