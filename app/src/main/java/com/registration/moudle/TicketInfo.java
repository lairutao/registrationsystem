package com.registration.moudle;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.io.Serializable;
import java.util.List;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/*
 * @创建者 lai
 * @创建时间  2019/7/11 17:10
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
@Entity(tableName = "ticketInfo")
public class TicketInfo implements Serializable, MultiItemEntity {

    /**
     * areaCode : CS
     * areaId : 16
     * checkpointId : 11
     * companyName : 新华人寿
     * courseFreeFlag : 0
     * createTime : 1561686883330
     * deleteFlag : 0
     * divisionId : 24
     * email : null
     * id : 994
     * identityName :
     * leaderFlag : 0
     * memberNum : D19C01615Yc
     * mobile : 18951858898
     * participationFlag : 1
     * passwd : 707865
     * qrcode : https://app.idaonline.cn/#/appdown?code=30lu979pjfdu2wl4
     * remark : null
     * reportTeamId : 693
     * teamCode : null
     * telephoneCode : null
     * ticketCode : 30lu979pjfdu2wl4
     * tip : 会员 钧启
     * updateTime : 1561686883330
     * username : 王霞
     */

    //地区代号
    private String        areaCode;
    //地区id
    private int           areaId;
    //报到台id
    private int           checkpointId;
    //公司
    private String        companyName;
    //是否免选课
    private int           courseFreeFlag;
    //创建时间
    private String        createTime;
    //删除标志位
    private int           deleteFlag;
    //界别id
    private int           divisionId;
    //email
    private String        email;
    @PrimaryKey
    private int           id;
    //身份名称
    private String        identityName;
    //是否是团队长   0 不是  1 是
    private int           leaderFlag;
    //会员编号
    private String        memberNum;
    //手机
    private String        mobile;
    //是否参会，1参会，0不参会
    private int           participationFlag;
    //验证码
    private String        passwd;
    //二维码url
    private String        qrcode;
    //备注
    private String        remark;
    //团队id
    private int           reportTeamId;
    //团队二维码
    private String        teamCode;
    //电话code
    private String        telephoneCode;
    //随机码
    private String        ticketCode;
    //提示语
    private String        tip;
    //更新时间
    private String        updateTime;
    //用户姓名
    private String        username;
    //团队名称
    private String        reportTeamName;
    //报到台
    private String        checkpointName;
    //报到  1已报到  0，null未报到
    private Integer       checkedFlag;
    //类型
    @Ignore  //忽略
    private int           itemtype;
    @Ignore
    private List<Integer> checkpointIdList;
    //团队id 报到台不同
    private String        checkpointIdList2;
    //备注图片
    private String        transferPicture;

    public TicketInfo(TicketInfo ticketInfo, int type) {
        this.itemtype = type;
        this.areaCode = ticketInfo.getAreaCode();
        this.areaId = ticketInfo.getAreaId();
        this.checkpointId = ticketInfo.getCheckpointId();
        this.companyName = ticketInfo.getCompanyName();
        this.courseFreeFlag = ticketInfo.getCourseFreeFlag();
        this.createTime = ticketInfo.getCreateTime();
        this.deleteFlag = ticketInfo.getDeleteFlag();
        this.divisionId = ticketInfo.getDivisionId();
        this.email = ticketInfo.getEmail();
        this.id = ticketInfo.getId();
        this.identityName = ticketInfo.getIdentityName();
        this.leaderFlag = ticketInfo.getLeaderFlag();
        this.memberNum = ticketInfo.getMemberNum();
        this.mobile = ticketInfo.getMobile();
        this.participationFlag = ticketInfo.getParticipationFlag();
        this.passwd = ticketInfo.getPasswd();
        this.qrcode = ticketInfo.getQrcode();
        this.remark = ticketInfo.getRemark();
        this.reportTeamId = ticketInfo.getReportTeamId();
        this.teamCode = ticketInfo.getTeamCode();
        this.telephoneCode = ticketInfo.getTelephoneCode();
        this.ticketCode = ticketInfo.getTicketCode();
        this.tip = ticketInfo.getTip();
        this.updateTime = ticketInfo.getUpdateTime();
        this.username = ticketInfo.getUsername();
        this.reportTeamName = ticketInfo.getReportTeamName();
        this.checkpointName = ticketInfo.getCheckpointName();
        this.checkedFlag = ticketInfo.getCheckedFlag();
        this.checkpointIdList2=ticketInfo.getCheckpointIdList2();
        this.transferPicture=ticketInfo.getTransferPicture();
    }

    public TicketInfo() {
    }

    public String getTransferPicture() {
        return transferPicture;
    }

    public void setTransferPicture(String transferPicture) {
        this.transferPicture = transferPicture;
    }

    public List<Integer> getCheckpointIdList() {
        return checkpointIdList;
    }

    public void setCheckpointIdList(List<Integer> checkpointIdList) {
        this.checkpointIdList = checkpointIdList;
    }

    public String getCheckpointIdList2() {
        return checkpointIdList2;
    }

    public void setCheckpointIdList2(String checkpointIdList2) {
        this.checkpointIdList2 = checkpointIdList2;
    }

    public Integer getCheckedFlag() {
        return checkedFlag;
    }

    public void setCheckedFlag(Integer checkedFlag) {
        this.checkedFlag = checkedFlag;
    }

    public String getReportTeamName() {
        return reportTeamName;
    }

    public void setReportTeamName(String reportTeamName) {
        this.reportTeamName = reportTeamName;
    }

    public String getCheckpointName() {
        return checkpointName;
    }

    public void setCheckpointName(String checkpointName) {
        this.checkpointName = checkpointName;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public int getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(int checkpointId) {
        this.checkpointId = checkpointId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getCourseFreeFlag() {
        return courseFreeFlag;
    }

    public void setCourseFreeFlag(int courseFreeFlag) {
        this.courseFreeFlag = courseFreeFlag;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentityName() {
        return identityName;
    }

    public void setIdentityName(String identityName) {
        this.identityName = identityName;
    }

    public int getLeaderFlag() {
        return leaderFlag;
    }

    public void setLeaderFlag(int leaderFlag) {
        this.leaderFlag = leaderFlag;
    }

    public String getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(String memberNum) {
        this.memberNum = memberNum;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getParticipationFlag() {
        return participationFlag;
    }

    public void setParticipationFlag(int participationFlag) {
        this.participationFlag = participationFlag;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getReportTeamId() {
        return reportTeamId;
    }

    public void setReportTeamId(int reportTeamId) {
        this.reportTeamId = reportTeamId;
    }

    public String getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    public String getTelephoneCode() {
        return telephoneCode;
    }

    public void setTelephoneCode(String telephoneCode) {
        this.telephoneCode = telephoneCode;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "TicketInfo{" +
                ", companyName='" + companyName + '\'' +
                ", deleteFlag=" + deleteFlag +
                ", email='" + email + '\'' +
                ", id=" + id +
                ", identityName='" + identityName + '\'' +
                ", memberNum='" + memberNum + '\'' +
                ", mobile='" + mobile + '\'' +
                ", qrcode='" + qrcode + '\'' +
                ", tip='" + tip + '\'' +
                ", ticketCode='" + ticketCode + '\'' +
                ", username='" + username + '\'' +
                ", reportTeamName='" + reportTeamName + '\'' +
                ", checkpointName='" + checkpointName + '\'' +
                '}';
    }

    @Override
    public int getItemType() {
        return itemtype;
    }
}
