package com.registration.moudle;

/*
 * @创建者 lai
 * @创建时间  2019/7/19 14:24
 * @描述      ${TODO}             相机发送消息刷新
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class MessageMoudle {
    private int    type;
    private String message;

    public MessageMoudle(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
