package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "identity")
public class IdentityEntity {
    /**
     * identityName : 学员
     * id : 20
     */
    @PrimaryKey
    public int id;
    public String identityName;
}
