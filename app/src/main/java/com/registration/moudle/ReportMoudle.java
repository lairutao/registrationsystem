package com.registration.moudle;

/*
 * @创建者 admin
 * @创建时间  2019/7/16 14:11
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class ReportMoudle {

    /**
     * checkpointId : 0
     * createTime : 0
     * device : string
     * divisionId : 0
     * id : 0
     * successFlag : 0
     * teamId : 0
     * ticketId : 0
     */

    private int checkpointId;
    private String    createTime;
    private String device;
    private int    divisionId;
    private int    id;
    private int    successFlag;
    private int    teamId;
    private int    ticketId;

    public int getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(int checkpointId) {
        this.checkpointId = checkpointId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(int successFlag) {
        this.successFlag = successFlag;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }
}
