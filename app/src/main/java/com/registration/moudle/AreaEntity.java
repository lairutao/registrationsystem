package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "area")
public class AreaEntity {
    /**
     * areaName : 台灣
     * id : 15
     */
    @PrimaryKey
    public int id;
    public String areaName;
}
