package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "prize")
public class PrizeEntity {
    /**
     * prizeName : 优秀主管白金奖
     * id : 1
     */
    @PrimaryKey
    public int id;
    public String prizeName;
}
