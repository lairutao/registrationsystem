package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "company")
public class CompanyEntity {
    /**
     * companyName : AIA International Limited
     * id : 1
     */
    @PrimaryKey
    public int id;
    public String companyName;
}
