package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "relation")
public class AppUserPrizeRelationEntity {
    /**
     * deleteFlag : 0
     * createTime : 1581558094261
     * participationFlag : 0
     * updateTime : 1581558094261
     * divisionId : 26
     * id : 20134
     * appUserInfoId : 13175
     * memberNum : W13H00322Nc
     * picture : https://imm-oss-app.idaonline.cn/IDAApply/2020/18f19211870b474bbe1a7f7e1813d1de.jpeg
     * prizeId : 2
     */
    @PrimaryKey
    public int id;
    public int deleteFlag;
    public String createTime;
    public int participationFlag;
    public String updateTime;
    public int divisionId;
    public int appUserInfoId;
    public String memberNum;
    public String picture;
    public int prizeId;
}
