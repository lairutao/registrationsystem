package com.registration.moudle;

/*
 * @创建者 huangxy
 * @创建时间  2020/2/1 9:40
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class CheckInListMoudle {

    public String documentNumber; //@Table = "userInfo"
    public String chineseName; //@Table = "userInfo"
    public String mobile; //@Table = "userInfo"
    public String email; //@Table = "userInfo"

    public int companyId; //@Table = "userInfo".companyId -- @Table = "company".id
    public String companyName; //@Table = "company"

    public int userId; //@Table = "userInfo".id -- @Table = "ticket".ownerUserInfoId

    public int ticketId; //@Table = "ticket".id
    public int ticketCheckInFlag; //@Table = "ticket".checkInFlag
    public String randomCode; //@Table = "ticket"

    public int groupId; //@Table = "ticket".ciGroupId -- @Table = "group".id
    public int leaderCheckInFlag; //@Table = "group".checkInFlag
    public int leaderTicketId; //@Table = "group"

    public int teamType; //报道类型，0个人，1团队(领队/团员)，2团队(领队非团员)

    public int getTeamType() {
        return teamType;
    }

    public void setTeamType(int teamType) {
        this.teamType = teamType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getChineseName() {
        return chineseName;
    }

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getTicketCheckInFlag() {
        return ticketCheckInFlag;
    }

    public void setTicketCheckInFlag(int ticketCheckInFlag) {
        this.ticketCheckInFlag = ticketCheckInFlag;
    }

    public String getRandomCode() {
        return randomCode;
    }

    public void setRandomCode(String randomCode) {
        this.randomCode = randomCode;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getLeaderCheckInFlag() {
        return leaderCheckInFlag;
    }

    public void setLeaderCheckInFlag(int leaderCheckInFlag) {
        this.leaderCheckInFlag = leaderCheckInFlag;
    }

    public int getLeaderTicketId() {
        return leaderTicketId;
    }

    public void setLeaderTicketId(int leaderTicketId) {
        this.leaderTicketId = leaderTicketId;
    }

    //团队报道
    public boolean isTeam() {
        return teamType > 0;
    }

    //报道类型，0个人，1团队
    public int getCiType() {
        return (teamType > 0)? 1: 0;
    }

    public String getGroupName() {
        return isTeam() ? "团队报道": "个人报道";
    }

    //是否是团员
    public boolean isGroup() {
        return groupId > 0;
    }

    //是否是领队
    public boolean isLeader() {
        return (groupId == 0) || (ticketId == leaderTicketId);
    }

    public String getTeamRole() {
        return isTeam()? (isLeader() ? "领队": "团员"): "";
    }

    //(个人报道)是否已报到
    public boolean isSelfCheckIn() {
        return ticketCheckInFlag > 0;
    }

    //(团队报道)是否已报到
    public boolean isLeaderCheckIn() {
        return isTeam() && (leaderCheckInFlag > 0);
    }

    //用户是否已报到
    public boolean isCheckIn() {
        if (isTeam()) {
            return isLeaderCheckIn();
        } else {
            return isSelfCheckIn();
        }
    }

    public String getCheckInStatus() {
        if (isTeam()) {
            if (isLeaderCheckIn()) {
                return isLeader() ? "团队已报到": "领队已报到";
            } else {
                return isLeader() ? "未报到": "请领队报到";
            }
        } else {
            return isSelfCheckIn() ? "已报到": "未报到";
        }
    }

    public String getCheckInStatus2() {
        if (isTeam()) {
            if (isLeaderCheckIn()) {
                return isLeader() ? "团队已报到": "领队已报到";
            } else {
                return isLeader() ? "该用户未报到": "请领队报到";
            }
        } else {
            return isSelfCheckIn() ? "该用户已报到": "该用户未报到";
        }
    }
}
