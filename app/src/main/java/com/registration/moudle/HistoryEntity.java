package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/26 9:40
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Entity(tableName = "history")
public class HistoryEntity {

    /**
     * subId : 0
     * ciTicketId : 0
     * ciSysUserId : 0
     * remark : string
     * serverTime : 0
     * id : 0
     * clientTime : 0
     * ciType : 0
     * successFlag : 0
     */

    @PrimaryKey(autoGenerate = true)
    public int id;
    public int subId; //个人报道为门票id，团队报道为团队id
    public int ciTicketId;
    public int ciSysUserId = 0; //签到工作人员的ID，客户端不用传
    public long serverTime = 0; //服务端时间，客户端不用传
    public long clientTime; //客户端时间
    public int ciType; //报道类型，0个人，1团队
    public int successFlag; //签到是否成功
    public String remark;

    public String ciUserName; //参会人员
    public int syncFlag; //签到是否已同步，0已同步，1未同步

    public HistoryEntity() {

    }

    @Ignore
    public HistoryEntity(int subId, int ciTicketId, int ciType, String ciUserName, String remark, int successFlag, long clientTime) {
        this.subId = subId;
        this.ciUserName = ciUserName;
        this.ciTicketId = ciTicketId;
        this.ciType = ciType;
        this.clientTime = clientTime;
        this.remark = remark;
        this.successFlag = successFlag;
        this.syncFlag = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public int getCiTicketId() {
        return ciTicketId;
    }

    public void setCiTicketId(int ciTicketId) {
        this.ciTicketId = ciTicketId;
    }

    public int getCiSysUserId() {
        return ciSysUserId;
    }

    public void setCiSysUserId(int ciSysUserId) {
        this.ciSysUserId = ciSysUserId;
    }

    public long getServerTime() {
        return serverTime;
    }

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    public long getClientTime() {
        return clientTime;
    }

    public void setClientTime(long clientTime) {
        this.clientTime = clientTime;
    }

    public int getCiType() {
        return ciType;
    }

    public void setCiType(int ciType) {
        this.ciType = ciType;
    }

    public String getCiUserName() {
        return ciUserName;
    }

    public void setCiUserName(String ciUserName) {
        this.ciUserName = ciUserName;
    }

    public int getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(int successFlag) {
        this.successFlag = successFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getSyncFlag() {
        return syncFlag;
    }

    public void setSyncFlag(int syncFlag) {
        this.syncFlag = syncFlag;
    }

    public boolean isSuccess() {
        return successFlag > 0;
    }

    public boolean isTeamCheckIn() {
        return ciType > 0;
    }
}
