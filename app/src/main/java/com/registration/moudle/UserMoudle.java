package com.registration.moudle;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/*
 * @创建者 lai
 * @创建时间  2019/7/5 9:40
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
@Entity(tableName = "user")
public class UserMoudle {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private int     id;
    //姓名
    private String  name;
    //参会编号
    private String  participantNumber;
    //餐盒
    private String  lunchBox;
    //电话
    private String  phone;
    //email
    private String  email;
    //公司
    private String  company;
    //性别
    private int     sex;
    //身份证
    private String  IdNumber;
    //地址
    private String  address;
    //微信号
    private String  wechatId;
    //code
    private String  code;
    //签到
    private boolean isSignin;
    //报到时间
    private String  registrationTime;
    //报到设备
    private String  device;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParticipantNumber() {
        return participantNumber;
    }

    public void setParticipantNumber(String participantNumber) {
        this.participantNumber = participantNumber;
    }

    public String getLunchBox() {
        return lunchBox;
    }

    public void setLunchBox(String lunchBox) {
        this.lunchBox = lunchBox;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getIdNumber() {
        return IdNumber;
    }

    public void setIdNumber(String idNumber) {
        IdNumber = idNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSignin() {
        return isSignin;
    }

    public void setSignin(boolean signin) {
        isSignin = signin;
    }

    public String getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }


    @Override
    public String toString() {
        return "UserMoudle{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", participantNumber='" + participantNumber + '\'' +
                ", lunchBox='" + lunchBox + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", company='" + company + '\'' +
                ", sex=" + sex +
                ", IdNumber='" + IdNumber + '\'' +
                ", address='" + address + '\'' +
                ", wechatId='" + wechatId + '\'' +
                ", code='" + code + '\'' +
                ", isSignin=" + isSignin +
                ", registrationTime='" + registrationTime + '\'' +
                ", device='" + device + '\'' +
                '}';
    }
}
