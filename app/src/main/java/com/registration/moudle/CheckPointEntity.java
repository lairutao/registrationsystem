package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "checkpoint")
public class CheckPointEntity {
    /**
     * deleteFlag : 0
     * checkpointName : 大會報道台
     * createTime : 1586323634867
     * tip : 123
     * updateTime : 1586323634867
     * id : 9
     * ciActivityId : 8
     */
    @PrimaryKey
    public int id;
    public int deleteFlag;
    public String checkpointName;
    public String createTime;
    public String updateTime;
    public int ciActivityId;
    public String tip;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCheckpointName() {
        return checkpointName;
    }

    public void setCheckpointName(String checkpointName) {
        this.checkpointName = checkpointName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getCiActivityId() {
        return ciActivityId;
    }

    public void setCiActivityId(int ciActivityId) {
        this.ciActivityId = ciActivityId;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }
}
