package com.registration.moudle;

import java.util.List;

/*
 * @创建者 lai
 * @创建时间  2019/7/18 15:07
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class SyncTicketList {
    private int              checkpointId;
    private String           device;
    private int              meetingId;
    private List<TicketList> ticketList;

    public int getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(int checkpointId) {
        this.checkpointId = checkpointId;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public List<TicketList> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<TicketList> ticketList) {
        this.ticketList = ticketList;
    }

    public static class TicketList {
        private Integer k;     //门票id
        private String  v;      //报到时间

        public TicketList(int empId, String time) {
            this.k = empId;
            this.v = time;
        }

        public Integer getK() {
            return k;
        }

        public void setK(Integer k) {
            this.k = k;
        }

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }
    }


}
