package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
/*
 * @创建者 lai
 * @创建时间  2019/7/11 10:05
 * @描述      ${TODO}         签到
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
@Entity(tableName = "signIn")
public class SignInMoudle {
    //主键id
    @PrimaryKey(autoGenerate = true)
    private int id;
    //签到时间
    private String time;
    //报到设备
    private String device;
    //外键id
    private int empId;

    public  SignInMoudle(String time,String device,int empId){
        this.time=time;
        this.device=device;
        this.empId=empId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
