package com.registration.moudle;

import java.util.List;

/*
 * @创建者 lai
 * @创建时间  2019/7/24 15:03
 * @描述      ${TODO}         会议
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class MettingMoudle {
    /**
     * current : 1
     * pages : 1
     * records : [{"abbreviate":"WCLIC","description":null,"id":1,"meetingName":"世界华人保险大会","meetingUrl":null,"num":100,"picture":"https://ida-app.oss-cn-hongkong.aliyuncs.com/static/meeting/meetingTheme/2020.jpg","topFlag":0},{"abbreviate":"IDA","description":"金卡戴珊付款交水电费很快乐撒是你发顺丰了\n羧甲淀粉钠开始接后方可零售价\n说服力可设计开发拉丝机\n时空裂缝哪里生解放南路 skdfm  莱克斯诺福建水泥肯定付 是的福克斯交电话费健康树 \n司法鉴定克里斯多夫零售价费咯是","id":2,"meetingName":"国际龙奖IDA年会","meetingUrl":"https://app.idaonline.cn/#/meeting_intro/ida","num":90,"picture":"https://ida-app.oss-cn-hongkong.aliyuncs.com/static/meeting/meetingTheme/2019.jpg","topFlag":1}]
     * size : 10
     * total : 2
     */

    //当前页数
    private int current;
    //当前页
    private String            pages;
    //数据个数
    private int               size;
    //条数
    private String            total;
    //会议集合
    private List<RecordsBean> records;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<RecordsBean> getRecords() {
        return records;
    }

    public void setRecords(List<RecordsBean> records) {
        this.records = records;
    }
}
