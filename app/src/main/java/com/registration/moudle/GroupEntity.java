package com.registration.moudle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "group")
public class GroupEntity {
    /**
     * deleteFlag : 0
     * immCompanyId : null
     * groupName : 台湾团队1
     * lastCheckTime : null
     * areaId : 15
     * createTime : 1586421141010
     * updateTime : 1607668909962
     * id : 3
     * ciActivityId : 8
     * checkInFlag : 0
     * ciCheckpointId : 11
     * leaderTicketId : 6056
     */
    @PrimaryKey
    public int id;
    public int deleteFlag;
    public String immCompanyId;
    public String groupName;
    public String lastCheckTime;
    public int areaId;
    public String createTime;
    public String updateTime;
    public int ciActivityId;
    public int checkInFlag;
    public int ciCheckpointId;
    public int leaderTicketId;
}
