package com.registration.moudle;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/*
 * @创建者 lai
 * @创建时间  2019/7/30 11:07
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class DialogMoudle implements MultiItemEntity {

    private int    itemtype;
    private String content;
    private String title;

    public DialogMoudle(int itemtype, String title, String content) {
        this.itemtype = itemtype;
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getItemtype() {
        return itemtype;
    }

    public void setItemtype(int itemtype) {
        this.itemtype = itemtype;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int getItemType() {
        return itemtype;
    }
}
