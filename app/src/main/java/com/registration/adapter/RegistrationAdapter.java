package com.registration.adapter;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.registration.R;
import com.registration.activity.TeamRegistionctivity;
import com.registration.database.AppDatabase;
import com.registration.moudle.SignInMoudle;
import com.registration.moudle.TicketInfo;
import com.registration.util.IdaMemberNumBuffer;
import com.registration.util.StringUtils;
import com.registration.util.TimeUtil;
import com.registration.view.TicketInfoDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/*
 * @创建者 lai
 * @创建时间  2019/7/9 14:49
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class RegistrationAdapter extends BaseMultiItemQuickAdapter<TicketInfo, BaseViewHolder> {
    private              List<TicketInfo>                               mTicketInfos;
    private static final int                                            TYPE_HEAD         = 1;        //团队长信息
    private static final int                                            TYPE_BODY         = 0;        //普通信息
    private              BaseQuickAdapter<SignInMoudle, BaseViewHolder> mSignAdapter;
    private              List<SignInMoudle>                             mSignInMoudleList = new ArrayList<>();

    public RegistrationAdapter(@Nullable List<TicketInfo> data, List<SignInMoudle> leaderList) {
        super(data);
        addItemType(TYPE_HEAD, R.layout.item_team_head);
        addItemType(TYPE_BODY, R.layout.item_list);
        this.mTicketInfos = data;
        this.mSignInMoudleList = leaderList;
    }

    @Override
    protected void convert(BaseViewHolder helper, TicketInfo item) {
        switch (helper.getItemViewType()) {
            case TYPE_BODY:
                helper.setText(R.id.tv_user_name, item.getUsername());
                helper.setText(R.id.tv_user_memberNum, "(" + item.getMemberNum() + ")");
                helper.setText(R.id.tv_user_companyname, item.getCompanyName());
                helper.setText(R.id.tv_user_mobile, item.getMobile());
                if (item.getCheckedFlag() == 1) {       //是否已签到
                    helper.setVisible(R.id.iv_issignin, true);
                } else {
                    helper.setVisible(R.id.iv_issignin, false);
                }
                if (item.getLeaderFlag() == 1) {        //是否是团队长  设置背景颜色
                    Log.d("test", "团队长---------" + mTicketInfos.get(helper.getAdapterPosition()));
                    helper.setBackgroundColor(R.id.rl_layout, mContext.getResources().getColor(R.color.gold));
                } else {
                    helper.setBackgroundColor(R.id.rl_layout, mContext.getResources().getColor(R.color.white));
                }
                //奖项
                String awards = IdaMemberNumBuffer.getPattern(item.getMemberNum());
                TextView awardstextView = helper.getView(R.id.tv_user_awards);
                if (StringUtils.isNotEmpty(awards)) {
                    awardstextView.setText(awards);
                } else {
                    awardstextView.setText("");
                }
                //团队名
                if (StringUtils.isNotEmpty(item.getReportTeamName())) {
                    helper.setText(R.id.tv_user_team, "团队名：" + item.getReportTeamName());
                } else {
                    helper.setText(R.id.tv_user_team, "");
                }
                break;
            case TYPE_HEAD:  //团队签到  团队长信息
                helper.setText(R.id.tv_team_username, item.getUsername() + "   " + item.getMemberNum())
                        .setText(R.id.tv_team_name, item.getReportTeamName())
                        .setText(R.id.tv_team_checkpoint, item.getCheckpointName())
                        .setText(R.id.tv_team_moblie, item.getMobile())
                        .setText(R.id.tv_team_conpanyname, item.getCompanyName());
                StringBuffer stringBuffer=new StringBuffer();
                TextView textView=helper.getView(R.id.tv_team_remark);
                if (StringUtils.isNotEmpty(item.getMemberNum())){
                    String remark = IdaMemberNumBuffer.getPattern(item.getMemberNum());
                    stringBuffer.append(remark);
                }
                if (StringUtils.isNotEmpty(item.getIdentityName())){
                    stringBuffer.append(""+item.getIdentityName());
                }
                textView.setText(stringBuffer.toString());
                /*if (StringUtils.isNotEmpty(item.getMemberNum())&& StringUtils.isNotEmpty(item.getIdentityName())) {
                    String remark = IdaMemberNumBuffer.getPattern(item.getMemberNum());
                    helper.setText(R.id.tv_team_remark, remark+" "+item.getIdentityName());
                } else {
                    helper.setText(R.id.tv_team_remark, "无");
                }*/
                //签到图标
                ImageView leadercheck = helper.getView(R.id.iv_leader_sign);

                if (item.getCheckedFlag() == 1) {       //是否签到
                    leadercheck.setVisibility(View.VISIBLE);
                } else {
                    leadercheck.setVisibility(View.GONE);
                }

                RecyclerView signrecyclerView = helper.getView(R.id.sign_recyclerview);
                signrecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                mSignAdapter = new BaseQuickAdapter<SignInMoudle, BaseViewHolder>(R.layout.item_list_dialog_header, mSignInMoudleList) {
                    @Override
                    protected void convert(BaseViewHolder helper, SignInMoudle item) {
                        if (StringUtils.isNotEmpty(item.getTime())){
                            if (item.getTime().contains("-")){
                                helper.setText(R.id.tv_time, item.getTime());    //报到时间
                            }else if (Long.parseLong(item.getTime())>0){
                                helper.setText(R.id.tv_time, TimeUtil.getStringByFormat(Long.parseLong(item.getTime()), TimeUtil.dateFormatYMDHMS));    //报到时间
                            }

                        }
                        helper.setText(R.id.tv_device_content, item.getDevice());       //报到设备
                    }
                };
                signrecyclerView.setAdapter(mSignAdapter);
                //                getSignList(item);
                break;
        }
    }
}
