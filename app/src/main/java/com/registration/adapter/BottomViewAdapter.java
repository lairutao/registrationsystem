package com.registration.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.registration.base.BaseFragment;

import java.util.List;

/*
 * @创建者 admin
 * @创建时间  2019/6/28 20:34
 * @描述      ${TODO}         底部adapter
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class BottomViewAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> mFragments;

    public BottomViewAdapter(FragmentManager manager, List<BaseFragment> mFragmentList) {
        super(manager);
        this.mFragments = mFragmentList;
    }

    @Override
    public Fragment getItem(int i) {
        return mFragments.get(i);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }
}
