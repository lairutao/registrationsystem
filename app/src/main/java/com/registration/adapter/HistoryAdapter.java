package com.registration.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.registration.R;
import com.registration.moudle.HistoryEntity;
import com.registration.util.StringUtils;
import com.registration.util.TimeUtil;

import java.util.List;

public class HistoryAdapter extends BaseQuickAdapter<HistoryEntity, BaseViewHolder> {

    public HistoryAdapter(@Nullable List<HistoryEntity> data) {
        super(R.layout.item_history, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HistoryEntity item) {
        helper.setText(R.id.tv_user_name, "名字："+ item.getCiUserName());
        helper.setText(R.id.tv_ticketid, "票id："+ item.getCiTicketId());
        helper.setText(R.id.tv_remarks, "备注："+ StringUtils.notEmpty(item.getRemark()));
        helper.setText(R.id.tv_checkin_time, "签到时间："+ TimeUtil.getStringByFormat(item.getClientTime(), TimeUtil.dateFormatYMDHMS));
    }
}
