package com.registration.adapter;

import android.os.Environment;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bm.library.Info;
import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.registration.R;
import com.registration.constants.Constants;
import com.registration.moudle.DialogMoudle;
import com.registration.util.StringUtils;
import com.registration.view.TicketInfoDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.room.Dao;

/*
 * @创建者 lai
 * @创建时间  2019/7/10 17:39
 * @描述      ${TODO}             dialog 门票信息
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class DialogAadpter extends BaseMultiItemQuickAdapter<DialogMoudle, BaseViewHolder> {
    private static final int TYPE_TICKET = 0;        //门票信息
    private static final int TYPE_REMARK = 1;        //备注

    public DialogAadpter(@Nullable List<DialogMoudle> data) {
        super(data);
        addItemType(TYPE_TICKET, R.layout.item_list_dialog);
        addItemType(TYPE_REMARK, R.layout.item_list_dialog2);
    }

    //添加数据

    @Override
    protected void convert(BaseViewHolder helper, DialogMoudle item) {
        switch (helper.getItemViewType()) {
            case TYPE_TICKET:
                helper.setText(R.id.tv_title, item.getTitle());
                helper.setText(R.id.tv_content, item.getContent());
                break;
            case TYPE_REMARK:
                //备注
                if (StringUtils.isNotEmpty(item.getTitle())) {
                    helper.setText(R.id.tv_remark, item.getTitle());
                }
                //图片
                ImageView imageView = helper.getView(R.id.iv_picture);
                if (StringUtils.isNotEmpty(item.getContent())) {
                    imageView.setVisibility(View.VISIBLE);
                    if (item.getContent().contains("http")){
                        Glide.with(mContext).load(item.getContent()).placeholder(R.drawable.icon_user).into(imageView);
                    }else {
                        File picfile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + Constants.APPPICTUREPATH, item.getContent());
                        Glide.with(mContext).load(picfile/*item.getContent()*/).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.icon_user).into(imageView);
                    }

                } else {
                    imageView.setVisibility(View.GONE);
                }
                //图片点击放大
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        TicketInfoDialog.photoView(mContext,item);
                    }
                });
                break;
        }
    }


}
