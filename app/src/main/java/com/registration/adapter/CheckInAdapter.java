package com.registration.adapter;

import android.util.Log;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.registration.R;
import com.registration.base.StickHeaderDecoration;
import com.registration.moudle.CheckInListMoudle;

import java.util.List;

public class CheckInAdapter extends BaseQuickAdapter<CheckInListMoudle, BaseViewHolder> implements StickHeaderDecoration.GroupItem {

    public CheckInAdapter(@Nullable List<CheckInListMoudle> data) {
        super(R.layout.item_check_in, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CheckInListMoudle item) {
        helper.setText(R.id.tv_user_type, item.getTeamRole());
        helper.setText(R.id.tv_status, item.getCheckInStatus());
        helper.setText(R.id.tv_user_name, "名字："+ item.getChineseName());
        helper.setText(R.id.tv_user_email, "邮件："+ item.getEmail());
        helper.setText(R.id.tv_user_phone, "电话："+ item.getMobile());
        helper.setText(R.id.tv_idcard, "身份证："+ item.getDocumentNumber());
    }

    @Override
    public boolean isItemHeader(int position) {
        if (position > 0 && getData().size() > position) {
            return !getGroupName(position).equals(getGroupName(position-1));
        }
        return true;
    }

    @Override
    public String getGroupName(int position) {
        return getData().get(position).getGroupName();
    }
}
