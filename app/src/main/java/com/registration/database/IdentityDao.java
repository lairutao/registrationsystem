package com.registration.database;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.registration.moudle.IdentityEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/27 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Dao
public interface IdentityDao {

    //查全部
    @Query("SELECT * FROM identity")
    Single<List<IdentityEntity>> getAllData();

    //查
    @Query("SELECT * FROM identity WHERE id IN (:id)")
    Single<IdentityEntity> queryOrderById(int id);

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(@NonNull IdentityEntity entity);

    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(@NonNull List<IdentityEntity> list);

    @Delete
    void deleteAll(List<IdentityEntity> list);

    //删除
    @Delete
    void delete(IdentityEntity... entities);

    //改
    @Update
    void update(IdentityEntity... entities);
}
