package com.registration.database;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.registration.moudle.HistoryEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/26 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Dao
public interface HistoryDao {

    //查全部
    @Query("SELECT * FROM history")
    Single<List<HistoryEntity>> getAllData();

    //查询签到历史
    @Query("SELECT * FROM history WHERE ciTicketId = (:id) AND ciType = (:type) AND successFlag > 0")
    Single<List<HistoryEntity>> getHistoryRecord(int id, int type);

    //查未同步的签到历史
    @Query("SELECT * FROM history WHERE syncFlag > 0")
    Single<List<HistoryEntity>> getSyncHistory();

    //查倒数n条记录
    @Query("SELECT * FROM history ORDER BY id DESC LIMIT (:count)")
    Single<List<HistoryEntity>> queryOrderByDesc(int count);

    //查
    @Query("SELECT * FROM history WHERE id IN (:id)")
    Single<HistoryEntity> queryOrderById(int id);

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(@NonNull HistoryEntity entity);

    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(@NonNull List<HistoryEntity> list);

    @Delete
    void deleteAll(List<HistoryEntity> list);

    //删除
    @Delete
    void delete(HistoryEntity... entities);

    //改
    @Update
    void update(HistoryEntity... entities);

}
