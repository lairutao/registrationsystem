package com.registration.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import android.content.Context;

import com.registration.moudle.ActivityEntity;
import com.registration.moudle.AppUserPrizeRelationEntity;
import com.registration.moudle.AreaEntity;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.CompanyEntity;
import com.registration.moudle.DivisionEntity;
import com.registration.moudle.GroupEntity;
import com.registration.moudle.HistoryEntity;
import com.registration.moudle.IdentityEntity;
import com.registration.moudle.PrizeEntity;
import com.registration.moudle.SignInMoudle;
import com.registration.moudle.DataPackMoudle;
import com.registration.moudle.TicketEntity;
import com.registration.moudle.TicketInfo;
import com.registration.moudle.UserInfoEntity;
import com.registration.moudle.UserMoudle;

/**
 * Created by lai on 2019/7/5.
 */
@Database(entities = {SignInMoudle.class, UserMoudle.class, TicketInfo.class, HistoryEntity.class, DataPackMoudle.class,
        ActivityEntity.class, AppUserPrizeRelationEntity.class, AreaEntity.class, CheckPointEntity.class, CompanyEntity.class, DivisionEntity.class,
        GroupEntity.class, IdentityEntity.class, PrizeEntity.class, TicketEntity.class, UserInfoEntity.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static final    String      DB_NAME = "AppDatabase.db";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DB_NAME)
                .allowMainThreadQueries()
                //.addMigrations(DB_MIGRATION) //版本迁移策略
                .fallbackToDestructiveMigration() //请空旧数据
                .build();
    }

    public abstract UserDao getUserDao();

    public abstract SignInDao getSignInDao();

    public abstract TicketInfoDao getTicketInfoDao();

    ///////////////////////////////////////////////////

    public abstract HistoryDao getHistoryDao();

    public abstract DataPackDao getDataPackDao();

    ///////////////////////////////////////////////////

    public abstract AreaDao getAreaDao();

    public abstract GroupDao getGroupDao();

    public abstract PrizeDao getPrizeDao();

    public abstract TicketDao getTicketDao();

    public abstract CompanyDao getCompanyDao();

    public abstract ActivityDao getActivityDao();

    public abstract DivisionDao getDivisionDao();

    public abstract IdentityDao getIdentityDao();

    public abstract RelationDao getRelationDao();

    public abstract UserInfoDao getUserInfoDao();

    public abstract CheckPointDao getCheckPointDao();

    static final Migration DB_MIGRATION = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE ticketInfo "
//                    + " ADD COLUMN checkpointIdList2 String");
        }
    };
}
