package com.registration.database;

import com.registration.moudle.TicketInfo;

import java.util.List;

import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 lai
 * @创建时间  2019/7/10 9:39
 * @描述      ${TODO}         门票信息
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
@Dao
public interface TicketInfoDao {

    //查全部
    @Query("SELECT * FROM ticketInfo")
    Flowable<List<TicketInfo>> loadAllTicket();

    //查全部
    @Query("SELECT * FROM ticketInfo")
    Single<List<TicketInfo>> loadAllTicketInfo();

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TicketInfo themes);


    //添加全部
    @DrawerArrowDrawable.ArrowDirection
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TicketInfo> themes);


    //查 根据id
    @Query("SELECT * FROM ticketInfo WHERE id IN (:id)")
    Single<TicketInfo> queryOrderById(int id);

    //删除
    @Delete
    void deleteUser(TicketInfo... ticketInfos);

    //改，更新单个
    @Update
    void updateUser(TicketInfo... ticketInfos);

    @Delete
    void deleteAll(List<TicketInfo> ticketInfos);


    //查 根据id    Flowable能够发射0或n个数据，并以成功或错误事件终止。 支持Backpressure，可以控制数据源发射的速度。
    @Query("SELECT * FROM ticketInfo WHERE reportTeamId IN (:id)")
    Flowable<List<TicketInfo>> queryReportTeamId(int id);

    //查 根据id   Single只发射单个数据或错误事件
    @Query("SELECT * FROM ticketInfo WHERE checkpointId IN (:id)")
    Flowable<List<TicketInfo>> queryCheckPointIds(int id);

    //查 根据id   Single只发射单个数据或错误事件
    @Query("SELECT * FROM ticketInfo WHERE reportTeamId IN (:id)")
    Single<List<TicketInfo>> queryReportTeamIds(int id);

    //改，更新集合
    @Update
    void updateListTicket(List<TicketInfo> ticketInfoList);
}
