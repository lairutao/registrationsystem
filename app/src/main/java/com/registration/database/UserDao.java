package com.registration.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Completable;
import io.reactivex.Flowable;

import com.registration.moudle.UserMoudle;

import java.util.List;

/*
 * @创建者 lai
 * @创建时间  2019/7/5 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
@Dao
public interface UserDao {
    //查全部
    @Query("SELECT * FROM user")
    Flowable<List<UserMoudle>> loadAllUser();

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void  insert(UserMoudle themes);


    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void  insertAll(List<UserMoudle> themes);


    //查
    @Query("SELECT * FROM user WHERE id IN (:id)")
     Flowable<UserMoudle> queryOrderById(int id);

    //删除
    @Delete
     void deleteUser(UserMoudle... userMoudles);

    //改
    @Update
     void updateUser(UserMoudle... userMoudles);

    @Delete
    void deleteAll(List<UserMoudle> userEntities);

}
