package com.registration.database;

import com.registration.moudle.SignInMoudle;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 admin
 * @创建时间  2019/7/11 11:46
 * @描述      ${TODO}         签到信息
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
@Dao
public interface SignInDao {
    @Query("SELECT * FROM signIn")
    Single<List<SignInMoudle>> getAllSign();

    //使用内连接查询
    @Query("SELECT * FROM signIn WHERE empId IN (:id)")
    Single<List<SignInMoudle>> getSignInById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(SignInMoudle signInMoudle);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSign(List<SignInMoudle> signInMoudleList);

    //删除全部的签到记录
    @Delete()
    void deleteAll(List<SignInMoudle> signInMoudleList);


    //获取签到名
    @Query("SELECT *FROM signIn WHERE device IN(:device)")
    Single<List<SignInMoudle>> getSignInBydevice(String device);
}
