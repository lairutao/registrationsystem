package com.registration.database;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.registration.moudle.HistoryEntity;
import com.registration.moudle.RemarkEntity;
import com.registration.moudle.TicketEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/27 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Dao
public interface TicketDao {

    //查全部
    @Query("SELECT * FROM ticket")
    Single<List<TicketEntity>> getAllData();

    //查未同步的备注信息
    @Query("SELECT id AS ticketId, transferProfile, remark FROM ticket WHERE syncFlag > 0")
    Single<List<RemarkEntity>> getSyncRemarkInfo();

    //查
    @Query("SELECT * FROM ticket WHERE id IN (:id)")
    Single<TicketEntity> queryOrderById(int id);

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(@NonNull TicketEntity entity);

    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(@NonNull List<TicketEntity> list);

    @Delete
    void deleteAll(List<TicketEntity> list);

    //删除
    @Delete
    void delete(TicketEntity... entities);

    //改
    @Update
    void update(TicketEntity... entities);
}
