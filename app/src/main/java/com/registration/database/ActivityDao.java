package com.registration.database;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.registration.moudle.ActivityEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/27 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Dao
public interface ActivityDao {

    //查全部
    @Query("SELECT * FROM activity")
    Single<List<ActivityEntity>> getAllData();

    //查
    @Query("SELECT * FROM activity WHERE id IN (:id)")
    Single<ActivityEntity> queryOrderById(int id);

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(@NonNull ActivityEntity entity);

    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(@NonNull List<ActivityEntity> list);

    @Delete
    void deleteAll(List<ActivityEntity> list);

    //删除
    @Delete
    void delete(ActivityEntity... entities);

    //改
    @Update
    void update(ActivityEntity... entities);
}
