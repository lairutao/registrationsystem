package com.registration.database;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.google.gson.JsonObject;
import com.registration.moudle.ActivityEntity;
import com.registration.moudle.AppUserPrizeRelationEntity;
import com.registration.moudle.AreaEntity;
import com.registration.moudle.CheckInDetailMoudle;
import com.registration.moudle.CheckInListMoudle;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.CompanyEntity;
import com.registration.moudle.DataPackMoudle;
import com.registration.moudle.DivisionEntity;
import com.registration.moudle.GroupEntity;
import com.registration.moudle.HistoryEntity;
import com.registration.moudle.IdentityEntity;
import com.registration.moudle.PrizeEntity;
import com.registration.moudle.RemarkEntity;
import com.registration.moudle.TicketEntity;
import com.registration.moudle.UserInfoEntity;

import java.util.List;
import java.util.concurrent.TimeoutException;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/26 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Dao
public abstract class DataPackDao {

    //查全部
    @Query("SELECT * FROM datapack")
    public abstract Single<List<DataPackMoudle>> getAllData();

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(@NonNull DataPackMoudle entity);

    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(@NonNull List<DataPackMoudle> list);

    //查
    @Query("SELECT * FROM datapack WHERE id IN (:id)")
    public abstract Single<DataPackMoudle> queryOrderById(int id);

    @Query("SELECT * FROM datapack LIMIT 1")
    public abstract Single<DataPackMoudle> getData();

    //删除
    @Delete
    public abstract void delete(DataPackMoudle... entities);

    //改
    @Update
    public abstract void update(DataPackMoudle... entities);

    @Query("DELETE FROM datapack")
    public abstract void deleteAll();

    /*******************************************************************/

    //合并数据
    public void mergeData(DataPackMoudle moudle) {
        if (moudle != null) {
            if (moudle.activityList != null && moudle.activityList.size() > 0) {
                setActivityList(moudle.activityList);
            }
            if (moudle.appUserPrizeRelationList != null && moudle.appUserPrizeRelationList.size() > 0) {
                setUserPrizeRelationList(moudle.appUserPrizeRelationList);
            }
            if (moudle.areaList != null && moudle.areaList.size() > 0) {
                setAreaList(moudle.areaList);
            }
            if (moudle.checkpointList != null && moudle.checkpointList.size() > 0) {
                setCheckPointList(moudle.checkpointList);
            }
            if (moudle.companyList != null && moudle.companyList.size() > 0) {
                setCompanyList(moudle.companyList);
            }
            if (moudle.divisionList != null && moudle.divisionList.size() > 0) {
                setDivisionList(moudle.divisionList);
            }
            if (moudle.groupList != null && moudle.groupList.size() > 0) {
                setGroupList(moudle.groupList);
            }
            if (moudle.identityList != null && moudle.identityList.size() > 0) {
                setIdentityList(moudle.identityList);
            }
            if (moudle.prizeList != null && moudle.prizeList.size() > 0) {
                setPrizeList(moudle.prizeList);
            }
            if (moudle.ticketList != null && moudle.ticketList.size() > 0) {
                setTicketList(moudle.ticketList);
            }
            if (moudle.userInfoList != null && moudle.userInfoList.size() > 0) {
                setUserInfoList(moudle.userInfoList);
            }
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setActivityList(@NonNull List<ActivityEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setUserPrizeRelationList(@NonNull List<AppUserPrizeRelationEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setAreaList(@NonNull List<AreaEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setCheckPointList(@NonNull List<CheckPointEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setCompanyList(@NonNull List<CompanyEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setDivisionList(@NonNull List<DivisionEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setGroupList(@NonNull List<GroupEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setIdentityList(@NonNull List<IdentityEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setPrizeList(@NonNull List<PrizeEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setTicketList(@NonNull List<TicketEntity> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void setUserInfoList(@NonNull List<UserInfoEntity> list);

    /*******************************************************************/

    //清空数据
    public void clearData() {
        clearArea();
        clearGroup();
        clearPrize();
        clearTicket();
        clearCompany();
        clearActivity();
        clearDivision();
        clearIdentity();
        clearRelation();
        clearUserInfo();
        clearCheckPoint();
        clearDataPack();
        clearHistory();
    }

    @Query("DELETE FROM area")
    protected abstract void clearArea();

    @Query("DELETE FROM `group`")
    protected abstract void clearGroup();

    @Query("DELETE FROM prize")
    protected abstract void clearPrize();

    @Query("DELETE FROM ticket")
    protected abstract void clearTicket();

    @Query("DELETE FROM company")
    protected abstract void clearCompany();

    @Query("DELETE FROM activity")
    protected abstract void clearActivity();

    @Query("DELETE FROM division")
    protected abstract void clearDivision();

    @Query("DELETE FROM identity")
    protected abstract void clearIdentity();

    @Query("DELETE FROM relation")
    protected abstract void clearRelation();

    @Query("DELETE FROM userInfo")
    protected abstract void clearUserInfo();

    @Query("DELETE FROM checkpoint")
    protected abstract void clearCheckPoint();

    @Query("DELETE FROM datapack")
    protected abstract void clearDataPack();

    @Query("DELETE FROM history")
    protected abstract void clearHistory();

    /*******************************************************************/

//    final String QUERYSQL = "SELECT " +
//            "userInfo.email, " +
//            "userInfo.mobile, " +
//            "userInfo.chineseName, " +
//            "userInfo.documentNumber, " +
//            "userinfo.id AS userId, " +
//            "company.id AS companyId, " +
//            "company.companyName, " +
//            "ticket.id AS ticketId, " +
//            "ticket.ciGroupId AS groupId, " +
//            "ticket.checkInFlag AS ticketCheckInFlag, " +
//            "`group`.checkInFlag AS leaderCheckInFlag, " +
//            "`group`.leaderTicketId FROM userInfo " +
//            "INNER JOIN ticket ON userInfo.id = ticket.ownerUserInfoId " +
//            "LEFT JOIN company ON userInfo.companyId = company.id " +
//            "LEFT JOIN `group` ON ticket.ciGroupId = `group`.id ";

    final String QUERYSQL = "SELECT " +
            "userInfo.email, " +
            "userInfo.mobile, " +
            "userInfo.chineseName, " +
            "userInfo.documentNumber, " +
            "userinfo.id AS userId, " +
            "company.id AS companyId, " +
            "company.companyName, " +
            "ticketGroup.id AS ticketId, " +
            "ticketGroup.isTeam AS teamType, " +
            "ticketGroup.ciGroupId AS groupId, " +
            "ticketGroup.randomCode AS randomCode, " +
            "ticketGroup.checkInFlag AS ticketCheckInFlag, " +
            "ticketGroup.leaderCheckInFlag, " +
            "ticketGroup.leaderTicketId FROM (" +
                //增加领队非团员用户名单到团队报道组
                //"SELECT t1.*, false AS isLeader FROM userInfo t1 UNION ALL " +
                //"SELECT t2.*, true AS isLeader FROM `group` gg INNER JOIN ticket tt ON gg.leaderTicketId = tt.id INNER JOIN userInfo t2 ON tt.ownerUserInfoId = t2.id WHERE tt.ciGroupId = 0" +
                "SELECT t1.*, 0 AS isTeam, t1.ciCheckpointId AS mCheckpointId, 0 AS leaderCheckInFlag, 0 AS leaderTicketId FROM ticket t1 WHERE ciGroupId = 0 UNION ALL " + //个人
                "SELECT t2.*, 1 AS isTeam, g1.ciCheckpointId AS mCheckpointId, g1.checkInFlag AS leaderCheckInFlag, leaderTicketId FROM ticket t2 INNER JOIN `group` g1 ON t2.ciGroupId = g1.id UNION ALL " + //团员
                "SELECT t3.*, 2 AS isTeam, g2.ciCheckpointId AS mCheckpointId, g2.checkInFlag AS leaderCheckInFlag, leaderTicketId FROM ticket t3 INNER JOIN `group` g2 ON t3.id = g2.leaderTicketId WHERE t3.ciGroupId = 0" + //领队(非团员)
            ") AS ticketGroup " +
            "INNER JOIN userInfo ON ticketGroup.ownerUserInfoId = userInfo.id " +
            "LEFT JOIN company ON userInfo.companyId = company.id ";

    //获取报到列表
    @Query(QUERYSQL + " ORDER BY teamType")
    public abstract Single<List<CheckInListMoudle>> getAllCheckInList();

    //查询报到列表
    @Query(QUERYSQL + " WHERE mobile LIKE '%'||(:keyword)||'%' " +
            "OR documentNumber LIKE '%'||(:keyword)||'%' " +
            "OR chineseName LIKE '%'||(:keyword)||'%' " +
            "OR email LIKE '%'||(:keyword)||'%' " +
            "ORDER BY teamType LIMIT (:limitCount)")
    protected abstract Single<List<CheckInListMoudle>> queryAllCheckInList(String keyword, int limitCount);

    //扫码查询
    @Query(QUERYSQL + " WHERE ticketGroup.randomCode = (:qrcode) ORDER BY teamType")
    public abstract Single<List<CheckInListMoudle>> queryAllCheckInList(String qrcode);

    //数据统计
    @Query(QUERYSQL + "WHERE mCheckpointId = (:id)")
    public abstract Single<List<CheckInListMoudle>> queryAllCheckInList(int id);

    //未报到
    @Query(QUERYSQL + " WHERE (teamType > 0 AND leaderCheckInFlag = 0) OR (teamType = 0 AND ticketCheckInFlag = 0) ORDER BY teamType")
    protected abstract Single<List<CheckInListMoudle>> queryAllCheckInList1();

    //已报到
    @Query(QUERYSQL + " WHERE (teamType > 0 AND leaderCheckInFlag > 0) OR (teamType = 0 AND ticketCheckInFlag > 0) ORDER BY teamType")
    protected abstract Single<List<CheckInListMoudle>> queryAllCheckInList2();

    //个人报道
    @Query(QUERYSQL + " WHERE teamType = 0 LIMIT 200")
    protected abstract Single<List<CheckInListMoudle>> queryAllCheckInList3();

    //团队报道
    @Query(QUERYSQL + " WHERE teamType > 0 LIMIT 200")
    protected abstract Single<List<CheckInListMoudle>> queryAllCheckInList4();

    public Single<List<CheckInListMoudle>> queryCheckInList(String keyword) {
        if (!TextUtils.isEmpty(keyword)) {
            if (keyword.equals("未报到")) {
                return queryAllCheckInList1();
            } else
            if (keyword.equals("已报到")) {
                return queryAllCheckInList2();
            } else
            if (keyword.equals("个人报道")) {
                return queryAllCheckInList3();
            } else
            if (keyword.equals("团队报道")) {
                return queryAllCheckInList4();
            }
            return queryAllCheckInList(keyword, 100);
        }
        return queryAllCheckInList("", 100);
    }

    public Single<List<CheckInListMoudle>> getCheckInList(int id) {
        if (id > 0) {
            return queryAllCheckInList(id);
        }
        return getAllCheckInList();
    }

    //查询报到详情
    @Query("SELECT ticket.id AS ticketId, " +
            "ticket.remark, " +
            "ticket.randomCode, " +
            "ticket.transferProfile, " +
            //"ticket.ciGroupId AS groupId, " +
            "identity.identityName, " +
            "ticket.ciCheckpointId AS checkpointId, " +
            "checkpoint.checkpointName, " +
            "`group`.id AS teamGroupId, " +
            "`group`.groupName, " +
            "(" +
                "SELECT COUNT(*) FROM ticket WHERE ticket.ciGroupId = `group`.id" +
            ") AS groupMemberNum, " +
            "`group`.ciCheckpointId AS groupCheckpointId, " +
            "groupCheckpoint.checkpointName AS groupCheckpointName, " +
            "groupTicket.ciGroupId AS leaderGroupId, " +
            "ticketUserInfo.chineseName AS leaderName, " +
            "ticket.identityId " +
            //"ownerUserInfoId AS userId, " +
            //"userInfo.documentNumber, " +
            //"userInfo.chineseName, " +
            //"userInfo.mobile, " +
            //"userInfo.email " +
            "FROM ticket " +
            //"LEFT JOIN userInfo ON ticket.ownerUserInfoId = userInfo.id " +
            "LEFT JOIN checkpoint ON ticket.ciCheckpointId = checkpoint.id " +
            "LEFT JOIN identity ON ticket.identityId = identity.id " +
            "LEFT JOIN `group` ON (ticket.ciGroupId = `group`.id OR ticket.id = `group`.leaderTicketId) " +
            "LEFT JOIN checkpoint groupCheckpoint ON `group`.ciCheckpointId = groupCheckpoint.id " +
            "LEFT JOIN ticket groupTicket ON `group`.leaderTicketId = groupTicket.id " +
            "LEFT JOIN userInfo ticketUserInfo ON groupTicket.ownerUserInfoId = ticketUserInfo.id " +
            "WHERE ticket.id = (:id)")
    public abstract Single<CheckInDetailMoudle> queryCheckInDetailById(int id);

    @Query("UPDATE ticket SET transferProfile = :transferProfile, remark = :remark, checkInFlag = 1, syncFlag = 1 WHERE id = :ticketId")
    protected abstract void updateTicketInfo(int ticketId, String transferProfile, String remark);

    @Query("UPDATE ticket SET transferProfile = :transferProfile, remark = :remark, syncFlag = 1 WHERE id = (SELECT ticket.id FROM `group` INNER JOIN ticket ON `group`.leaderTicketId = ticket.id WHERE `group`.id = :groupId)")
    protected abstract void updateTicketInfo1(int groupId, String transferProfile, String remark);

    @Query("UPDATE ticket SET checkInFlag = 1, syncFlag = 1 WHERE ciGroupId = :groupId")
    protected abstract void updateTicketInfo2(int groupId);

    @Query("UPDATE `group` SET checkInFlag = 1 WHERE id = :groupId")
    protected abstract void updateGroupInfo(int groupId);

    @Insert//(onConflict = OnConflictStrategy.REPLACE)
    protected abstract void insertHistory(@NonNull HistoryEntity entity);

    //报到
    public void userCheckIn(@NonNull RemarkEntity remarkEntity, @NonNull HistoryEntity historyEntity) {
        if (historyEntity.isSuccess()) { //签到成功
            if (historyEntity.isTeamCheckIn()) { //团队报道
                updateTicketInfo1(historyEntity.getSubId(), remarkEntity.getTransferProfile(), remarkEntity.getRemark()); //领队
                updateTicketInfo2(historyEntity.getSubId()); //团员
                updateGroupInfo(historyEntity.getSubId()); //团队
            } else { //个人报道
                updateTicketInfo(remarkEntity.getTicketId(), remarkEntity.getTransferProfile(), remarkEntity.getRemark());
            }
        }
        historyEntity.setSyncFlag(1);
        insertHistory(historyEntity);
    }

    @Query("UPDATE ticket SET syncFlag = 0 WHERE syncFlag > 0")
    protected abstract void syncAllTicketInfo();

    @Query("UPDATE history SET syncFlag = 0 WHERE syncFlag > 0")
    protected abstract void syncAllHistory();

    //同步状态
    public void syncDataPack() {
        syncAllTicketInfo();
        syncAllHistory();
    }
}
