package com.registration.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class EntityConverter {

    private static Gson gson;
    private static Gson GsonInstance() {
        if (gson == null) {
            synchronized (EntityConverter.class) {
                gson = new Gson();
            }
        }
        return gson;
    }

    @TypeConverter
    public String objectToString(List list) {
        return GsonInstance().toJson(list);
    }

    @TypeConverter
    public List stringToObject(String json) {
        return GsonInstance().fromJson(json, new TypeToken<List>(){}.getType());
    }
}
