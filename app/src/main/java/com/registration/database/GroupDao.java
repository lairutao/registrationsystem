package com.registration.database;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.registration.moudle.GroupEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/*
 * @创建者 huangxy
 * @创建时间  2020/1/27 9:39
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */

@Dao
public interface GroupDao {

    //查全部
    @Query("SELECT * FROM `group`")
    Single<List<GroupEntity>> getAllData();

    //查
    @Query("SELECT * FROM `group` WHERE id IN (:id)")
    Single<GroupEntity> queryOrderById(int id);

    //添加
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(@NonNull GroupEntity entity);

    //添加全部
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(@NonNull List<GroupEntity> list);

    @Delete
    void deleteAll(List<GroupEntity> list);

    //删除
    @Delete
    void delete(GroupEntity... entities);

    //改
    @Update
    void update(GroupEntity... entities);
}
