package com.registration.constants;

import com.registration.BuildConfig;

/*
 * @创建者 lai
 * @创建时间  2019/7/3 14:59
 * @描述      ${TODO}             常量
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class Constants {

//    public static final String BASE_URL       = "http://192.168.1.244:8085";         //本地测试
//    public static final String BASE_URL       = "http://admin.controller.fun:8087";
//    public static final String BASE_URL       = "https://admin.app.idaonline.cn";        //线上
    public static final String BASE_URL       = "http://ci.imm.plus:8087";              //新版本
//    public static final String BASE_URL       = "http://121.8.164.83:26377";          //测试

    public static final String DES_KEY = BuildConfig.DEBUG ? "NJRCxWpFPaCgdmZw": "sbPHsCEyrS5YVvHc";

    public static final String TOKEN          = "token";
    public static final String SYNCTIME       = "syncTime";
    public static final String RANDOMKEY      = "randomKey";
    public static final String CHANNELID      = "channelId";
    public static final String COOKIES        = "cookies"; //cookies
    public static final String CHECKPOINTINFO = "checkpointInfo"; //报到台信息 list
    public static final String ACCOUNT        = "account"; //账号
    public static final String PASSWORD       = "password"; //密码
    public static final String CHECKPINT      = "checkpoint"; //报到台 单个 当前报到台
    public static final String URL            = "url"; //其他服务器
    public static final String ISCHECKPOINT   = "isCheckpoint"; //是否允许不同平台报到
    public static final String DEVICE         = "device"; //报到设备
    public static final String MEETINGLIST    = "mettingList"; //会议list
    public static final String MEETING        = "metting"; //会议
    public static final String DIVSIONLIST    = "divsionList"; //界别list
    public static final String DIVSION        = "divsion"; //界数
    public static final String APPPICTUREPATH = "com.registration/pic/"; //图片保存地址
    public static final int SELECT_PHOTO      = 10001; //选取图片返回码
    public static final int TAKE_PHOTO        = 10000; //图片拍照返回码
    public static final int SCAN_CODE         = 10002; //扫码报到返回码
}
