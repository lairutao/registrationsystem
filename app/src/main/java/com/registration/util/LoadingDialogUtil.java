package com.registration.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.registration.R;

import androidx.appcompat.app.AlertDialog;

/*
 * @创建者 lai
 * @创建时间  2019/6/28 18:08
 * @描述      ${TODO}         加载中的dialog
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class LoadingDialogUtil {
    // 加载 dialog 相关
    public static AlertDialog loadingDialog;

    //显示dialog
    public static void showLoadingDialog(Context context) {
        if (loadingDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.LoadingDialog);
            View rootView = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
            builder.setView(rootView);
            loadingDialog = builder.create();
            loadingDialog.setCanceledOnTouchOutside(false);
            Window window = loadingDialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(null);
            }
        }
        if (!loadingDialog.isShowing()) {
            loadingDialog.show();
        }
    }

    //隐藏dialog
    public static void hideLoadingDialog() {
        if (loadingDialog != null||(loadingDialog!=null&&loadingDialog.isShowing())) {
            loadingDialog.dismiss();
//            if (loadingDialog.isShowing()) {
                loadingDialog = null;
//            }
        }
    }
}
