package com.registration.util;

import java.util.ArrayList;
import java.util.List;

public class CollectionStream<T> {

    private List<T> list;
    private Predicate predicate = t -> true;

    public CollectionStream(List<T> list) {
        this.list = list;
    }

    public CollectionStream<T> filter(Predicate<? super T> predicate) {
        this.predicate = predicate;
        return this;
    }

    public List<T> collectors2List() {
        List<T> result = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (T t: list) {
                if (predicate.test(t)) {
                    result.add(t);
                }
            }
        }
        return result;
    }

    public interface Predicate<T> {
        boolean test(T t);
    }
}
