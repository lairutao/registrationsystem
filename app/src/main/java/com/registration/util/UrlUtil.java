package com.registration.util;

import org.apache.commons.validator.routines.UrlValidator;

/*
 * @创建者 lai
 * @创建时间  2019/8/2 11:01
 * @描述      ${TODO}   网址工具类
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class UrlUtil {
    /**
     * 判断 url 是否合法
     * @param addr
     * @return
     */
    public static boolean isIP(String addr) {
        String[] schemas={"http", "https"};
        UrlValidator urlValidator = new UrlValidator(schemas);
        return urlValidator.isValid(addr);
    }

    /**
     * 获取 url 参数
     * @param url
     * @return
     */
    public static String getValueByName(String url, String name) {
        String result = "";
        int index = url.indexOf("?");
        String temp = url.substring(index + 1);
        String[] keyValue = temp.split("&");
        for (String str : keyValue) {
            if (str.contains(name)) {
                result = str.replace(name + "=", "");
                break;
            }
        }
        return result;
    }
}
