package com.registration.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by ZeQiang Fang on 2019/3/14.
 */
public class Base64Utils {

    public static String getBase64(HashMap<String, Object> map) {
        String object = JsonHelper.getGson().toJson(map);

        return getBase64(object);
    }


    public static String getBase64(String str) {

        String strBase64 = "";
        try {
            strBase64 = Base64.encodeToString(str.getBytes("utf-8"),Base64.NO_WRAP);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return strBase64;
    }


}
