package com.registration.util;

import com.google.gson.Gson;

/**
 * Created by ZeQiang Fang on 2018/8/2.
 */

public class JsonHelper {

    private JsonHelper() {

    }

    public static Gson getGson() {
        return GsonHolder.sInstance;
    }

    public static  class GsonHolder {
        public static final Gson sInstance = new Gson();
    }


}
