package com.registration.util;

import android.content.Intent;
import android.net.Uri;

import com.registration.base.MyApplication;

import java.io.Serializable;

public class IntentUtil {

    /**
     * Intent params
     */
    public static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    public static final String ARG_PARAM3 = "param3";

    private static MyApplication appInstance = MyApplication.getInstance();

    public static Intent getIntent(Class target) {
        Intent intent = new Intent(appInstance.getContext(), target);
        return intent;
    }

    public static void startActivity(Class target) {
        Intent intent = new Intent(appInstance.getContext(), target);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        appInstance.getContext().startActivity(intent);
    }

    public static <T extends Serializable> void startActivity(Class target, T model) {
        Intent intent = new Intent(appInstance.getContext(), target);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ARG_PARAM1, model);
        appInstance.getContext().startActivity(intent);
    }

    public static <T extends Serializable> void startActivity(Class target, T model, T model2) {
        Intent intent = new Intent(appInstance.getContext(), target);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ARG_PARAM1, model);
        intent.putExtra(ARG_PARAM2, model2);
        appInstance.getContext().startActivity(intent);
    }

    public static <T extends Serializable> void startActivity(Class target, T model, T model2, T model3) {
        Intent intent = new Intent(appInstance.getContext(), target);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ARG_PARAM1, model);
        intent.putExtra(ARG_PARAM2, model2);
        intent.putExtra(ARG_PARAM3, model3);
        appInstance.getContext().startActivity(intent);
    }

    //跳到应用市场
    public static void toApplicationMarket() {
        String url = "market://details?id=" + appInstance.getContext().getPackageName();
        Intent marketIntent = new Intent("android.intent.action.VIEW");
        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        marketIntent.setData(Uri.parse(url));
        appInstance.getContext().startActivity(marketIntent);
    }

    //跳转到本地浏览器
    public static void toLocalBrower(String url) {
        if (StringUtils.isBlank(url)) {
            return;
        }
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.parse(url);
        intent.setData(uri);
        appInstance.getContext().startActivity(intent);
    }
}
