package com.registration.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.os.SystemClock;
import android.widget.Toast;


import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @创建者 Administrator
 * @创建时间 2018/4/10.18:02.
 * @描述 ${TODO}.   全局捕获异常
 */

public class CatchException implements Thread.UncaughtExceptionHandler {

    private        Context        mContext;
    private static CatchException instance;

    //存储设备信息和异常信息
    private Map<String, String> infos     = new HashMap<>();
    // 用于格式化日期,作为日志文件名的一部分
    private DateFormat          formatter = new SimpleDateFormat("yyyy-MM-dd");
    private Thread.UncaughtExceptionHandler mHandler;

    public static CatchException getInstance() {
        if (instance == null) {
            instance = new CatchException();
        }
        return instance;
    }

    /**
     * 初始化
     *
     * @param context
     */
    public void init(Context context) {
        mContext = context;
        //获取默认的异常处理
        mHandler = Thread.getDefaultUncaughtExceptionHandler();
        //设置默认处理器
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        if (hanleException(e) && mHandler != null) {
            mHandler.uncaughtException(t, e);
        } else {
            SystemClock.sleep(3000);
            //退出程序
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }

    /**
     * 自定义错误处，收集错误信息，发送错误报告
     *
     * @param e
     * @return
     */
    private boolean hanleException(Throwable e) {
        if (e == null) {
            return false;
        }
        try {
            new Thread() {
                @Override
                public void run() {
                    Looper.prepare();
                    Toast.makeText(mContext, "系统出现异常", Toast.LENGTH_SHORT).show();
                    Looper.loop();
                }
            }.start();
            //设备信息
            collectDeviceIndo(mContext);
            //保存日志文件
            saveCrashInfoFile(e);
            SystemClock.sleep(3000);
        } catch (Exception ex) {
            e.printStackTrace();
        }

        return true;

    }


    /**
     * 收集设备参数信息
     *
     * @param context
     */
    private void collectDeviceIndo(Context context) {

        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context
                    .getPackageName(), PackageManager.GET_ACTIVITIES);
            if (pi != null) {
                String versionName = pi.versionName + "";
                String versionCode = pi.versionCode + "";
//                String iemi = MyApplication.getIEMI(mContext);
                infos.put("versionName", versionName);
                infos.put("versionCode", versionCode);
//                infos.put("IEMI", iemi);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Field[] fields = Build.class.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            try {
                infos.put(field.getName(), field.get(null).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }


    //保存日志文件
    private String saveCrashInfoFile(Throwable e) {
        StringBuffer sb = new StringBuffer();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = sdf.format(new Date());
            sb.append("\r\n" + date + "\n");
            for (Map.Entry<String, String> entry : infos.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                sb.append(key + "=" + value + "\n");
            }

            Writer writer = new StringWriter();
            PrintWriter pw = new PrintWriter(writer);
            e.printStackTrace(pw);
            Throwable throwable = e.getCause();
            while (throwable != null) {
                throwable.printStackTrace(pw);
                throwable = throwable.getCause();
            }
            pw.flush();
            pw.close();
            String result = writer.toString();
            sb.append(result);

            String fileName = writeFile(sb.toString());
            return fileName;
        } catch (Exception exception) {
            exception.printStackTrace();
            try {
                writeFile(sb.toString());
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return null;

    }

    /**
     * 写入文件
     *
     * @param s
     * @return
     * @throws Exception
     */
    private String writeFile(String s) throws Exception {
        String time = formatter.format(new Date());
        String fileName = "/catch" + time + ".log";
        if (FileUtils.hasSdcard()) {
            String path = getGlobalPath();
            File file = new File(path);
            if (!file.exists()) {
                file.mkdir();
            }
            FileOutputStream fos = new FileOutputStream(path + fileName, true);
            fos.write(s.getBytes());
            fos.flush();
            fos.close();

        }
        return fileName;
    }

    //文件位置
    public String getGlobalPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath()
                + File.separator + "catch"+ File.separator;
    }
}
