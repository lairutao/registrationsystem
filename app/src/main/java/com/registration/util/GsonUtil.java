package com.registration.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/*
 * @创建者 lai
 * @创建时间  2019/7/26 16:34
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class GsonUtil {
    public static String fromArrayList(List<Integer> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    public static ArrayList<Integer> fromString(String value) {
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }
}
