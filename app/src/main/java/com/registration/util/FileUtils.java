package com.registration.util;

import android.os.Environment;
import androidx.annotation.NonNull;

import com.registration.base.MyApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @创建者 Administrator
 * @创建时间 2018/4/2.11:41.
 * @描述 ${TODO}.
 */

public class FileUtils {
    private static Date             date       = null;
    private static SimpleDateFormat dateFormat = null;
    private static String           date1      = "";
    private static String           lineNumber = "";

    public FileUtils() {
    }

    /**
     * 获取指定文件中的所有.MP4文件
     *
     * @return
     */
    @NonNull
    public static List<String> getVideoPath() {
        File file = new File(Environment.getExternalStorageDirectory().getPath() +"/"+ "video");
        List<String> MP4List = new ArrayList<>();
        FileUtils.recursionFile(file, MP4List);
        return MP4List;
    }


    //遍历手机所有文件 并将路径名存入集合中 参数需要 路径和集合
    public static void recursionFile(File dir, List<String> mp4) {
        //得到某个文件夹下所有的文件
        File[] files = dir.listFiles();
        //文件为空
        if (files == null) {
            return;
        }
        //遍历当前文件下的所有文件
        for (File file : files) {
            //如果是文件夹
            if (file.isDirectory()) {
                //则递归(方法自己调用自己)继续遍历该文件夹
                recursionFile(file, mp4);
            } else { //如果不是文件夹 则是文件
                //如果文件名以 .m3u8结尾则是m3u8文件
                if (file.getName().endsWith(".m3u8")|| file.getName().endsWith(".mp4")) {
                    //往视频集合中 添加视频的路径
                    mp4.add(file.getAbsolutePath());
                }
            }
        }
    }


    /**
     * 获取指定文件中的所有.jpg文件
     *
     * @return
     */
    @NonNull
    public static List<String> getPicPath() {
        File file = new File(Environment.getExternalStorageDirectory().getPath() +"/"+"pdf");
        List<String> picList = new ArrayList<>();
        FileUtils.findPicFile(file, picList);
        return picList;
    }


    //遍历手机所有文件 并将路径名存入集合中 参数需要 路径和集合
    public static void findPicFile(File dir, List<String> pic) {
        //得到某个文件夹下所有的文件
        File[] files = dir.listFiles();
        //文件为空
        if (files == null) {
            return;
        }
        //遍历当前文件下的所有文件
        for (File file : files) {
            //如果是文件夹
            if (file.isDirectory()) {
                //则递归(方法自己调用自己)继续遍历该文件夹
                findPicFile(file, pic);
            } else { //如果不是文件夹 则是文件
                //如果文件名以 .jpg结尾则是jpg文件
                if (file.getName().endsWith(".jpg")||file.getName().endsWith(".png")) {
                    //往视频集合中 添加视频的路径
                    String path=file.getAbsolutePath();
                    String newstr=path.substring(path.indexOf("pic/")+4,path.length());
                    pic.add(newstr);
                }
            }
        }
    }

    /**
     * 获取指定文件中的所有.jpg文件
     *
     * @return
     */
    @NonNull
    public static List<String> getPdfPath() {
        File file = new File(Environment.getExternalStorageDirectory().getPath() +"/"+ "pdf");
        List<String> picList = new ArrayList<>();
        FileUtils.findPdfFile(file, picList);
        return picList;
    }


    //遍历手机所有文件 并将路径名存入集合中 参数需要 路径和集合
    public static void findPdfFile(File dir, List<String> pic) {
        //得到某个文件夹下所有的文件
        File[] files = dir.listFiles();
        //文件为空
        if (files == null) {
            return;
        }
        //遍历当前文件下的所有文件
        for (File file : files) {
            //如果是文件夹
            if (file.isDirectory()) {
                //则递归(方法自己调用自己)继续遍历该文件夹
                findPicFile(file, pic);
            } else { //如果不是文件夹 则是文件
                //如果文件名以 .jpg结尾则是jpg文件
                if (file.getName().endsWith(".pdf")) {
                    //往视频集合中 添加视频的路径
                    String path=file.getAbsolutePath();
                    String newstr=path.substring(path.indexOf("pic/")+4,path.length());
                    pic.add(newstr);
                }
            }
        }
    }

    /**
     * 删除某个文件夹下的所有文件夹和文件
     */


    public static boolean deletefile(String delpath)
            throws FileNotFoundException, IOException {
        try {

            File file = new File(delpath);
            if (!file.isDirectory()) {
                System.out.println("1");
                file.delete();
            } else if (file.isDirectory()) {
                System.out.println("2");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File delfile = new File(delpath + "\\" + filelist[i]);
                    if (!delfile.isDirectory()) {
                        System.out.println("path=" + delfile.getPath());
                        System.out.println("absolutepath="
                                + delfile.getAbsolutePath());
                        System.out.println("name=" + delfile.getName());
                        delfile.delete();
                        System.out.println("删除文件成功");
                    } else if (delfile.isDirectory()) {
                        deletefile(delpath + "\\" + filelist[i]);
                    }
                }
                file.delete();

            }

        } catch (FileNotFoundException e) {
            System.out.println("deletefile()   Exception:" + e.getMessage());
        }
        return true;
    }

    /**
     * @param saveDir
     * @return
     * @throws IOException 判断下载目录是否存在
     */
    public static String isExistDir(String saveDir) throws IOException {
        // 下载位置
        File downloadFile = new File(Environment.getExternalStorageDirectory(), saveDir);
        if (!downloadFile.mkdirs()) {
            downloadFile.createNewFile();
        }
        String savePath = downloadFile.getAbsolutePath();
        return savePath;
    }

    public static boolean hasSdcard() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }


    public static String getRootPath() {
        // 判断sd卡是否存在
        boolean sdCardExist = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        if (sdCardExist) {
            // 存在默认安装进SD卡
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        } else {
            // 没有SD卡装到内置存储位置
            return (MyApplication.getInstance().getFilesDir() + "");
        }
    }

    /**
     * 保存关键日志文件
     *
     * @param fileName   ：保存的文件名
     * @param logmessage ：要保存的日志信息
     */
    public static synchronized void createLog(String fileName, String logmessage) {
        if (hasSdcard()) {
            createLogtoSDCard(fileName, logmessage);
        }
    }


    /**
     * 把日志文件写到sd卡
     *
     * @param fileName
     * @param logmessage
     */
    public static void createLogtoSDCard(String fileName, String logmessage) {
        try {

            File sdCardDir = new File(getRootPath());// 获取SDCard目录
            if (date1 == null || "".equals(date1) || !date1.equals(getDate(1))) {
                date1 = getDate(1);
            }
            // 统一放在Logs文件夹下
            File saveFile = new File(sdCardDir,  "/log_" + date1);
            if (!saveFile.exists()) {
                saveFile.mkdirs();
            }
            // 创建log文件
            File file = new File(saveFile, fileName);
            if (!file.exists()) {
                file.createNewFile();
            }

            String date = getDate(0);
            StringBuffer buffer = new StringBuffer();
            buffer.append("[" + date + "] ");
            buffer.append(/*" \r\n" + */logmessage + " \r\n");
            String message = buffer.toString();

            FileOutputStream outStream = new FileOutputStream(file, true);
            outStream.write(message.getBytes("utf-8"));
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 获取当前时间 2018-04-30 11:11:19
     *
     * @return
     */
    private static String getDate(int type) {
        if (date != null) {
            date = null;
        }
        date = new Date();
        if (dateFormat != null) {
            dateFormat = null;
        }
        // 可以方便地修改日期格式
        if (type == 1) {
            dateFormat = new SimpleDateFormat("yyyyMMdd");
        } else {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        }
        return dateFormat.format(date);
    }

}
