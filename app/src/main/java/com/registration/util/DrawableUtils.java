package com.registration.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

/**
 * Created by huangxy on 2021/1/25.
 * https://github.com/GitSmark/AndroidStudioTemplates
 */
public class DrawableUtils {

    public static Drawable TintWithRes(@NonNull Context context, @DrawableRes int IdRes, @ColorRes int id) {
        return TintWithColor(context, IdRes, ContextCompat.getColor(context, id));
    }

    public static Drawable TintWithColor(@NonNull Context context, @DrawableRes int IdRes, @ColorInt int color) {
        try {
            Drawable drawable = ContextCompat.getDrawable(context, IdRes);
            return TintWithColor(drawable, color);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Drawable TintWithRes(@NonNull Context context, @NonNull Drawable drawable, @ColorRes int id) {
        return TintWithColor(drawable, ContextCompat.getColor(context, id));
    }

    public static Drawable TintWithColor(@NonNull Drawable drawable, @ColorInt int color) {
        try {
            Drawable.ConstantState state = drawable.getConstantState();
            Drawable tintDrawable = DrawableCompat.wrap((state != null)? state.newDrawable(): drawable).mutate();
            tintDrawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            DrawableCompat.setTint(tintDrawable, color);
            return tintDrawable;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
