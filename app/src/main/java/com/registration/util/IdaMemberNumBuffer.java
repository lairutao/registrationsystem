package com.registration.util;


import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 会员 ，ida成员
 */
public class IdaMemberNumBuffer {

    private String memberNum;

    private static Matcher matcher;

    private String division;

    private String area;

    private String number;

    private String participation;

    private String managerPrize;

    private String salesmanPrize;

    private static final String pattern = "(W|D)(\\d{1,2})([A-Z]{1,2})(\\d*)([Y|N]{0,1})([A-Z]{0,1})([a-z]{0,1})";

    public IdaMemberNumBuffer(String memberNum) {
        Pattern compile = Pattern.compile(pattern);
        matcher = compile.matcher(memberNum);
        if (matcher.find()) {
            division = matcher.group(1) + matcher.group(2);
            area = matcher.group(3);
            number = matcher.group(4);
            participation = matcher.group(5);
            managerPrize = matcher.group(6);
            salesmanPrize = matcher.group(7);
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(division);
        sb.append(area);
        sb.append(number);
        sb.append(participation);
        sb.append(managerPrize);
        sb.append(salesmanPrize);
        return sb.toString();
    }


    public String getMaxPrize() {
        Integer s = 0, m = 0;
        if (StringUtils.isNotEmpty(salesmanPrize)) {
            s = IDAPrizeType.valueOf(salesmanPrize).getLevel();
        }
        if (StringUtils.isNotEmpty(managerPrize)) {
            m = IDAPrizeType.valueOf(managerPrize).getLevel();
        }
        if (s > m) {
            return salesmanPrize;
        } else if (m > s) {
            return managerPrize;
        }
        return "n";
    }


    public boolean isMember() {
        return StringUtils.isNotEmpty(salesmanPrize) || StringUtils.isNotEmpty(managerPrize);
    }

    /**
     * 限会员编号使用，大陆地区门票编号不适用
     */
    public boolean isParticipation() {
        return "Y".equals(participation);
    }

    public static void main(String[] args) {
        IdaMemberNumBuffer buffer = new IdaMemberNumBuffer("D19T00063YCb");
    }

    public String getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(String memberNum) {
        this.memberNum = memberNum;
    }

    public Matcher getMatcher() {
        return matcher;
    }

    public void setMatcher(Matcher matcher) {
        this.matcher = matcher;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getManagerPrize() {
        return managerPrize;
    }

    public void setManagerPrize(String managerPrize) {
        this.managerPrize = managerPrize;
    }

    public String getSalesmanPrize() {
        return salesmanPrize;
    }

    public void setSalesmanPrize(String salesmanPrize) {
        this.salesmanPrize = salesmanPrize;
    }


    public enum IDAPrizeType {
        P(800, "主管白金奖", "P"),
        p(700, "业务白金奖", "p"),

        A(600, "主管金龙奖", "A"),
        a(500, "业务金龙奖", "a"),

        B(400, "主管银龙奖", "B"),
        b(300, "业务银龙奖", "b"),

        C(200, "主管铜龙奖", "C"),
        c(100, "业务铜龙奖", "c"),

        n(0, "普通会员", "n");

        Integer level;
        String  name;
        String  code;

        IDAPrizeType(Integer level, String name, String code) {
            this.level = level;
            this.name = name;
            this.code = code;
        }

        public Integer getLevel() {
            return level;
        }

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }
    }

    //团队成员
    private static Pattern compile = Pattern.compile(pattern);

    //是否是ida成员  true IDA成员   false 普通成员
    public static boolean ismemberNum(String memberNum) {
        matcher = compile.matcher(memberNum);
        if (matcher.find()) {
            String managerPrize = matcher.group(6);
            String salesmanPrize = matcher.group(7);
            return StringUtils.isNotEmpty(salesmanPrize) || StringUtils.isNotEmpty(managerPrize);
        } else {
            return false;
        }
    }

    //奖项
    public static String getPattern(String memberNum) {
        matcher = compile.matcher(memberNum);
        if (matcher.find()) {
            String managerPrize = matcher.group(6);
            String salesmanPrize = matcher.group(7);
            StringBuffer sb = new StringBuffer();
            if (StringUtils.isNotEmpty(managerPrize)) {
                sb.append(IDAPrizeType.valueOf(managerPrize).getName());
            }
            if (StringUtils.isNotEmpty(salesmanPrize) && StringUtils.isNotEmpty(managerPrize)) {
                sb.append("," + IDAPrizeType.valueOf(salesmanPrize).getName());
            } else if (StringUtils.isNotEmpty(salesmanPrize)) {
//                String s = IDAPrizeType.valueOf(salesmanPrize).getName();
//                Log.d("test", "奖项---" + s);
                sb.append(IDAPrizeType.valueOf(salesmanPrize).getName());
            }
            return sb.toString();
        } else {
            return null;
        }
    }
}
