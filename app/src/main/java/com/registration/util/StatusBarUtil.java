package com.registration.util;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

/**
 * Created by imm on 2017/6/6.
 */

public class StatusBarUtil {
    //android 5.0以上状态栏着色模式或全屏模式
    public static void StatusBarColor(AppCompatActivity activity, boolean isColor, boolean isHideStatusBar, int color){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(isHideStatusBar==true) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS | WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
            else {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            }
            window.setStatusBarColor(color);
            ViewGroup viewGroup = (ViewGroup) activity.findViewById(Window.ID_ANDROID_CONTENT);
            View childView = viewGroup.getChildAt(0);
            if (childView != null) {
                //第二个参数：true表示着色模式，会预留出状态栏的空间；false表示全屏模式，不会预留出状态栏的空间
                if(isColor==true) {
                    ViewCompat.setFitsSystemWindows(childView, true);
                }
                else {
                    ViewCompat.setFitsSystemWindows(childView, false);
                }
            }
        }
        else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT&&Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP){
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);//将状态栏设置成透明的
            ViewGroup viewGroup = (ViewGroup) activity.findViewById(Window.ID_ANDROID_CONTENT);
            int statusBarHeight = getStatusBarHeight(activity);
            View topView = viewGroup.getChildAt(0);
            if(topView!=null&&topView.getLayoutParams()!=null&&topView.getLayoutParams().height==statusBarHeight){
                //避免重复添加View
                topView.setBackgroundColor(color);
                return;
            }
            //使childView预留空间
            if(topView!=null){
                if(isColor==true) {
                    ViewCompat.setFitsSystemWindows(topView, true);//false表示全屏模式，true表示着色模式
                }
                else {
                    ViewCompat.setFitsSystemWindows(topView, false);//false表示全屏模式，true表示着色模式
                }
            }
            //添加假View
            topView=new View(activity);
            ViewGroup.LayoutParams lp=new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,statusBarHeight);
            topView.setBackgroundColor(color);
            viewGroup.addView(topView,0,lp);

        }
    }

    private static int getStatusBarHeight(AppCompatActivity activity) {
        int result=0;
        int resuorceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if(resuorceId>0){
            result=activity.getResources().getDimensionPixelSize(resuorceId);
        }
        return result;
    }
}
