package com.registration.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.registration.R;
import com.registration.base.BaseActivity;
import com.registration.util.CheckPermissionUtils;
import com.registration.view.TicketInfoDialog;

import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * 登录界面
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.tv_enter)
    TextView  mTvEnter;
    @BindView(R.id.tv_setting)
    TextView  mTvSetting;
    @BindView(R.id.iv)
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //全屏
        BarUtils.setStatusBarAlpha(this, 0);
        DeviceUtils.getModel();
    }

    @Override
    public int getContentViewID() {
        return R.layout.activity_login;
    }

    @OnClick({R.id.tv_enter, R.id.tv_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_enter:         //用户登录
                initPermissionAndScans();
                break;
            case R.id.tv_setting:           //设置
                Intent intent = new Intent(LoginActivity.this, SettingActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideLoading();
    }

    /**
     * 初始化检查权限和跳转
     */
    private void initPermissionAndScans() {
        //检查权限
        String[] permissions = CheckPermissionUtils.checkPermission(LoginActivity.this);
        if (permissions.length == 0) {//权限都申请了
            TicketInfoDialog ticketInfoDialog = new TicketInfoDialog();
            ticketInfoDialog.login(LoginActivity.this);
        } else {
            //申请权限
            ActivityCompat.requestPermissions(LoginActivity.this, permissions, 0);
        }
    }

}
