package com.registration.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.adapter.DialogAadpter;
import com.registration.adapter.RegistrationAdapter;
import com.registration.base.BaseActivity;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.RecordsBean;
import com.registration.moudle.ReportMoudle;
import com.registration.moudle.SignInMoudle;
import com.registration.moudle.TicketInfo;
import com.registration.moudle.TransferPictureMoudle;
import com.registration.net.ApiResponse;
import com.registration.net.CommonCallBack;
import com.registration.net.IdaParams;
import com.registration.net.ServiceGenerager;
import com.registration.util.BitmapUtils;
import com.registration.util.IdaMemberNumBuffer;
import com.registration.util.LoadingDialogUtil;
import com.registration.util.StringUtils;
import com.registration.util.TimeUtil;
import com.registration.view.TicketInfoDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.registration.constants.Constants.TAKE_PHOTO;

public class TeamRegistionctivity extends BaseActivity {
    @BindView(R.id.tv_team_before_the_number)
    TextView           mTvTeamBeforeTheNumber;
    @BindView(R.id.tv_team_headcount)
    TextView           mTvTeamHeadcount;
    @BindView(R.id.team_info_recyclerview)
    RecyclerView       mTeamInfoRecyclerview;
    @BindView(R.id.normal_view)
    SmartRefreshLayout mSmartLayout;
    @BindView(R.id.common_toolbar)
    Toolbar            toolbar;
    @BindView(R.id.tv_team_registration_ida)
    TextView           mTvTeamRegistrationIda;
    @BindView(R.id.btn_team_sign)
    Button             mBtnTeamSign;
    private ArrayList<TicketInfo> mTicketInfoList;
    private RegistrationAdapter   mAdapter;
    private int                   mReportTeamId;
    private TicketInfoDialog      mTicketInfoDialog;
    private TicketInfo            mTeamTicketInfo;
    private List<SignInMoudle>    mLeaderList;
    private TicketInfo            mTicketInfo1;
    private Uri                   mPhotoUri;
    private String                mPhotoName;

    @Override
    public int getContentViewID() {
        return R.layout.activity_team_registion;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        //设置显示toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //设置title
        toolbar.setTitle(getString(R.string.team_registion));
        //dialog
        mTicketInfoDialog = new TicketInfoDialog();
        //团队长/领队
        mTeamTicketInfo = (TicketInfo) getIntent().getSerializableExtra("teamticketInfo");
        //团队id
        mReportTeamId = mTeamTicketInfo.getReportTeamId();
        //团队所有门票信息
        mTicketInfoList = new ArrayList<>();
        //团队长签到记录
        mLeaderList = new ArrayList<>();
        //排列方式
        mTeamInfoRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        //设置adapter
        mAdapter = new RegistrationAdapter(mTicketInfoList, mLeaderList);
        mTeamInfoRecyclerview.setAdapter(mAdapter);
        //根据团队id获取团队数据
        getTeamListByid(mReportTeamId);
        //获取签到记录
        getLeaderSignByid();
        //事件监听
        initListener();
    }


    //获取为团队id 用户门票数据集合
    private void getTeamListByid(int reportTeamId) {
        AppDatabase.getInstance(this)
                .getTicketInfoDao()
                .queryReportTeamId(reportTeamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<TicketInfo>>() {
                    @Override
                    public void accept(List<TicketInfo> ticketInfos) throws Exception {
                        hideLoading();
                        //已报到
                        if (!TeamRegistionctivity.this.isFinishing()) {
                            if (ticketInfos != null && ticketInfos.size() > 0) {
                                //优先级排序 是否团队长
                                Collections.sort(ticketInfos, new Comparator<TicketInfo>() {
                                    @Override
                                    public int compare(TicketInfo o1, TicketInfo o2) {
                                        return o2.getLeaderFlag() - o1.getLeaderFlag();
                                    }
                                });

                                //清除原先数据
                                mTicketInfoList.clear();
                                //团队长信息
                                mTicketInfoList.add(new TicketInfo(ticketInfos.get(0), 1));

                                mTeamTicketInfo = mTicketInfoList.get(0);
                                //移除团队长信息  团队长/领队不计入团队总人数中
                                ticketInfos.remove(0);
                                //添加数据库team 成员数据
                                for (int i = 0; i < ticketInfos.size(); i++) {
                                    mTicketInfoList.add(new TicketInfo(ticketInfos.get(i), 0));
                                }
                                //更新数据
                                mAdapter.notifyDataSetChanged();
                                //取消下拉刷新效果
                                if (mSmartLayout != null) {
                                    mSmartLayout.finishRefresh();
                                }
                                //已报到集合数据
                                List<TicketInfo> totalList = new ArrayList<>();
                                //IDA成员 集合数据
                                List<TicketInfo> idaList = new ArrayList<>();

                                //遍历集合 筛选并添加符合条件的数据
                                for (int i = 0; i < mTicketInfoList.size(); i++) {
                                    TicketInfo ticketInfo = mTicketInfoList.get(i);
                                    //                                    if (ticketInfo.getCheckedFlag() == 1) {//是否报到
                                    //                                        registrationList.add(ticketInfo);
                                    //                                    }
                                    //添加ida成员数据信息
                                    if (IdaMemberNumBuffer.ismemberNum(ticketInfo.getMemberNum())) {
                                        idaList.add(ticketInfo);
                                    }
                                    //参会人数
                                    if (ticketInfo.getParticipationFlag() == 0||StringUtils.isNotEmpty(ticketInfo.getIdentityName())) {
                                    } else {
                                        totalList.add(ticketInfo);
                                    }


                                }
                                //学员人数
                                if (idaList.size() == totalList.size()) {
                                    mTvTeamBeforeTheNumber.setText("0");
                                } else {
                                    String size = String.valueOf(totalList.size() - idaList.size());
                                    mTvTeamBeforeTheNumber.setText(size);
                                }

                                //IDA人数
                                mTvTeamRegistrationIda.setText(String.valueOf(idaList.size()));
                                //总人数
                                mTvTeamHeadcount.setText(String.valueOf(totalList.size()));
                                /*//弹出 团队长/领队签到记录
                                if (mTicketInfoList.get(0).getCheckedFlag()==1){
                                    mTicketInfoDialog.registHint(TeamRegistionctivity.this, mTicketInfoList.get(0), null, true);
                                }*/
                            } else {
                                mTvTeamBeforeTheNumber.setText("0");
                                mTvTeamRegistrationIda.setText("0");
                            }
                        }
                    }
                });
    }

    private void initListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TeamRegistionctivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //下拉刷新
        mSmartLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                //获取门票信息
                getTeamListByid(mReportTeamId);
            }
        });
        //上来加载更多
        mSmartLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mSmartLayout.finishLoadMore();
                ToastUtils.showShort(getString(R.string.no_more));
            }
        });

        //用户信息item 点击
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                TicketInfo ticketInfo = (TicketInfo) adapter.getData().get(position);
                mTicketInfoDialog.registHint(TeamRegistionctivity.this, ticketInfo, null, true);
            }
        });

        mBtnTeamSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取当前时间戳
                String currenttime = String.valueOf(Calendar.getInstance().getTimeInMillis());
//                String time = TimeUtil.getStringByFormat(currenttime, TimeUtil.dateFormat);
                //获取当前报到设备
                String device = Hawk.get(Constants.DEVICE);
                //显示加载框
                showLoading();
                //更新签到记录
                mTicketInfoDialog.setSign(TeamRegistionctivity.this, currenttime, device, mTicketInfoList);
            }
        });
    }

    //获取签到记录
    private void getLeaderSignByid() {
        AppDatabase.getInstance(TeamRegistionctivity.this).getSignInDao()
                .getSignInById(mTeamTicketInfo.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<SignInMoudle>>() {
                    @Override
                    public void accept(List<SignInMoudle> signInMoudleList) throws Exception {
                        mLeaderList.clear();
                        if (signInMoudleList.size() > 0) {
                            mLeaderList.addAll(signInMoudleList);
                            mAdapter.notifyDataSetChanged();
                            mTicketInfoDialog.registHint(TeamRegistionctivity.this, mTeamTicketInfo, null, true);
                        }

                    }
                });
        getReportStatus();
    }


    //获取签到记录
    private void getReportStatus() {
        //显示加载框
        showLoading();
        //会议届数
        RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
        ServiceGenerager.getInstance().createService().reportStatus(IdaParams.reportStatus(mTeamTicketInfo.getTicketCode(), recordsBean.getMeetingId()))
                .enqueue(new CommonCallBack<ApiResponse<List<ReportMoudle>>>() {
                    @Override
                    public void onSuc(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                        LoadingDialogUtil.hideLoadingDialog();
                        if (response.body() != null && response.body().data != null) {
                            Log.d("test", "团队长签到记录" + response.body().data.toString());
                            List<ReportMoudle> reportMoudles = response.body().data;
                            List<SignInMoudle> signInMoudleList = new ArrayList<>();
                            for (int i = 0; i < reportMoudles.size(); i++) {
                                ReportMoudle reportMoudle = reportMoudles.get(i);
                                signInMoudleList.add(new SignInMoudle(reportMoudle.getCreateTime(), reportMoudle.getDevice(), reportMoudle.getId()));
                            }
                            mLeaderList.clear();
                            //添加全部报到信息
                            mLeaderList.addAll(signInMoudleList);
                            //刷新签到信息
                            mAdapter.notifyDataSetChanged();
                        }


                    }

                    @Override
                    public void onFail(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                        LoadingDialogUtil.hideLoadingDialog();
                    }
                });
    }

    //调用系统相机拍照
    private void takePhoto() {
        //指定拍照
        Intent getImageByCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //拍照图片名字
        mPhotoName = mTicketInfo1.getId() + ".jpg";
        //路径
        File photoFile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + Constants.APPPICTUREPATH, mPhotoName);

        try {
            if (photoFile.exists()) {
                photoFile.delete();
            }
            photoFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //拍照的uri
        if (Build.VERSION.SDK_INT >= 24) {
            mPhotoUri = FileProvider.getUriForFile(TeamRegistionctivity.this, getApplicationContext().getPackageName() + ".provider", photoFile);
        } else {
            mPhotoUri = Uri.fromFile(photoFile);
        }
        getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
        this.startActivityForResult(getImageByCamera, TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //打印log 方便测试
        Log.i("test", "onActivityResult");
        //如果回传码为0和回传码生成
        if (resultCode == RESULT_OK && requestCode == TAKE_PHOTO) {
            Bitmap bitmap = null;
            try {
                //获取原图片
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(mPhotoUri));
                //压缩图片
                Bitmap bitmaps = BitmapUtils.compressScale(bitmap);
                //更新相册图片
                boolean hasbitmap = BitmapUtils.addSignatureToGallery(TeamRegistionctivity.this, bitmaps, mPhotoName);
                if (hasbitmap) {   //是否保存成功
                    //回收bitmap
                    bitmap.recycle();
                    //会议届数
                    RecordsBean RecordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
                    //原先的dialog销毁
                    mTicketInfoDialog.dialogDismiss();
                    //设置用户门票的图片
                    mTicketInfo1.setTransferPicture(mPhotoName);
                    //更新门票信息
                    AppDatabase.getInstance(TeamRegistionctivity.this).getTicketInfoDao().updateUser(mTicketInfo1);
                    //重新加载用户门票信息
                    mTicketInfoDialog.registHint(TeamRegistionctivity.this, mTicketInfo1, null, true);
                    //上传的实体bean
                    TransferPictureMoudle transferPictureMoudle = new TransferPictureMoudle();
                    //图片转换成base64
                    String base64 = BitmapUtils.bitmapToBase64(bitmaps);
                    transferPictureMoudle.setBase64Image(base64);
                    //门票
                    transferPictureMoudle.setTicketCode(mTicketInfo1.getTicketCode());
                    //会议id
                    transferPictureMoudle.setMeetingId(RecordsBean.getMeetingId());
                    //网络上传用户门票图片
                    ServiceGenerager.getInstance().createService().transferPicture(transferPictureMoudle)
                            .enqueue(new CommonCallBack<ApiResponse<String>>() {
                                @Override
                                public void onSuc(String code, String errMsg, Response<ApiResponse<String>> response) {

                                }
                            });
                } else {
                    ToastUtils.showShort("图片保存失败");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(TicketInfo ticketInfo) {
        mTicketInfo1 = null;
        mTicketInfo1 = ticketInfo;
        if (StringUtils.isNotEmpty(ticketInfo.getUsername()))
            //拍照
            takePhoto();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TeamRegistionctivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
