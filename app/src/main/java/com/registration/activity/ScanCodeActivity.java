package com.registration.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.blankj.utilcode.util.ToastUtils;
import com.registration.R;
import com.registration.base.BaseActivity;
import com.uuzuche.lib_zxing.activity.CaptureFragment;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import butterknife.BindView;

/*
 * @创建者 huangxy
 * @创建时间  2021/2/4 10:08
 * @描述      ${TODO}   扫码报到
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class ScanCodeActivity extends BaseActivity {

    @BindView(R.id.common_toolbar)
    Toolbar toolbar;

    private CaptureFragment captureFragment;
    private boolean isOpen = false;

    @Override
    public int getContentViewID() {
        return R.layout.activity_second;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 为二维码扫描界面设置定制化界面
        captureFragment = new CaptureFragment();
        captureFragment.setAnalyzeCallback(analyzeCallback);
        CodeUtils.setFragmentArgs(captureFragment, R.layout.my_camera);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_my_container, captureFragment).commit();

        initView();
    }

    private void initView() {
        LinearLayout linearLayout = findViewById(R.id.linear1);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    CodeUtils.isLightEnable(true);
                    isOpen = true;
                } else {
                    CodeUtils.isLightEnable(false);
                    isOpen = false;
                }

            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(getString(R.string.scan_qr_code));
        setNavigationBack(toolbar);
    }

    /**
     * 二维码解析回调函数
     */
    CodeUtils.AnalyzeCallback analyzeCallback = new CodeUtils.AnalyzeCallback() {

        @Override
        public void onAnalyzeSuccess(Bitmap mBitmap, String result) {
            captureFragment.onPause();
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt(CodeUtils.RESULT_TYPE, CodeUtils.RESULT_SUCCESS);
            bundle.putString(CodeUtils.RESULT_STRING, result);
            intent.putExtras(bundle);
            ScanCodeActivity.this.setResult(RESULT_OK, intent);
            ScanCodeActivity.this.finish();
        }

        @Override
        public void onAnalyzeFailed() {
            captureFragment.onPause();
            ToastUtils.showLong(R.string.code_fail_hint);
            captureFragment.onResume();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
