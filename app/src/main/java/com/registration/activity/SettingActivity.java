package com.registration.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.base.BaseActivity;
import com.registration.constants.Constants;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.RecordsBean;
import com.registration.util.StringUtils;
import com.registration.view.TicketInfoDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 设置界面
 */
public class SettingActivity extends BaseActivity {
    @BindView(R.id.ll_metting)
    LinearLayout mLlMetting;
    @BindView(R.id.tv_period)
    TextView     mTvPeriod;
    @BindView(R.id.ll_period)
    LinearLayout mLlPeriod;
    @BindView(R.id.tv_checkpoint)
    TextView     mTvCheckpoint;
    @BindView(R.id.rl_checkpoint)
    LinearLayout mLlCheckpoint;
    @BindView(R.id.switch_register)
    Switch       mSwitchRegister;
    @BindView(R.id.switch_server)
    Switch       mSwitchServer;
    @BindView(R.id.tv_server)
    TextView     mTVServer;
    @BindView(R.id.btn_save)
    Button       mBtnSave;
    @BindView(R.id.tv_device)
    TextView     mTvDevice;
    @BindView(R.id.iv_back)
    ImageView    mIvBack;
    @BindView(R.id.ll_sync_server)
    LinearLayout mLlsyncServer;
    @BindView(R.id.tv_metting)
    TextView     mTvMetting;
    @BindView(R.id.tv_ali_server)
    TextView     mTvAliServer;

    private TicketInfoDialog mTicketInfoDialog;

    @Override
    public int getContentViewID() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTicketInfoDialog = new TicketInfoDialog();
        //返回按钮显示
        mIvBack.setVisibility(View.VISIBLE);
        //同步
        mLlsyncServer.setVisibility(View.GONE);

        //设置报到台
        if (Hawk.contains(Constants.CHECKPINT)) {
            CheckPointEntity checkPointEntity = Hawk.get(Constants.CHECKPINT);
            mTvCheckpoint.setText(checkPointEntity.getCheckpointName());
        }
        //设置会议
        if (Hawk.contains(Constants.MEETING)) {
            RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.MEETING);
            mTvMetting.setText(recordsBean.getAbbreviate());
        }
        //设置界别
        if (Hawk.contains(Constants.DIVSION)) {
            RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
            mTvPeriod.setText(String.valueOf(recordsBean.getDivisionYear()));
        }

        //是否设置了外部服务器
        if (Hawk.contains(Constants.URL)) {
            String service = (String) Hawk.get(Constants.URL);
            mTVServer.setText(service);
            mSwitchServer.setChecked(false);
        } else {
            mSwitchServer.setChecked(true);
        }
        //报到设备
        if (Hawk.contains(Constants.DEVICE)) {
            String device = (String) Hawk.get(Constants.DEVICE);
            mTvDevice.setText(device);
        }

        mTvAliServer.setText(Constants.BASE_URL);
        //事件监听
        initSwitchListener();
    }

    private void initSwitchListener() {
        //是否允许不同报到台报到
        if (Hawk.contains(Constants.ISCHECKPOINT)) {
            mSwitchRegister.setChecked(true);
        } else {
            mSwitchRegister.setChecked(false);
        }
        //是否允许不同报到台报到 监听
        mSwitchRegister.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Hawk.put(Constants.ISCHECKPOINT, true);
                } else {
                    if (Hawk.contains(Constants.ISCHECKPOINT)) {
                        Hawk.delete(Constants.ISCHECKPOINT);
                    }
                }
            }
        });

        //是否使用阿里服务器 监听
        mSwitchServer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {  //使用阿里服务器
                    //删除外部服务器地址
                    if (Hawk.contains(Constants.URL)) {
                        Hawk.delete(Constants.URL);
                    }
                    //外部服务地址为空
                    mTVServer.setText(null);
                } else {        //使用外部服务器
                    String server = mTVServer.getText().toString().trim();
                    //外部服务器是否设置
                    if (StringUtils.isNotEmpty(server)) {
                        //保存外部服务器
                        Hawk.put(Constants.URL, server);
                    } else {
                        //设置已选中
                        //                        mSwitchServer.setChecked(true);
                        if (Hawk.contains(Constants.URL)){
                        }else {
                            mTicketInfoDialog.showServer(SettingActivity.this, mTVServer, getString(R.string.server_hint), mSwitchServer, true);
                        }
                    }
                }
            }
        });
    }

    @OnClick({R.id.ll_metting, R.id.ll_period, R.id.rl_checkpoint, R.id.ll_server, R.id.ll_device,R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:           //返回
                finish();
                break;
            case R.id.ll_metting:           //会议
                if (Hawk.contains(Constants.MEETINGLIST)){
                    mTicketInfoDialog.showMettingList(SettingActivity.this, mTvMetting, mTvMetting, false);
                }else {
                    mTicketInfoDialog.registCodeHint(SettingActivity.this,"请先登录后设置");
                }

                break;
            case R.id.ll_period:            //界别
                if (Hawk.contains(Constants.MEETINGLIST)){
                    mTicketInfoDialog.showMettingList(SettingActivity.this, mTvMetting, mTvPeriod, true);
                }else {
                    mTicketInfoDialog.registCodeHint(SettingActivity.this,"请先登录后设置");
                }

                break;
            case R.id.rl_checkpoint:        //报到台
                if (Hawk.contains(Constants.DIVSION)){
                    mTicketInfoDialog.showCheckPoint(SettingActivity.this, mTvCheckpoint);
                }else {
                    mTicketInfoDialog.registCodeHint(SettingActivity.this,getString(R.string.meting_division_hint));
                }
                break;
            case R.id.ll_server:        //设置其他服务器
                mTicketInfoDialog.showServer(SettingActivity.this, mTVServer, getString(R.string.server_hint), mSwitchServer,true);
                break;
            case R.id.ll_device:        //设备名称
                mTicketInfoDialog.showServer(SettingActivity.this, mTvDevice, getString(R.string.device_hint), null,false);
                break;
            case R.id.ll_sync_server:        //同步数据
                showLoading();
                break;

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideLoading();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        hideLoading();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode()==KeyEvent.KEYCODE_BACK){
            hideLoading();
        }
        return super.dispatchKeyEvent(event);
    }
}
