package com.registration.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.registration.R;
import com.registration.adapter.HistoryAdapter;
import com.registration.base.BaseActivity;
import com.registration.database.AppDatabase;
import com.registration.moudle.HistoryEntity;
import com.registration.util.DrawableUtils;
import com.registration.util.IntentUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HistoryActivity extends BaseActivity {

    @BindView(R.id.common_toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerview;

    private HistoryAdapter mAdapter = null;
    private List<HistoryEntity> historyList = new ArrayList<>();

    @Override
    public int getContentViewID() {
        return R.layout.activity_history;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar.setNavigationIcon(DrawableUtils.TintWithRes(this, R.drawable.back, R.color.icon_color));
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        showLoading();
        mAdapter = new HistoryAdapter(historyList);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerview.setAdapter(mAdapter);
        //用户信息item 点击
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //HistoryEntity item = (HistoryEntity) adapter.getData().get(position);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        int ciType = getIntent().getIntExtra(IntentUtil.ARG_PARAM1, 0);
        int ticketId = getIntent().getIntExtra(IntentUtil.ARG_PARAM2, 0);
        if (ticketId > 0) {
            AppDatabase.getInstance(this).getHistoryDao().getHistoryRecord(ticketId, ciType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HistoryEntity>>() {
                    @Override
                    public void accept(List<HistoryEntity> list) throws Exception {
                        historyList.clear();
                        if (list != null && list.size() > 0) {
                            historyList.addAll(list);
                        }
                        mAdapter.notifyDataSetChanged();
                        hideLoading();
                        if (historyList.size() == 0) {
                            ToastUtils.showLong(R.string.no_historys_hint);
                        }
                    }
                });
        } else {
            ToastUtils.showLong(R.string.init_error_hint);
        }
    }
}
