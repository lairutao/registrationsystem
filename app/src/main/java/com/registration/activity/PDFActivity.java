//package com.registration.activity;
//
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.Bundle;
//import android.util.Log;
//import android.webkit.DownloadListener;
//import android.webkit.WebResourceRequest;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//
//import com.joanzapata.pdfview.PDFView;
//import com.joanzapata.pdfview.listener.OnLoadCompleteListener;
//import com.joanzapata.pdfview.listener.OnPageChangeListener;
//import com.registration.R;
//import com.registration.util.DownloadUtil;
//
//import androidx.appcompat.app.AppCompatActivity;
//import butterknife.BindView;
//import butterknife.ButterKnife;
//
//public class PDFActivity extends AppCompatActivity implements OnLoadCompleteListener, OnPageChangeListener {
//    @BindView(R.id.webview)
//    WebView mWebview;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_pdf);
//        ButterKnife.bind(this);
//        //        //获取设置
//        //        WebSettings webSettings = mWebview.getSettings();
//        //        //支持 javascript交互
//        //        webSettings.setJavaScriptEnabled(true);
//        //
//        //        //允许js弹出窗口
//        //        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
//        //        //布局算法
//        //        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//        //        //是否支持viewport属性，
//        //        webSettings.setUseWideViewPort(true);
//        //        //是否使用overviewmode 加载页面
//        //        webSettings.setLoadWithOverviewMode(true);
//        //        //支持缩放
//        //        webSettings.setSupportZoom(true);
//        //
//        //        //加载pdf
//        ////        http://storage.xuetangx.com/public_assets/xuetangx/PDF/PlayerAPI_v1.0.6.pdf
//        //        mWebview.loadUrl("https://docs.google.com/viewer?url=http://storage.xuetangx.com/public_assets/xuetangx/PDF/PlayerAPI_v1.0.6.pdf");
//        //        mWebview.loadUrl("https://www.baidu.com");
//        //        mWebview.getSettings().setJavaScriptEnabled(true);
//        //        mWebview.getSettings().setSupportZoom(true);
//        //        mWebview.getSettings().setDomStorageEnabled(true);
//        //        mWebview.getSettings().setAllowFileAccess(true);
//        //        mWebview.getSettings().setUseWideViewPort(true);
//        //        mWebview.getSettings().setBuiltInZoomControls(true);
//        //        mWebview.requestFocus();
//        //        mWebview.getSettings().setLoadWithOverviewMode(true);
//        //        mWebview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        //        String pdfUrl = "http://storage.xuetangx.com/public_assets/xuetangx/PDF/PlayerAPI_v1.0.6.pdf";
//        //        String data = "<iframe src='http://docs.google.com/gview?embedded=true&url="+pdfUrl+"'"+" width='100%' height='100%' s	tyle='border: none;'></iframe>";
//        //        mWebview.loadData(data, "text/html", "UTF-8");
//        loadPDF();
//    }
//
//    private void displayFromFile(String fileUrl, String fileName) {
//        //        mWebview.fileFromLocalStorage(this, this, this, fileUrl, fileName);   //设置pdf文件地址
//    }
//
//    @Override
//    public void loadComplete(int nbPages) {
//
//    }
//
//    @Override
//    public void onPageChanged(int page, int pageCount) {
//
//    }
//
//
//    private void loadPDF() {
//        mWebview.getSettings().setJavaScriptEnabled(true);
//        mWebview.getSettings().setSupportZoom(true);
//        mWebview.getSettings().setDomStorageEnabled(true);
//        mWebview.getSettings().setAllowFileAccess(true);
//        mWebview.getSettings().setUseWideViewPort(true);
//        mWebview.getSettings().setBuiltInZoomControls(true);
//        mWebview.requestFocus();
//        mWebview.getSettings().setLoadWithOverviewMode(true);
//        String pdfUrl = "http://www8.cao.go.jp/okinawa/8/2012/0409-1-1.pdf";
//        mWebview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + pdfUrl);
//
//        mWebview.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//            }
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                if (url.endsWith("pdf")) {
//                    DownloadUtil.get().download(url, "1.pdf", new DownloadUtil.OnDownloadListener() {
//                        @Override
//                        public void onDownloadSuccess() {
//                            Log.d("test","---------成功");
//                        }
//
//                        @Override
//                        public void onDownloading(int progress) {
//                            Log.d("test","---------"+progress);
//                        }
//
//                        @Override
//                        public void onDownloadFailed() {
//                            Log.d("test","---------失败");
//                        }
//                    });
//                }
//                view.loadUrl(url);
//                return true;
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//
//            }
//
//            @Override
//            public void onReceivedError(WebView view, int errorCode,
//                                        String description, String failingUrl) {
//                super.onReceivedError(view, errorCode, description, failingUrl);
//
//            }
//
//        });
//
//
//    }
//
//}
