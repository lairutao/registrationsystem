package com.registration.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.base.BaseActivity;
import com.registration.base.MyApplication;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.MessageMoudle;
import com.registration.moudle.RecordsBean;
import com.registration.moudle.TicketInfo;
import com.registration.moudle.TransferPictureMoudle;
import com.registration.net.ApiResponse;
import com.registration.net.CommonCallBack;
import com.registration.net.ServiceGenerager;
import com.registration.util.BitmapUtils;
import com.registration.util.StringUtils;
import com.registration.view.TicketInfoDialog;
import com.uuzuche.lib_zxing.activity.CaptureFragment;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.core.content.FileProvider;
import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.registration.constants.Constants.TAKE_PHOTO;

/**
 * 扫二维码后回调扫描结果
 */
public class SecondActivity extends BaseActivity {

    @BindView(R.id.common_toolbar)
    Toolbar toolbar;
    private CaptureFragment  captureFragment;
    private boolean          isOpen = false;
    private List<TicketInfo> mTicketInfoList;
    private TicketInfoDialog mTicketInfoDialog;
    private TicketInfo       mTicketInfo;
    private TicketInfo       mTicketInfo1;
    private Uri              mPhotoUri;
    private String           mPhotoName;

    @Override
    public int getContentViewID() {
        return R.layout.activity_second;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        captureFragment = new CaptureFragment();
        mTicketInfoList = MyApplication.getInstance().getTicketList();
        mTicketInfoDialog = new TicketInfoDialog();
        // 为二维码扫描界面设置定制化界面
        CodeUtils.setFragmentArgs(captureFragment, R.layout.my_camera);

        captureFragment.setAnalyzeCallback(analyzeCallback);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_my_container, captureFragment).commit();

        initView();
    }


    private void initView() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear1);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    CodeUtils.isLightEnable(true);
                    isOpen = true;
                } else {
                    CodeUtils.isLightEnable(false);
                    isOpen = false;
                }

            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        toolbar.setTitle(getString(R.string.scan_qr_code));
    }


    /**
     * 二维码解析回调函数
     */
    CodeUtils.AnalyzeCallback analyzeCallback = new CodeUtils.AnalyzeCallback() {
        @Override
        public void onAnalyzeSuccess(Bitmap mBitmap, String result) {
            //            Intent resultIntent = new Intent();
            //            Bundle bundle = new Bundle();
            //            bundle.putInt(CodeUtils.RESULT_TYPE, CodeUtils.RESULT_SUCCESS);
            //            bundle.putString(CodeUtils.RESULT_STRING, result);
            //            resultIntent.putExtras(bundle);
            //            SecondActivity.this.setResult(RESULT_OK, resultIntent);
            //            SecondActivity.this.finish();
            obser(result);
            captureFragment.onPause();
        }

        @Override
        public void onAnalyzeFailed() {
            //            Intent resultIntent = new Intent();
            //            Bundle bundle = new Bundle();
            //            bundle.putInt(CodeUtils.RESULT_TYPE, CodeUtils.RESULT_FAILED);
            //            bundle.putString(CodeUtils.RESULT_STRING, "");
            //            resultIntent.putExtras(bundle);
            //            SecondActivity.this.setResult(RESULT_OK, resultIntent);
            //            SecondActivity.this.finish();
            Toast.makeText(SecondActivity.this, getString(R.string.code_fail_hint), Toast.LENGTH_LONG).show();
            captureFragment.onPause();
            captureFragment.onResume();
        }
    };

    //异步查询操作 搜索
    private void obser(String code) {
        //创建观察者对象
        Observable.create(new ObservableOnSubscribe<TicketInfo>() {
            @Override
            public void subscribe(ObservableEmitter<TicketInfo> emitter) throws Exception {
                //用于保存符合条件的数据
                mTicketInfo = new TicketInfo();
                //搜索内容不区分大小写
                String searchText = code.toLowerCase();
                if (!searchText.equals("") || searchText.length() != 0) {
                    //遍历数据
                    for (int i = 0; i < mTicketInfoList.size(); i++) {
                        //对象实体数据 不区分大小写
                        String infoString = mTicketInfoList.get(i).toString().toLowerCase();
                        //添加符合条件的数据
                        if (!infoString.equals("") && infoString.contains(searchText) || infoString.startsWith(searchText)) {
                            mTicketInfo = mTicketInfoList.get(i);
                        }
                    }
                    Log.d("test", "--------" + mTicketInfo.toString().toLowerCase());
                    //设置最新数据
                    emitter.onNext(mTicketInfo);
                } else {
                    //设置数据
                    emitter.onNext(null);
                }

            }
        }).subscribeOn(Schedulers.io())             //指定subscribe（）发生再io线程
                .observeOn(AndroidSchedulers.mainThread())      //指定subscribe 的回调发生在主线程
                .subscribe(new Observer<TicketInfo>() {       //订阅事件
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(TicketInfo ticketInfo) {           //相当于onclick()/onevent()
                        //清除原先的数据
                        mTicketInfo = null;
                        //添加最新数据
                        if (ticketInfo != null && StringUtils.isNotEmpty(ticketInfo.getUsername())) {
                            if (ticketInfo.getReportTeamId() > 0) {
                                if (ticketInfo.getLeaderFlag() == 1) {   //是否是团队长，领队
                                    if (Hawk.contains(Constants.CHECKPINT)) {   //是否有设置报到台
                                        CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
                                        getTeamListByid(ticketInfo.getReportTeamId(), checkPointEntity);
                                    } else {  //提示领队 报到台
                                        mTicketInfoDialog.registHint(SecondActivity.this, ticketInfo, null, true);
                                    }

                                } else {//查询 团队长信息展示
                                    getTeamListByid(ticketInfo.getReportTeamId(), null);
                                }
                            } else { //个人报到  dialog
                                mTicketInfoDialog.registHint(SecondActivity.this, ticketInfo, null, false);
                            }

                        } else {        //二维码未找到
                            if (StringUtils.isNotEmpty(code)) {
                                mTicketInfoDialog.registCodeHint(SecondActivity.this, getString(R.string.no_find_hint) + code);
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                    }          //事件队列异常


                    @Override
                    public void onComplete() {
                    }                  //事件队列完结  标记
                });
    }


    //获取为团队id 用户门票数据集合
    private void getTeamListByid(int reportTeamId, CheckPointEntity checkPointEntity) {
        AppDatabase.getInstance(this)
                .getTicketInfoDao()
                .queryReportTeamIds(reportTeamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<TicketInfo>>() {
                    @Override
                    public void accept(List<TicketInfo> ticketInfos) throws Exception {
                        if (ticketInfos != null && ticketInfos.size() > 0) {
                            Collections.sort(ticketInfos, new Comparator<TicketInfo>() {//优先级排序 团队长
                                @Override
                                public int compare(TicketInfo o1, TicketInfo o2) {
                                    return o2.getLeaderFlag() - o1.getLeaderFlag();
                                }
                            });
                            if (checkPointEntity != null && checkPointEntity.getId() > 0) {
                                //团队长/领队和队员是否是当前报到台
                                if (checkPointEntity.getId() == ticketInfos.get(0).getCheckpointId() || checkPointEntity.getId() == ticketInfos.get(1).getCheckpointId()) {
                                    Intent intent = new Intent(SecondActivity.this, TeamRegistionctivity.class);
                                    intent.putExtra("teamticketInfo", ticketInfos.get(0));
                                    startActivity(intent);
                                } else {        //提示报到台信息
                                    mTicketInfoDialog.registHint(SecondActivity.this, ticketInfos.get(0), "您属于"+ticketInfos.get(0).getReportTeamName()+"，请提醒您的团队长/领队 "+ticketInfos.get(0).getUsername()+" 前来报到", true);
                                }
                            } else {
                                TicketInfo LeaderticketInfo = ticketInfos.get(0);
                                if (LeaderticketInfo.getLeaderFlag() == 1) {  //是否是团队长
                                    mTicketInfoDialog.showDialog(SecondActivity.this, LeaderticketInfo, getString(R.string.checkpoint_hint_setting)+LeaderticketInfo.getCheckpointName(), true);
                                } else {        //该团队没有团队长/领队
                                    mTicketInfoDialog.registCodeHint(SecondActivity.this, getString(R.string.team_hint2));
                                }
                            }
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    //接收到签到信息  刷新扫码  可连续扫码
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageMoudle message) {
        MessageMoudle messageMoudle = message;
        if (StringUtils.isNotEmpty(messageMoudle.getMessage())) {
            if (analyzeCallback != null) {
                // 恢复线程
                synchronized (analyzeCallback) {
                    captureFragment.onResume();
                }

            }
        }
    }


    //调用系统相机拍照
    private void takePhoto() {
        //指定拍照
        Intent getImageByCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //拍照图片名字
        mPhotoName = mTicketInfo1.getId() + ".jpg";
        //路径
        File photoFile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + Constants.APPPICTUREPATH, mPhotoName);
        //拍照的uri
        mPhotoUri = FileProvider.getUriForFile(SecondActivity.this, getApplicationContext().getPackageName() + ".provider", photoFile);
        getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
        startActivityForResult(getImageByCamera, TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //打印log 方便测试
        Log.i("test", "onActivityResult");
        //如果回传码为0和回传码生成
        if (resultCode == RESULT_OK && requestCode == TAKE_PHOTO) {
            Bitmap bitmap = null;
            try {
                //获取原图片
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(mPhotoUri));
                //压缩图片
                Bitmap bitmaps = BitmapUtils.compressScale(bitmap);
                //更新相册图片
                boolean hasbitmap = BitmapUtils.addSignatureToGallery(SecondActivity.this, bitmaps, mPhotoName);
                if (hasbitmap) {   //是否保存成功
                    //回收bitmap
                    bitmap.recycle();
                    //会议届数
                    RecordsBean RecordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
                    //原先的dialog销毁
                    mTicketInfoDialog.dialogDismiss();
                    //设置用户门票的图片
                    mTicketInfo1.setTransferPicture(mPhotoName);
                    //更新门票信息
                    AppDatabase.getInstance(SecondActivity.this).getTicketInfoDao().updateUser(mTicketInfo1);
                    //重新加载用户门票信息
                    mTicketInfoDialog.registHint(SecondActivity.this, mTicketInfo1, null, true);
                    //上传的实体bean
                    TransferPictureMoudle transferPictureMoudle = new TransferPictureMoudle();
                    //图片转换成base64
                    String base64 = BitmapUtils.bitmapToBase64(bitmaps);
                    transferPictureMoudle.setBase64Image(base64);

                    //门票
                    transferPictureMoudle.setTicketCode(mTicketInfo1.getTicketCode());
                    //会议id
                    transferPictureMoudle.setMeetingId(RecordsBean.getMeetingId());
                    //网络上传用户门票图片
                    ServiceGenerager.getInstance().createService().transferPicture(transferPictureMoudle)
                            .enqueue(new CommonCallBack<ApiResponse<String>>() {
                                @Override
                                public void onSuc(String code, String errMsg, Response<ApiResponse<String>> response) {

                                }
                            });
                } else {
                    ToastUtils.showShort("图片保存失败");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(TicketInfo ticketInfo) {
        mTicketInfo1 = null;
        mTicketInfo1 = ticketInfo;
        if (StringUtils.isNotEmpty(ticketInfo.getUsername()))
            //拍照
            takePhoto();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SecondActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}

