package com.registration.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.registration.R;
import com.registration.adapter.BottomViewAdapter;
import com.registration.base.BaseActivity;
import com.registration.base.BaseFragment;
import com.registration.database.AppDatabase;
import com.registration.fragment.CheckInFragment;
import com.registration.fragment.HistoryFragment;
import com.registration.fragment.SettingFragment;
import com.registration.fragment.StatisticsFragment;
import com.registration.moudle.HistoryEntity;
import com.registration.view.CustomViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseActivity {

    @BindView(R.id.bottom_navigation_view)
    BottomNavigationView mBottomNavigationView;
    @BindView(R.id.viewpager_launch)
    CustomViewPager mViewpager;

    private List<BaseFragment> mFragments;
    private List<HistoryEntity> mHistorys;
    private LinearLayout badgeLayout;
    private TextView badgeView;
    private MenuItem menuItem;

    @Override
    public int getContentViewID() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        initViewPagerWithFragment();
        initBottomNavigationView();
    }

    //初始化viewpager+fragment
    private void initViewPagerWithFragment() {

        mFragments = new ArrayList<>();
        mFragments.add(new CheckInFragment());
        mFragments.add(new SettingFragment());
        mFragments.add(new HistoryFragment());
        mFragments.add(new StatisticsFragment());

        //viewpager 滑动监听
        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int position) {
                if (menuItem != null) {
                    menuItem.setChecked(false);
                } else {
                    mBottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                menuItem = mBottomNavigationView.getMenu().getItem(position);
                menuItem.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        //viewpager adapter
        BottomViewAdapter adapter = new BottomViewAdapter(getSupportFragmentManager(), mFragments);
        mViewpager.setOffscreenPageLimit(mFragments.size());
        mViewpager.setAdapter(adapter);
    }

    //初始化
    private void initBottomNavigationView() {

        //底部按钮点击监听
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.tab_registrations: //报到
                        mViewpager.setCurrentItem(0, false);
                        break;
                    case R.id.tab_setting:       //设置
                        mViewpager.setCurrentItem(1, false);
                        break;
                    case R.id.tab_history:       //History
                        mViewpager.setCurrentItem(2, false);
                        break;
                    case R.id.tab_statistics:    //统计
                        mViewpager.setCurrentItem(3, false);
                        break;
                }
                return false;
            }
        });

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) mBottomNavigationView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(0);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_badgeview, itemView, false);
        badgeLayout = view.findViewById(R.id.ll_badgeview);
        badgeView = view.findViewById(R.id.tv_badgeview);
        badgeLayout.setVisibility(View.INVISIBLE);
        itemView.addView(view);

        //延迟执行
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateBadge();
            }
        }, 1234);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(HistoryEntity entity) {
        updateBadge();
    }

    private void updateBadge() {
        AppDatabase.getInstance(this).getHistoryDao().getSyncHistory()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<HistoryEntity>>() {
                @Override
                public void accept(List<HistoryEntity> list) throws Exception {
                    setBadgeView(list.size());
                    mHistorys = list;
                }
            });
    }

    private void setBadgeView(int num) {
        if (badgeView != null) {
            badgeView.setText(""+Math.min(num, 999));
            //badgeView.setText((num>99)? "99+": ""+num);
            badgeLayout.setVisibility((num>0)? View.VISIBLE: View.INVISIBLE);
        }
    }

    public List<HistoryEntity> getSyncHistory() {
        return mHistorys;
    }

    private long lastClickTime = 0;

    @Override
    public void onBackPressed() {
        long currentTime = System.currentTimeMillis();
        if ( currentTime - lastClickTime > 2000) {
            ToastUtils.showShort(R.string.back_pressed_hint);
            lastClickTime = currentTime;
        } else {
            System.exit(0);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
