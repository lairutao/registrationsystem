package com.registration.base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel<T> extends ViewModel {

    private MutableLiveData<T> liveData = new MutableLiveData<>();

    public MutableLiveData<T> getLiveData() {
        return liveData;
    }
}
