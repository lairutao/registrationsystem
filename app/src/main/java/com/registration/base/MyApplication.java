package com.registration.base;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.registration.constants.Constants;
import com.registration.moudle.TicketInfo;
import com.registration.util.CatchException;

import java.util.ArrayList;
import java.util.List;

/*
 * @创建者 lai
 * @创建时间  2019/6/28 16:58
 * @描述      ${TODO}
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class MyApplication extends Application {

    private Context mContext = null;
    private static MyApplication instance = null;

    //所有门票信息
    private List<TicketInfo> mTicketInfoList;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = getApplicationContext();
        Hawk.init(this).build();//初始化Hawk
        //捕获异常
        CatchException.getInstance().init(this);
    }

    public static MyApplication getInstance() {
        return instance;
    }

    public synchronized Context getContext() {
        return mContext;
    }

    public void setTicketList(List<TicketInfo> ticketInfoList) {
        mTicketInfoList=ticketInfoList;
    }

    public List<TicketInfo> getTicketList() {
        return  mTicketInfoList;
    }

    //是否有本地数据
    public boolean hasDataPick() {
        return Hawk.contains(Constants.SYNCTIME);
    }
}
