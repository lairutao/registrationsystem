package com.registration.base;

/**
 * Created by ZeQiang Fang on 2018/12/3.
 */

public interface BaseView {
    void showLoading();

    void hideLoading();

    void showErrorMsg(String errorMsg);

//    void showNetError(String error);
}
