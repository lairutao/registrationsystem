package com.registration.base;

import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.registration.R;
import com.registration.util.CommonUtils;
import com.registration.util.LoadingDialogUtil;
import com.registration.util.StatusBarUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/*
 * @创建者 lai
 * @创建时间  2019/6/28 16:06
 * @描述      ${TODO}     activity基类
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {
    private final String   TAG = getClass().getSimpleName();
    private       Unbinder unBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //隐藏标题栏
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setStatusBarColor();
        setContentView(getContentViewID());
        unBinder = ButterKnife.bind(this);
    }

    //修改状态栏的颜色
    protected void setStatusBarColor() {
        StatusBarUtil.StatusBarColor(this, true, false, getResources().getColor(R.color.main_red));
    }

    //设置返回键监听事件
    protected void setNavigationBack(@NonNull Toolbar toolbar) {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //显示加载框
    @Override
    public void showLoading() {
        if (!this.isFinishing())
        LoadingDialogUtil.showLoadingDialog(this);
    }

    //隐藏加载框
    @Override
    public void hideLoading() {
        LoadingDialogUtil.hideLoadingDialog();
    }

    //显示错误信息
    @Override
    public void showErrorMsg(String errorMsg) {
        CommonUtils.showSnackMessage(this, errorMsg);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unBinder != null && unBinder != Unbinder.EMPTY) {
            unBinder.unbind();
            unBinder = null;
        }
    }

    //设置布局
    public abstract int getContentViewID();
}
