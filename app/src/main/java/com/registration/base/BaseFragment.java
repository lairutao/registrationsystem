package com.registration.base;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.registration.util.CommonUtils;
import com.registration.util.LoadingDialogUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/*
 * @创建者 admin
 * @创建时间  2019/6/28 17:55
 * @描述      ${TODO}         fragment基类
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public abstract class BaseFragment extends Fragment implements BaseView {

    protected final String TAG = getClass().getSimpleName();
    private Unbinder unbinder;
    private View     rootView;

    /**
     * 是否可见
     */
    private boolean mIsVisible = false;

    /**
     * 是否刷新
     */
    private boolean mIsRefresh = false;

    //设置布局
    protected abstract int contentViewID();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(contentViewID(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lazyLoad(getUserVisibleHint());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        lazyLoad(isVisibleToUser);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        lazyLoad(!hidden);
    }

    private void lazyLoad(boolean isVisibleToUser){
        mIsVisible = isVisibleToUser;
        if (isAdded() && isVisibleToUser) {
            onActivate();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mIsVisible && mIsRefresh){
            mIsRefresh = false;
            onReactivate();
        }
    }

    protected void onActivate(){}

    protected void onReactivate(){
        onActivate();
    }

    public void setReactivate(boolean isRefresh){
        mIsRefresh = isRefresh;
    }

    public <T extends View> T findViewById(int id) {
        if (rootView == null) {
            throw new IllegalStateException(TAG + " does not have a view");
        }
        return rootView.findViewById(id);
    }

    //显示加载框
    @Override
    public void showLoading() {
        if (!getActivity().isFinishing())
        LoadingDialogUtil.showLoadingDialog(getActivity());
    }

    //隐藏加载框
    @Override
    public void hideLoading() {
        LoadingDialogUtil.hideLoadingDialog();
    }

    //显示错误信息
    @Override
    public void showErrorMsg(String errorMsg) {
        if (isAdded()) {
            CommonUtils.showSnackMessage(getActivity(), errorMsg);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null && unbinder != Unbinder.EMPTY) {
            unbinder.unbind();
            unbinder = null;
        }
    }
}
