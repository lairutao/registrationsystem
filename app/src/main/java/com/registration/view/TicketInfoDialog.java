package com.registration.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.orhanobut.hawk.Hawk;
import com.registration.R;
import com.registration.activity.MainActivity;
import com.registration.adapter.DialogAadpter;
import com.registration.constants.Constants;
import com.registration.database.AppDatabase;
import com.registration.moudle.AppUserPrizeRelationEntity;
import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.DialogMoudle;
import com.registration.moudle.MessageMoudle;
import com.registration.moudle.MettingMoudle;
import com.registration.moudle.RecordsBean;
import com.registration.moudle.ReportMoudle;
import com.registration.moudle.SignInMoudle;
import com.registration.moudle.SyncTicketList;
import com.registration.moudle.TicketInfo;
import com.registration.moudle.AuthMoudle;
import com.registration.net.ApiResponse;
import com.registration.net.CommonCallBack;
import com.registration.net.IdaParams;
import com.registration.net.ServiceGenerager;
import com.registration.util.IdaMemberNumBuffer;
import com.registration.util.JsonHelper;
import com.registration.util.LoadingDialogUtil;
import com.registration.util.NetworkUtils;
import com.registration.util.StringUtils;
import com.registration.util.TimeUtil;
import com.registration.util.UrlUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/*
 * @创建者 lai
 * @创建时间  2019/7/12 15:03
 * @描述      ${TODO}         门票签到信息dialog
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */public class TicketInfoDialog {
    //全部报到台
    private List<CheckPointEntity> mCheckPointEntities;
    //全部报到台adapter
    private BaseQuickAdapter<CheckPointEntity, BaseViewHolder> mCheckPointAdapter;
    //签到记录adapter
    private BaseQuickAdapter<SignInMoudle, BaseViewHolder>     mSignAdapter;
    //会议&& 界别
    private List<RecordsBean>                                  mRecordsBeanList;
    private BaseQuickAdapter<RecordsBean, BaseViewHolder>      mMettingAdapter;
    private NiftyDialogBuilder                                 mDialogBuildermetting;
    private NiftyDialogBuilder                                 mDialogBuildercheck;
    private NiftyDialogBuilder                                 mDialogBuilder;

    /**
     * 登录
     *
     * @param context
     */
    public void login(Context context) {
        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle(context.getResources().getString(R.string.login))                                  //.withTitle(null)  no title
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def//def
                .withMessage(null)                     //.withMessage(null)  no Msg
                .withDuration(180)                                          //def
                .withEffect(Effectstype.Newspager)                                         //def Effectstype.Slidetop
                .withButton1Text(View.VISIBLE, "登    录")                                      //def gone
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .setCustomView(R.layout.dialog_login, dialogBuilder.getContext())         //.setCustomView(View or ResId,context)
                .setButton1Click(new View.OnClickListener() {               //确定
                    @Override
                    public void onClick(View v) {
                        EditText editText = dialogBuilder.findViewById(R.id.edit_name);
                        EditText editpassword = dialogBuilder.findViewById(R.id.edit_password);
                        String loginuser = editText.getText().toString().trim();
                        String password = editpassword.getText().toString().trim();
                        if (StringUtils.isNotEmpty(loginuser) && StringUtils.isNotEmpty(password)) {   //账号密码输入是否为空
                            Log.d("test", "账号：" + loginuser + "密码：" + password);
                            if (NetworkUtils.isAvailable(context)) {         //有网络 验证登录
                                loginUser(loginuser, password, dialogBuilder, context);
                            } else {                                                        //无网络，获取已登录的账号密码登录
                                login(loginuser, password, dialogBuilder, context);
                            }
                        } else {
                            ToastUtils.showShort(R.string.login_hint);
                        }
                    }
                }).show();
    }

    /**
     * 无网络 登录
     *
     * @param loginuser     账号
     * @param password      密码
     * @param dialogBuilder dialog
     */
    private void login(String loginuser, String password, NiftyDialogBuilder dialogBuilder, Context context) {

        if (Hawk.contains(Constants.ACCOUNT)) {     //判断是否有保存的账号 密码信息
            //获取账号
            String login = Hawk.get(Constants.ACCOUNT);
            //获取密码
            String passwords = Hawk.get(Constants.PASSWORD);
            //判断账号密码是否正确
            if (loginuser.equals(login) && password.equals(passwords)) {
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
                dialogBuilder.dismiss();
            } else {
                ToastUtils.showShort(R.string.login_hint_error);
                dialogBuilder.dismiss();
            }
        } else {   //无网络也无账号信息保存 登录
            dialogBuilder.dismiss();
            //if (loginuser.equals("admin") && password.equals("111111")) {
            //    Intent intent = new Intent(context, MainActivity.class);
            //    context.startActivity(intent);
            //    ((Activity) context).finish();
            //    dialogBuilder.dismiss();
            //} else {
            //    ToastUtils.showShort(R.string.login_hint_error);
            //    dialogBuilder.dismiss();
            //}
        }
    }

    /**
     * 网络验证登录
     *
     * @param loginuser     账号
     * @param password      密码
     * @param dialogBuilder dialog
     */
    private void loginUser(String loginuser, String password, NiftyDialogBuilder dialogBuilder, Context context) {
        //删除cookie
        if (Hawk.contains(Constants.COOKIES)) {
            Hawk.delete(Constants.COOKIES);
        }
        //显示加载框
        LoadingDialogUtil.showLoadingDialog(context);
        ServiceGenerager.getInstance().createService().login(IdaParams.passwordCodeParams(loginuser, password))
                .enqueue(new CommonCallBack<ApiResponse<AuthMoudle>>() {
                    @Override
                    public void onSuc(String code, String errMsg, Response<ApiResponse<AuthMoudle>> response) {
                        if (code.equals("200")) {
                            //保存账号密码
                            Hawk.put(Constants.ACCOUNT, loginuser);
                            Hawk.put(Constants.PASSWORD, password);
                            //保持账号auth信息
                            AuthMoudle authMoudle = response.body().data;
                            if (authMoudle != null) {
                                Hawk.put(Constants.TOKEN, authMoudle.getToken());
                                Hawk.put(Constants.RANDOMKEY, authMoudle.getRandomKey());
                            }
                            //跳转到主界面
                            Intent intent = new Intent(context, MainActivity.class);
                            context.startActivity(intent);

                            ((Activity) context).finish();
                            //dialog消失
                            dialogBuilder.dismiss();
                            //隐藏加载框
                            LoadingDialogUtil.hideLoadingDialog();
                        }
                    }

                    @Override
                    public void onFail(String code, String errMsg, Response<ApiResponse<AuthMoudle>> response) {
                        //隐藏加载框
                        LoadingDialogUtil.hideLoadingDialog();
                        //请求失败，账号密码错误
                        if (StringUtils.isNotEmpty(errMsg)) {
                            ToastUtils.showShort(errMsg);
                        }
                        login(loginuser, password, dialogBuilder, context);

                    }
                });
    }


    /**
     * 签到信息
     *
     * @param context
     * @param ticketInfo
     */
    public void showDialog(Context context, TicketInfo ticketInfo, String message, boolean isgone) {
        mDialogBuilder = NiftyDialogBuilder.getInstance(context);
        mDialogBuilder
                .withTitle(message == null ? ticketInfo.getUsername() : context.getResources().getString(R.string.hint))                                  //.withTitle(null)  no title
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withMessage(message)                     //.withMessage(null)  no Msg
                .withMessageColor(context.getResources().getColor(R.color.black))                              //def  | withMessageColor(int resid)
                .withDuration(700)                                          //def
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .setCustomView(R.layout.dialog_list, mDialogBuilder.getContext())         //.setCustomView(View or ResId,context)
                .withButton1Text(View.VISIBLE, "取消")
                .withButton2Text(View.VISIBLE, context.getResources().getString(R.string.sign_in))
                .setButton1Click(new View.OnClickListener() {               //取消
                    @Override
                    public void onClick(View v) {
                        //隐藏dialog
                        mDialogBuilder.dismiss();
                        //secondActivity 接收数据  刷新扫码
                        EventBus.getDefault().post(new MessageMoudle(1, "notify"));
                    }
                })
                .setButton2Click(new View.OnClickListener() {           //签到
                    @Override
                    public void onClick(View v) {
                        if (Hawk.contains(Constants.DEVICE)) {
                            //获取签到设备
                            String device = (String) Hawk.get(Constants.DEVICE);
                            //签到
                            setSignInfo(ticketInfo, context, mDialogBuilder, device);
                        } else {
                            mDialogBuilder.dismiss();

                            //设置报到设备dialog
                            showServer(context, null, context.getResources().getString(R.string.device_hint), null, false);
                        }
                        //secondActivity 接收数据  刷新扫码
                        EventBus.getDefault().post(new MessageMoudle(1, "notify"));
                    }
                }).show();
        if (isgone) {
            mDialogBuilder.withButton1Text(View.VISIBLE, context.getResources().getString(R.string.confirm));
            mDialogBuilder.withButton2Text(View.GONE, "");
        }

        //用户门票信息


        List<DialogMoudle> list1 = new ArrayList<>();
        list1.add(new DialogMoudle(0, "姓名", ticketInfo.getUsername() + ticketInfo.getMemberNum()));
        if (StringUtils.isNotEmpty(ticketInfo.getMemberNum())) {
            String awards = IdaMemberNumBuffer.getPattern(ticketInfo.getMemberNum());
            list1.add(new DialogMoudle(0, "奖项", awards));
        } else {
            list1.add(new DialogMoudle(0, "奖项", ""));
        }

        list1.add(new DialogMoudle(0, "手机", ticketInfo.getMobile()));
        list1.add(new DialogMoudle(0, "团队", ticketInfo.getReportTeamName()));
        list1.add(new DialogMoudle(0, "报到台", ticketInfo.getCheckpointName()));
        list1.add(new DialogMoudle(0, "公司", ticketInfo.getCompanyName()));
        list1.add(new DialogMoudle(0, "备注", ""));

        if (StringUtils.isNotEmpty(ticketInfo.getRemark()) && StringUtils.isNotEmpty(ticketInfo.getTransferPicture())) {
            list1.add(new DialogMoudle(1, ticketInfo.getRemark(), ticketInfo.getTransferPicture()));
        } else if (StringUtils.isNotEmpty(ticketInfo.getRemark()) && StringUtils.isEmpty(ticketInfo.getTransferPicture())) {
            list1.add(new DialogMoudle(1, ticketInfo.getRemark(), ""));
        } else if (StringUtils.isEmpty(ticketInfo.getRemark()) && StringUtils.isNotEmpty(ticketInfo.getTransferPicture())) {
            list1.add(new DialogMoudle(1, "", ticketInfo.getTransferPicture()));
        }


        list1.add(new DialogMoudle(0, "二维码", ticketInfo.getQrcode()));
        list1.add(new DialogMoudle(0, "身份", ticketInfo.getIdentityName()));

        list1.add(new DialogMoudle(0, "email", ticketInfo.getEmail()));
        list1.add(new DialogMoudle(0, "会员", ticketInfo.getTip()));
        list1.add(new DialogMoudle(0, "地址", ""));
        list1.add(new DialogMoudle(0, "微信号", ""));
        list1.add(new DialogMoudle(0, "验证码", ticketInfo.getPasswd()));
        list1.add(new DialogMoudle(0, "code", ticketInfo.getTicketCode()));
        //签到信息
        List<SignInMoudle> list2 = new ArrayList<>();
        //绑定recyclerview
        RecyclerView recyclerView = mDialogBuilder.findViewById(R.id.dialog_recyclerview);
        //排列方式
        recyclerView.setLayoutManager(new LinearLayoutManager(mDialogBuilder.getContext()));
        DialogAadpter mDialogAadpter = new DialogAadpter(list1);
        //设置adapter
        recyclerView.setAdapter(mDialogAadpter);
        //获取签到数据
        getSiginfo(context, ticketInfo, list2, mDialogAadpter);

        //头部签到view
        View headview = getHeadView(recyclerView, list2);
        //添加头部签到信息
        mDialogAadpter.addHeaderView(headview);

        //点击监听
        mDialogAadpter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position == 6) {
                    //                    TextView textView = view.findViewById(R.id.tv_remark);
                    showRemark(context, "图片和备注", ticketInfo);
                }
            }
        });
    }

    //获取签到数据
    private void getSiginfo(Context context, TicketInfo ticketInfo, List<SignInMoudle> list2, DialogAadpter dialogAadpter) {
        //获取签到信息  根据用户 id
        AppDatabase.getInstance(context).getSignInDao().getSignInById(ticketInfo.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<SignInMoudle>>() {
                    @Override
                    public void accept(List<SignInMoudle> signInMoudles) throws Exception {
                        //清空原先的数据
                        list2.clear();
                        if (signInMoudles != null && signInMoudles.size() > 0) {
                            Collections.sort(signInMoudles, new Comparator<SignInMoudle>() {        //根据id 排序 最新签到
                                @Override
                                public int compare(SignInMoudle o1, SignInMoudle o2) {
                                    return o2.getId() - o1.getId();
                                }
                            });
                        }
                        //添加全部报到信息
                        list2.addAll(signInMoudles);
                        //刷新签到信息
                        mSignAdapter.notifyDataSetChanged();
                        //刷新界面数据
                        dialogAadpter.notifyDataSetChanged();
                    }
                });

        getReportStatus(ticketInfo, context, list2, dialogAadpter);
    }

    //获取签到记录
    private void getReportStatus(TicketInfo ticketInfo, Context context, List<SignInMoudle> list2, DialogAadpter dialogAadpter) {
        //显示加载框
        LoadingDialogUtil.showLoadingDialog(context);
        //会议届数
        RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
        ServiceGenerager.getInstance().createService().reportStatus(IdaParams.reportStatus(ticketInfo.getTicketCode(), recordsBean.getMeetingId()))
                .enqueue(new CommonCallBack<ApiResponse<List<ReportMoudle>>>() {
                    @Override
                    public void onSuc(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                        LoadingDialogUtil.hideLoadingDialog();
                        if (response.body() != null && response.body().data != null) {
                            Log.d("test", "签到记录" + response.body().data.toString());
                            List<ReportMoudle> reportMoudles = response.body().data;
                            List<SignInMoudle> signInMoudleList = new ArrayList<>();
                            for (int i = 0; i < reportMoudles.size(); i++) {
                                ReportMoudle reportMoudle = reportMoudles.get(i);
                                signInMoudleList.add(new SignInMoudle(reportMoudle.getCreateTime(), reportMoudle.getDevice(), reportMoudle.getId()));
                            }
                            list2.clear();
                            //添加全部报到信息
                            list2.addAll(signInMoudleList);
                            //刷新签到信息
                            mSignAdapter.notifyDataSetChanged();
                            //刷新界面数据
                            dialogAadpter.notifyDataSetChanged();
                        }


                    }

                    @Override
                    public void onFail(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                        LoadingDialogUtil.hideLoadingDialog();
                    }
                });
    }

    /**
     * 插入签到信息
     *
     * @param ticketInfo
     * @param context
     * @param dialogBuilder
     */
    private void setSignInfo(TicketInfo ticketInfo, Context context, NiftyDialogBuilder dialogBuilder, String device) {
        //获取当前时间戳
        String currenttime = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String time = TimeUtil.getStringByFormat(Calendar.getInstance().getTimeInMillis(), TimeUtil.dateFormat);
        Log.d("test", "时间-------" + time);
        //获取当前报到设备
        //        String device = DeviceUtils.getModel();
        //签到信息
        SignInMoudle signInMoudle = new SignInMoudle(currenttime, device, ticketInfo.getId());
        //插入签到数据
        AppDatabase.getInstance(context).getSignInDao().insertUser(signInMoudle);
        //更新签到
        TicketInfo ticketInfo1 = ticketInfo;
        ticketInfo1.setCheckedFlag(1);
        //更新数据库门票签到信息
        AppDatabase.getInstance(context).getTicketInfoDao().updateUser(ticketInfo1);

        //签到成功提示
        ToastUtils.showShort(context.getResources().getString(R.string.sign_hint_success));
        //隐藏dialog
        dialogBuilder.dismiss();
        getReport(ticketInfo, context, device);


    }

    /**
     * 网络提交签到数据
     *
     * @param ticketInfo
     * @param context
     * @param device
     */
    private void getReport(TicketInfo ticketInfo, Context context, String device) {
        //网络是否可用  提交签到数据到服务器
        if (NetworkUtils.isAvailable(context)) {
            RecordsBean RecordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);

            ServiceGenerager.getInstance()
                    .createService().getReport
                    (IdaParams.getSignInfo(RecordsBean.getMeetingId(),
                            ticketInfo.getTicketCode(), ticketInfo.getCheckpointId(), device))
                    .enqueue(new CommonCallBack<ApiResponse<List<ReportMoudle>>>() {
                        @Override
                        public void onSuc(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                            //                            if (response.body()!=null&& response.body().data!=null){
                            //                                List<ReportMoudle> reportMoudles=response.body().data;
                            //                                List<SignInMoudle>signInMoudleList=new ArrayList<>();
                            //                                for (int i=0;i<reportMoudles.size();i++){
                            //                                    ReportMoudle reportMoudle=reportMoudles.get(i);
                            //                                    signInMoudleList.add(new SignInMoudle(reportMoudle.getCreateTime(),reportMoudle.getDevice(),reportMoudle.getId()));
                            //                                }
                            //
                            //                                AppDatabase.getInstance(context).getSignInDao().insertAllSign(signInMoudleList);
                            //                            }
                        }

                        @Override
                        public void onFail(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                            //                            ToastUtils.showShort(code);
                        }
                    });
        }
    }

    //添加报到信息view
    private View getHeadView(RecyclerView recyclerView, List<SignInMoudle> list2) {
        View view = View.inflate(recyclerView.getContext(), R.layout.dialog_list_head, null);
        RecyclerView recyclerView1 = view.findViewById(R.id.dialog_recyclerview_head);
        recyclerView1.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        mSignAdapter = new BaseQuickAdapter<SignInMoudle, BaseViewHolder>(R.layout.item_list_dialog_header, list2) {
            @Override
            protected void convert(BaseViewHolder helper, SignInMoudle item) {
                if (StringUtils.isNotEmpty(item.getTime())) {
                    if (item.getTime().contains("-")) {
                        helper.setText(R.id.tv_time, item.getTime());    //报到时间
                    } else if (Long.parseLong(item.getTime()) > 0) {
                        helper.setText(R.id.tv_time, TimeUtil.getStringByFormat(Long.parseLong(item.getTime()), TimeUtil.dateFormatYMDHMS));    //报到时间
                    }

                }
                helper.setText(R.id.tv_device_content, item.getDevice());       //报到设备
            }
        };
        recyclerView1.setAdapter(mSignAdapter);
        return view;
    }


    /**
     * 不同报到台 报到提示
     *
     * @param context
     * @param ticketInfo
     */
    public void registHint(Context context, TicketInfo ticketInfo, String message, boolean isgone) {
        if (Hawk.contains(Constants.CHECKPINT) && Hawk.contains(Constants.DIVSION)) {
            //设置的报到台信息
            CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
            if (Hawk.contains(Constants.ISCHECKPOINT)) {        //是否允许不同报到台报到
                //显示当前点击用户门票信息
                showDialog(context, ticketInfo, message, isgone);
            } else {
                if (checkPointEntity.getId() == ticketInfo.getCheckpointId()) {   //判断是否报到台id 与用户门票信息是否一致
                    //显示当前点击用户门票信息
                    showDialog(context, ticketInfo, message, isgone);
                } else {        //不一致 提示报到台报到
                    showDialog(context, ticketInfo, context.getResources().getString(R.string.checkpoint_hint_setting) + ticketInfo.getCheckpointName(), true);
                }
            }
        } else {
            registCodeHint(context, context.getResources().getString(R.string.checkpoint_hint));
        }
    }

    /**
     * 扫码未找到对应数据 提示
     *
     * @param context
     * @param message
     */
    public void registCodeHint(Context context, String message) {
        NiftyDialogBuilder dialogBuilder2 = NiftyDialogBuilder.getInstance(context);
        dialogBuilder2
                .withTitle("提示")
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withMessage(message)                     //.withMessage(null)  no Msg
                .withMessageColor(context.getResources().getColor(R.color.black))                              //def  | withMessageColor(int resid)
                .withDuration(700)                                          //def
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, context.getResources().getString(R.string.confirm))
                .setButton1Click(new View.OnClickListener() {               //是
                    @Override
                    public void onClick(View v) {
                        //隐藏dialog
                        dialogBuilder2.dismiss();
                        //secondActivity 接收数据  刷新扫码
                        EventBus.getDefault().post(new MessageMoudle(1, "notify"));
                    }
                }).show();
    }

    /**
     * 报到确认弹框
     *
     * @param context      上下文
     * @param title         标题
     * @param text          内容
     */
    public void showConfirm(Context context, String title, String text, actionListener listener) {

        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle(title)
                .withTitleColor(context.getResources().getColor(R.color.black)) //def
                .withDuration(700)                                          //def
                .withMessage(null)
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, context.getResources().getString(R.string.confirm))
                .withButton2Text(View.VISIBLE, context.getResources().getString(R.string.cancel))
                .setCustomView(R.layout.dialog_confirm, dialogBuilder.getContext())
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {       //确定
                        if (listener != null) {
                            listener.onConfirm();
                        }
                        dialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {       //取消
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                }).show();

        TextView tip = dialogBuilder.findViewById(R.id.confirm_text);
        if (tip != null) {
            tip.setText(text);
        }
    }

    public interface actionListener {
        void onConfirm();
    }

    /**
     * 设置操作人
     *
     * @param context      上下文
     * @param textView     控件
     * @param title        标题
     */
    public void showOperator(Context context, TextView textView, String title) {
        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle(title)
                .withTitleColor(context.getResources().getColor(R.color.black)) //def
                .withDuration(700)                                          //def
                .withMessage(null)
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, context.getResources().getString(R.string.confirm))
                .withButton2Text(View.VISIBLE, "取消")
                .setCustomView(R.layout.dialog_text, dialogBuilder.getContext())
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {       //确定
                        EditText editText = dialogBuilder.findViewById(R.id.edit_text);
                        String operator = editText.getText().toString().trim();
                        //if (StringUtils.isNotEmpty(operator)) {       //判空
                            if (textView != null) {
                                //保存报到设备
                                String account = Hawk.get(Constants.ACCOUNT);
                                Hawk.put(Constants.ACCOUNT +"_"+ account, operator);
                                //同步刷新内容
                                textView.setText(operator);
                                //隐藏dialog
                                dialogBuilder.dismiss();
                            }
                        //}
                    }
                })
                .setButton2Click(new View.OnClickListener() {       //取消
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                }).show();
    }

    /**
     * 设置服务器或者报到设备
     *
     * @param context      上下文
     * @param textView     控件
     * @param title        标题
     * @param switchServer 开关按钮
     * @param isServer     是否设置服务器
     */
    public void showServer(Context context, TextView textView, String title, Switch switchServer, boolean isServer) {
        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle(title)
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withDuration(700)                                          //def
                .withMessage(null)
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, context.getResources().getString(R.string.confirm))
                .withButton2Text(View.VISIBLE, "取消")
                .setCustomView(R.layout.dialog_text, dialogBuilder.getContext())
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {       //确定
                        EditText editText = dialogBuilder.findViewById(R.id.edit_text);
                        String server = editText.getText().toString().trim();
                        if (StringUtils.isNotEmpty(server)) {       //判空
                            if (textView != null) {
                                if (isServer) {  //是否是服务器
                                    boolean isIp = UrlUtil.isIP(server);
                                    if (isIp) {
                                        //保存输入的服务器地址
                                        Hawk.put(Constants.URL, server);
                                        textView.setText(server);
                                        if (switchServer != null) {
                                            switchServer.setChecked(true);
                                        }
                                        //隐藏dialog
                                        dialogBuilder.dismiss();
                                    } else {
                                        ToastUtils.showShort("ip地址输入不合法,请重新输入");
                                    }
                                } else {
                                    //保存报到设备
                                    Hawk.put(Constants.DEVICE, server);
                                    //异步通信更新
                                    EventBus.getDefault().post(server);

                                    textView.setText(server);
                                    //隐藏dialog
                                    dialogBuilder.dismiss();
                                }

                            } else {
                                //保存报到设备
                                Hawk.put(Constants.DEVICE, server);
                                //异步通信更新
                                EventBus.getDefault().post(server);
                                if (textView!=null){
                                    textView.setText(server);
                                }
                                //隐藏dialog
                                dialogBuilder.dismiss();
                            }
                        }
                    }
                })
                .setButton2Click(new View.OnClickListener() {       //取消
                    @Override
                    public void onClick(View v) {
                        if (switchServer != null) {
                            switchServer.setChecked(false);
                        }
                        //隐藏dialog
                        dialogBuilder.dismiss();
                    }
                }).show();
    }

    /**
     * 报到台设置   dialog
     *
     * @param context      上下文
     * @param tvCheckpoint 控件
     */
    public void showCheckPoint(Context context, TextView tvCheckpoint) {
        AppDatabase.getInstance(context)
            .getCheckPointDao()
            .getAllData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<CheckPointEntity>>() {
                @Override
                public void accept(List<CheckPointEntity> list) throws Exception {
                    if (list != null && list.size() > 0) {
                        initCheckPoint(context, tvCheckpoint, list);
                    } else {
                        ToastUtils.showShort(R.string.sync_datapack);
                    }
                }
            });
        //getCheckPoint(context);
    }

    //获取报到台
    private void getCheckPoint(Context context) {
        //显示加载框
        LoadingDialogUtil.showLoadingDialog(context);
        RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
        //网络请求数据
        ServiceGenerager
            .getInstance().createService()
            .getCheckPointInfo(IdaParams.getTicketInfo(recordsBean.getMeetingId()))
            .enqueue(new CommonCallBack<ApiResponse<List<CheckPointEntity>>>() {
                @Override
                public void onSuc(String code, String errMsg, Response<ApiResponse<List<CheckPointEntity>>> response) {
                    //隐藏加载框
                    LoadingDialogUtil.hideLoadingDialog();
                    Log.d("test", "报到台----" + JsonHelper.getGson().toJson(response.body().code));
                    //报到台信息
                    List<CheckPointEntity> pointMoudles = response.body().data;
                    if (pointMoudles != null && pointMoudles.size() > 0) {
                        Log.d("test", "报到台有" + pointMoudles.size() + "个");
                        //缓存报到台信息
                        Hawk.put(Constants.CHECKPOINTINFO, pointMoudles);
                        //更新数据并刷新界面
                        mCheckPointEntities.clear();
                        mCheckPointEntities.addAll(pointMoudles);
                        mCheckPointAdapter.notifyDataSetChanged();
                    } else {
                        mDialogBuildercheck.dismiss();
                        registCodeHint(context, context.getString(R.string.no_data_hint));
                    }

                }

                @Override
                public void onFail(String code, String errMsg, Response<ApiResponse<List<CheckPointEntity>>> response) {
                    super.onFail(code, errMsg, response);
                    //隐藏加载框
                    LoadingDialogUtil.hideLoadingDialog();
                    ToastUtils.showShort(errMsg);
                }
            });
    }

    //初始化布局  报到台
    private void initCheckPoint(Context context, TextView tvCheckpoint, List<CheckPointEntity> checkPointEntityList) {
        mCheckPointEntities = new ArrayList<>();
        //读取缓存数据
        //if (Hawk.contains(Constants.CHECKPOINTINFO)) {
        //    checkPointEntityList = (List<CheckPointEntity>) Hawk.get(Constants.CHECKPOINTINFO);
            mCheckPointEntities.addAll(checkPointEntityList);
        //}

        CheckPointEntity extraEntity = new CheckPointEntity();
        extraEntity.setId(0); extraEntity.setCheckpointName("无");
        //extraEntity.setCiActivityId(checkPointEntityList.get(0).ciActivityId);
        mCheckPointEntities.add(extraEntity);

        mDialogBuildercheck = NiftyDialogBuilder.getInstance(context);
        mDialogBuildercheck
                .withTitle(context.getResources().getString(R.string.checkpoint))                                  //.withTitle(null)  no title
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withMessage(null)                     //.withMessage(null)  no Msg
                .withDuration(700)                                          //def
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                .setCustomView(R.layout.dialog_list, mDialogBuildercheck.getContext())         //.setCustomView(View or ResId,context)
                .show();

        RecyclerView recyclerView = mDialogBuildercheck.findViewById(R.id.dialog_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(mDialogBuildercheck.getContext()));
        mCheckPointAdapter = new BaseQuickAdapter<CheckPointEntity, BaseViewHolder>(R.layout.item_checkpoint_list_dialog, mCheckPointEntities) {
            @Override
            protected void convert(BaseViewHolder helper, CheckPointEntity item) {
                //报到台名字
                helper.setText(R.id.tv_checkpoint, item.getCheckpointName());
            }
        };
        recyclerView.setAdapter(mCheckPointAdapter);
        //列表点击事件
        mCheckPointAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //获取点击的报到台信息
                CheckPointEntity checkPointEntity = (CheckPointEntity) adapter.getData().get(position);
                if (checkPointEntity.getId() > 0) {
                    //设置控件的值
                    tvCheckpoint.setText(checkPointEntity.getCheckpointName());
                    //保存报到台信息
                    Hawk.put(Constants.CHECKPINT, checkPointEntity);
                    Hawk.put(Constants.ISCHECKPOINT, false);
                } else {
                    if (Hawk.contains(Constants.CHECKPINT)) {
                        Hawk.delete(Constants.CHECKPINT);
                    }
                    Hawk.put(Constants.ISCHECKPOINT, true);
                    tvCheckpoint.setText(null);
                }
                //隐藏dialog
                mDialogBuildercheck.dismiss();
            }
        });
    }

    //同步服务器已签到的门票信息
    public void syncTicketData(Context context) {
        if (Hawk.contains(Constants.DEVICE) && Hawk.contains(Constants.CHECKPINT)) {
            //显示加载框
            LoadingDialogUtil.showLoadingDialog(context);
            //获取已设置的报到台信息
            CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
            //获取设置报到设备
            String device = Hawk.get(Constants.DEVICE);
            //同步对象
            SyncTicketList syncTicketList = new SyncTicketList();
            //设置报到台id
            syncTicketList.setCheckpointId(checkPointEntity.getId());
            //设置报到设备
            syncTicketList.setDevice(device);
            //会议id
            RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
            syncTicketList.setMeetingId(recordsBean.getMeetingId());
            //获取当前设备的签到信息
            AppDatabase.getInstance(context)
                    .getSignInDao().getSignInBydevice(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<List<SignInMoudle>>() {
                        @Override
                        public void accept(List<SignInMoudle> signInMoudleList) throws Exception {
                            //签到时间和设备
                            List<SyncTicketList.TicketList> syncTicketLists = new ArrayList<>();
                            if (signInMoudleList != null && signInMoudleList.size() > 0) {
                                for (int i = 0; i < signInMoudleList.size(); i++) {
                                    SignInMoudle signInMoudle = signInMoudleList.get(i);
                                    SyncTicketList.TicketList syncTicketList = new SyncTicketList.TicketList(signInMoudle.getEmpId(), signInMoudle.getTime());
                                    syncTicketLists.add(syncTicketList);
                                }
                            }
                            //添加签到信息
                            syncTicketList.setTicketList(syncTicketLists);
                            //网络同步信息
                            ServiceGenerager.getInstance()
                                    .createService()
                                    .syncReport(syncTicketList)
                                    .enqueue(new CommonCallBack<ApiResponse<List<Integer>>>() {
                                        @Override
                                        public void onSuc(String code, String errMsg, Response<ApiResponse<List<Integer>>> response) {
                                            if (response.body() != null && response.body().data != null) {
                                                //已签到的门票id List
                                                List<Integer> integerList = response.body().data;
                                                //获取全部的门票信息
                                                AppDatabase.getInstance(context).getTicketInfoDao().loadAllTicketInfo()
                                                        .subscribeOn(Schedulers.io())
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .subscribe(new Consumer<List<TicketInfo>>() {
                                                            @Override
                                                            public void accept(List<TicketInfo> ticketInfos) throws Exception {
                                                                //所有门票信息
                                                                List<TicketInfo> ticketInfoList = new ArrayList<>();
                                                                //遍历门票信息
                                                                for (int i = 0; i < ticketInfos.size(); i++) {
                                                                    //遍历已签到的门票id
                                                                    for (int j = 0; j < integerList.size(); j++) {
                                                                        //找到符合 的id  更新数据库门票信息
                                                                        if (ticketInfos.get(i).getId() == integerList.get(j)) {
                                                                            //设置已签到
                                                                            ticketInfos.get(i).setCheckedFlag(1);
                                                                            ticketInfoList.add(ticketInfos.get(i));
                                                                        }
                                                                    }
                                                                }
                                                                //更新数据
                                                                AppDatabase.getInstance(context).getTicketInfoDao().insertAll(ticketInfoList);
                                                                //隐藏加载框
                                                                LoadingDialogUtil.hideLoadingDialog();
                                                                ToastUtils.showShort(context.getString(R.string.sync_success));
                                                            }
                                                        });
                                            }

                                        }

                                        @Override
                                        public void onFail(String code, String errMsg, Response<ApiResponse<List<Integer>>> response) {
                                            super.onFail(code, errMsg, response);
                                            //失败信息
                                            LoadingDialogUtil.hideLoadingDialog();
                                            ToastUtils.showShort(context.getString(R.string.sync_fail) + code + errMsg);
                                        }
                                    });

                        }
                    });

        } else {        //未设置报到台及报到设备提示
            registCodeHint(context, context.getString(R.string.setting_hint));
        }
    }


    /**
     * 会议设置   dialog
     *
     * @param context      上下文
     * @param tvMetting    会议
     * @param tvCheckpoint 控件 界别
     * @param isdivision   是否是界别
     */
    public void showMettingList(Context context, TextView tvMetting, TextView tvCheckpoint, boolean isdivision) {
        initMettingList(context, tvMetting, tvCheckpoint, isdivision);
        getMettingList(context, isdivision);
    }

    //获取会议
    private void getMettingList(Context context, boolean isdivision) {
        if (isdivision) {    //是否是界别
            if (Hawk.contains(Constants.MEETING)) {      //获取界别数据
                RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.MEETING);
                getMettingData(context, recordsBean.getId());
            } else {      //设置会议提示
                mDialogBuildermetting.dismiss();
                registCodeHint(context, context.getString(R.string.division_hint));
            }
        } else { //获取会议数据
            getMettingData(context, 0);
        }


    }

    private void getMettingData(Context context, int mettingId) {
        //显示加载框
        LoadingDialogUtil.showLoadingDialog(context);
        //网络请求数据
        if (mettingId > 0) {        //界别网络请求
            ServiceGenerager
                    .getInstance().createService()
                    .getDivsionList(IdaParams.getDivisonList(mettingId, 1, 30))
                    .enqueue(new CommonCallBack<ApiResponse<MettingMoudle>>() {
                        @Override
                        public void onSuc(String code, String errMsg, Response<ApiResponse<MettingMoudle>> response) {
                            //隐藏加载框
                            LoadingDialogUtil.hideLoadingDialog();
                            Log.d("test", "会议----" + JsonHelper.getGson().toJson(response.body().code));
                            //会议信息
                            MettingMoudle mettingMoudle = response.body().data;
                            List<RecordsBean> recordsBeanList = mettingMoudle.getRecords();
                            if (recordsBeanList != null && recordsBeanList.size() > 0) {
                                //缓存会议信息
                                Hawk.put(Constants.DIVSIONLIST, mettingMoudle);
                                //更新数据并刷新界面
                                mRecordsBeanList.clear();
                                mRecordsBeanList.addAll(recordsBeanList);
                                mMettingAdapter.notifyDataSetChanged();
                            } else {
                                mDialogBuildermetting.dismiss();
                                registCodeHint(context, context.getString(R.string.no_data_hint));
                            }

                        }

                        @Override
                        public void onFail(String code, String errMsg, Response<ApiResponse<MettingMoudle>> response) {
                            super.onFail(code, errMsg, response);
                            //隐藏加载框
                            LoadingDialogUtil.hideLoadingDialog();
                            mDialogBuildermetting.dismiss();
                            ToastUtils.showShort(errMsg);
                        }
                    });
        } else {            //会议网络请求
            ServiceGenerager
                    .getInstance().createService()
                    .getMettingList(IdaParams.getMettingList(1, 30))
                    .enqueue(new CommonCallBack<ApiResponse<MettingMoudle>>() {
                        @Override
                        public void onSuc(String code, String errMsg, Response<ApiResponse<MettingMoudle>> response) {
                            //隐藏加载框
                            LoadingDialogUtil.hideLoadingDialog();
                            Log.d("test", "会议----" + JsonHelper.getGson().toJson(response.body().code));
                            //会议信息
                            MettingMoudle mettingMoudle = response.body().data;
                            List<RecordsBean> recordsBeanList = mettingMoudle.getRecords();
                            if (recordsBeanList != null && recordsBeanList.size() > 0) {
                                //缓存会议信息
                                Hawk.put(Constants.MEETINGLIST, mettingMoudle);
                                //更新数据并刷新界面
                                mRecordsBeanList.clear();
                                mRecordsBeanList.addAll(recordsBeanList);
                                mMettingAdapter.notifyDataSetChanged();
                            } else {
                                mDialogBuildermetting.dismiss();
                                registCodeHint(context, context.getString(R.string.no_data_hint));
                            }
                        }

                        @Override
                        public void onFail(String code, String errMsg, Response<ApiResponse<MettingMoudle>> response) {
                            super.onFail(code, errMsg, response);
                            //隐藏加载框
                            LoadingDialogUtil.hideLoadingDialog();
                            mDialogBuildermetting.dismiss();
                            ToastUtils.showShort(errMsg);
                        }
                    });
        }

    }

    //初始化布局  会议&&界别
    private void initMettingList(Context context, TextView tvMetting, TextView tvCheckpoint, boolean isdivision) {
        mRecordsBeanList = new ArrayList<>();
        //读取缓存数据
        if (isdivision) {
            if (Hawk.contains(Constants.DIVSIONLIST)) {
                MettingMoudle mettingMoudle = (MettingMoudle) Hawk.get(Constants.DIVSIONLIST);
                mRecordsBeanList.clear();
                mRecordsBeanList.addAll(mettingMoudle.getRecords());
            }
        } else {
            if (Hawk.contains(Constants.MEETINGLIST)) {
                MettingMoudle mettingMoudle = (MettingMoudle) Hawk.get(Constants.MEETINGLIST);
                mRecordsBeanList.clear();
                mRecordsBeanList.addAll(mettingMoudle.getRecords());
            }
        }

        mDialogBuildermetting = NiftyDialogBuilder.getInstance(context);
        mDialogBuildermetting
                .withTitle(isdivision == true ? context.getString(R.string.division) : context.getResources().getString(R.string.metting))                                  //.withTitle(null)  no title
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withMessage(null)                     //.withMessage(null)  no Msg
                .withDuration(700)                                          //def
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                .setCustomView(R.layout.dialog_list, mDialogBuildermetting.getContext())         //.setCustomView(View or ResId,context)
                .show();

        RecyclerView recyclerView = mDialogBuildermetting.findViewById(R.id.dialog_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(mDialogBuildermetting.getContext()));
        //报到台名字
        mMettingAdapter = new BaseQuickAdapter<RecordsBean, BaseViewHolder>(R.layout.item_checkpoint_list_dialog, mRecordsBeanList) {
            @Override
            protected void convert(BaseViewHolder helper, RecordsBean item) {
                if (isdivision) {
                    //届数
                    helper.setText(R.id.tv_checkpoint, String.valueOf(item.getDivisionYear()));
                } else {
                    //会议名字
                    helper.setText(R.id.tv_checkpoint, item.getAbbreviate());
                }

            }
        };
        recyclerView.setAdapter(mMettingAdapter);
        //列表点击事件
        mMettingAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //获取点击的会议信息
                RecordsBean recordsBean = (RecordsBean) adapter.getData().get(position);

                //保存会议信息
                if (isdivision) {
                    Hawk.put(Constants.DIVSION, recordsBean);
                    //设置控件的值
                    tvCheckpoint.setText(String.valueOf(recordsBean.getDivisionYear()));
                } else {
                    Hawk.put(Constants.MEETING, recordsBean);
                    if (Hawk.contains(Constants.DIVSION)) {
                        Hawk.delete(Constants.DIVSION);
                    }
                    //设置控件的值
                    tvMetting.setText(recordsBean.getAbbreviate());
                    //界别
                    tvCheckpoint.setText("");
                }

                //隐藏dialog
                mDialogBuildermetting.dismiss();
            }
        });
    }

    /**
     * cookie失效  重新登录提示
     *
     * @param context
     * @param message
     */
    public void registLoginHint(Context context, String message) {
        NiftyDialogBuilder dialogBuilder2 = NiftyDialogBuilder.getInstance(context);
        dialogBuilder2
                .withTitle("提示")
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withMessage(message)                     //.withMessage(null)  no Msg
                .withMessageColor(context.getResources().getColor(R.color.black))                              //def  | withMessageColor(int resid)
                .withDuration(700)                                          //def
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, context.getResources().getString(R.string.confirm))
                .setButton1Click(new View.OnClickListener() {               //是
                    @Override
                    public void onClick(View v) {
                        //跳转登录界面
                        //                        Intent intent = new Intent(context, LoginActivity.class);
                        //                        context.startActivity(intent);
                        //                        ((Activity) context).finish();
                        //隐藏dialog
                        dialogBuilder2.dismiss();
                    }
                }).show();
    }


    /**
     * 团队长与队员报到台不一致
     *
     * @param context  上下文
     * @param message  信息提示
     * @param teamList 团队队员全部信息
     */
    public void registTeamHint(Context context, String message, List<TicketInfo> teamList, String time, String device) {
        CheckPointEntity checkPointEntity = (CheckPointEntity) Hawk.get(Constants.CHECKPINT);
        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle("提示")
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withMessage(message)                     //.withMessage(null)  no Msg
                .withMessageColor(context.getResources().getColor(R.color.black))                              //def  | withMessageColor(int resid)
                .withDuration(700)                                          //def
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, context.getResources().getString(R.string.team_leader_hint))
                .withButton2Text(View.VISIBLE, context.getResources().getString(R.string.team_member_hint))
                .setButton1Click(new View.OnClickListener() {               //团队长/领队 签到
                    @Override
                    public void onClick(View v) {
                        //隐藏dialog
                        dialogBuilder.dismiss();
                        if (checkPointEntity.getId() == teamList.get(0).getCheckpointId()) {  //队长的报倒台是否与当前报到台一致
                            //更新签到  团队长/领队
                            updateTeamLeader(context, time, device, teamList);
                            //成功提示
                            ToastUtils.showShort(context.getString(R.string.sign_hint_success));
                        } else {
                            registCodeHint(context, teamList.get(0).getCheckpointName());
                        }

                    }
                })
                .setButton2Click(new View.OnClickListener() {           //队员签到
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                        if (checkPointEntity.getId() == teamList.get(1).getCheckpointId()) {       //报到台是否与队员的报到台一样
                            //更新队员的签到记录
                            updateTeamMember(context, time, device, teamList);
                            //签到成功提示
                            ToastUtils.showShort(context.getString(R.string.sign_hint_success));
                        } else {  //不一样 提示去队员的报到台报到
                            registCodeHint(context, teamList.get(1).getCheckpointName());
                        }

                    }
                })
                .show();
    }


    //签到团队
    public void setSign(Context context, String time, String device, List<TicketInfo> ticketInfoList) {
        //更新队员的签到信息
        updateTeamMember(context, time, device, ticketInfoList);
        //更新团队长/领队签到信息
        updateTeamLeader(context, time, device, ticketInfoList);

        //团队签到 网络提交数据
        updateTeamInfo(device, ticketInfoList);
    }

    //更新团队长/领队签到
    private void updateTeamLeader(Context context, String time, String device, List<TicketInfo> ticketInfoList) {
        //-------更新领队或团队长的签到信息-------
        //签到信息
        SignInMoudle signInMoudle = new SignInMoudle(time, device, ticketInfoList.get(0).getId());
        //插入签到数据
        AppDatabase.getInstance(context).getSignInDao().insertUser(signInMoudle);
        //更新签到
        ticketInfoList.get(0).setCheckedFlag(1);
        //更新数据库门票签到信息
        AppDatabase.getInstance(context).getTicketInfoDao().updateUser(ticketInfoList.get(0));

        ToastUtils.showShort(context.getString(R.string.sign_hint_success));
    }

    /**
     * //团队签到 网络提交数据
     *
     * @param device
     * @param ticketInfoList
     */
    private void updateTeamInfo(String device, List<TicketInfo> ticketInfoList) {
        RecordsBean recordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
        ServiceGenerager.getInstance().createService()
                .getReport(IdaParams.getSignInfo(recordsBean.getMeetingId(), ticketInfoList.get(0).getTicketCode(), ticketInfoList.get(0).getCheckpointId(), device))
                .enqueue(new CommonCallBack<ApiResponse<List<ReportMoudle>>>() {
                    @Override
                    public void onSuc(String code, String errMsg, Response<ApiResponse<List<ReportMoudle>>> response) {
                        Log.d("test", "团队报到---" + response.toString());
                    }
                });
    }

    /**
     * 队员签到
     *
     * @param context
     * @param time
     * @param device
     * @param ticketInfoList
     */
    private void updateTeamMember(Context context, String time, String device, List<TicketInfo> ticketInfoList) {
        for (int i = 0; i < ticketInfoList.size(); i++) {
            if (i == 0) {
                continue;
            } else {
                TicketInfo ticketInfo = ticketInfoList.get(i);
                //签到信息
                SignInMoudle signInMoudle = new SignInMoudle(time, device, ticketInfo.getId());
                //插入签到数据
                AppDatabase.getInstance(context).getSignInDao().insertUser(signInMoudle);
                //更新签到
                ticketInfo.setCheckedFlag(1);
                //更新数据库门票签到信息
                AppDatabase.getInstance(context).getTicketInfoDao().updateUser(ticketInfo);
            }

        }
    }


    /**
     * 设置备注 和 拍照
     *
     * @param context    上下文
     * @param title      标题
     * @param ticketInfo
     */
    public void showRemark(Context context, String title, TicketInfo ticketInfo) {
        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle(title)
                .withTitleColor(context.getResources().getColor(R.color.black))                                  //def
                .withDuration(700)                                          //def
                .withMessage(null)
                .withEffect(Effectstype.Fadein)                                         //def Effectstype.Slidetop
                .withDialogColor(context.getResources().getColor(R.color.white))
                .isCancelableOnTouchOutside(false)                           //def    | isCancelable(true)
                .withButton1Text(View.VISIBLE, "拍照")
                .withButton2Text(View.VISIBLE, "备注")
                .setCustomView(R.layout.dialog_text, dialogBuilder.getContext())
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {       //确定
                        //隐藏dialog
                        dialogBuilder.dismiss();

                        EventBus.getDefault().post(ticketInfo);
                    }
                })
                .setButton2Click(new View.OnClickListener() {       //取消
                    @Override
                    public void onClick(View v) {
                        EditText editText = dialogBuilder.findViewById(R.id.edit_text);
                        String server = editText.getText().toString().trim();
                        if (StringUtils.isNotEmpty(server)) {       //判空

                            //设置备注
                            ticketInfo.setRemark(server);
                            //更新备注信息
                            AppDatabase.getInstance(context).getTicketInfoDao().updateUser(ticketInfo);
                            ToastUtils.showShort("修改成功");
                            //隐藏dialog
                            dialogBuilder.dismiss();
                            //隐藏dialog 原先的
                            mDialogBuilder.dismiss();
                            //重新加载用户门票信息
                            showDialog(context, ticketInfo, null, true);
                            RecordsBean RecordsBean = (RecordsBean) Hawk.get(Constants.DIVSION);
                            //网络请求修改备注
                            ServiceGenerager.getInstance().createService().
                                    remark(IdaParams.updateRemark(ticketInfo.getTicketCode(),
                                            server, RecordsBean.getMeetingId())).enqueue(new CommonCallBack<ApiResponse<String>>() {
                                @Override
                                public void onSuc(String code, String errMsg, Response<ApiResponse<String>> response) {

                                }
                            });

                        } else {
                            ToastUtils.showShort("请填写备注信息");
                        }
                    }
                })
                .show();

        EditText editText = dialogBuilder.findViewById(R.id.edit_text);
        if (StringUtils.isNotEmpty(ticketInfo.getRemark())){        //是否有设置备注  设置默认备注
            editText.setText(ticketInfo.getRemark());
        }
    }

    //隐藏dailog   门票用户信息
    public void dialogDismiss() {
        if (mDialogBuilder != null && mDialogBuilder.isShowing()) {
            mDialogBuilder.dismiss();
        }
    }

    /**
     * 备注图片放大功能
     *
     * @param context
     * @param item
     */
    public static void photoView(Context context, DialogMoudle item) {
        NiftyDialogBuilder dialogBuilder2 = NiftyDialogBuilder.getInstance(context);
        dialogBuilder2
                .setContentView(R.layout.item_photoview);

        PhotoView photoView = dialogBuilder2.findViewById(R.id.photoview);
        //判断是否是加载本地，网络图片
        if (item.getContent().contains("http")) {
            Glide.with(context).load(item.getContent()).placeholder(R.drawable.icon_user).into(photoView);
        } else {
            File picfile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + Constants.APPPICTUREPATH, item.getContent());
            Glide.with(context).load(picfile/*item.getContent()*/).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.icon_user).into(photoView);
        }
        //启动缩放放大
        photoView.enable();
        //点击图片
        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoView.setVisibility(View.GONE);
                dialogBuilder2.dismiss();
            }
        });
        dialogBuilder2.show();
    }
}
