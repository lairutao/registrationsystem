package com.registration.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.registration.R;

import androidx.annotation.NonNull;

/*
 * @创建者 admin
 * @创建时间  2019/7/8 11:54
 * @描述      ${TODO}         登录dialog
 * @更新者    $Author$
 * @更新时间  $Date$
 * @更新描述  ${TODO}
 */
public class LoginDialog extends Dialog {
    public LoginDialog(@NonNull Context context) {
        super(context);
    }

    public LoginDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.dialog_login);
    }
}
