package com.registration.net;

import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.DeviceUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;
import com.registration.base.MyApplication;
import com.registration.constants.Constants;
import com.registration.moudle.HistoryEntity;
import com.registration.moudle.RemarkEntity;
import com.registration.moudle.SyncTicketList;
import com.registration.util.Base64Utils;
import com.registration.util.DESUtil;
import com.registration.util.JsonHelper;
import com.registration.util.MD5Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ZeQiang Fang on 2019/3/14.
 */
public class IdaParams {

    public static final int code_login = 1;

    public static HashMap<String, Object> commonParams() {
        HashMap<String, Object> baseParams = new HashMap<>();
        baseParams.put("device", "ANDROID_" + DeviceUtils.getModel());
        baseParams.put("channelId", Hawk.get(Constants.CHANNELID, ""));
        Locale locale = MyApplication.getInstance().getResources().getConfiguration().locale;
        //判断国家简写：
        String country = locale.getCountry();
        int i18;
        if (country.equals("CN")) {//繁体中文或中文
            i18 = 1;
        } else if (country.equals("TW")) {//非中文
            i18 = 2;
        } else {
            i18 = 3;
        }
        baseParams.put("i18", i18);
        return baseParams;
    }

    public static EncryptRequest commonParamsBase64() {
        HashMap<String, Object> map = commonParams();
        return paramsToBase64Salt(map);
    }


    public static EncryptRequest paramsToBase64Salt(HashMap<String, Object> map) {
        String base64 = Base64Utils.getBase64(map);
        String randomkey = Hawk.get(Constants.RANDOMKEY, "");
        if (!TextUtils.isEmpty(randomkey)) {//有签名
            String sign = MD5Utils.MD5(base64 + randomkey);
            return new EncryptRequest(base64, sign);
        } else {
            return new EncryptRequest(base64);
        }
    }

    public static Map<String,Object> passwordCodeParams(String username, String password) {//账号密码登录

        if (Hawk.contains(Constants.TOKEN)) {
            Hawk.delete(Constants.TOKEN);
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("userName", username);      //账号
        map.put("password", password);      //密码
        String obj = JsonHelper.getGson().toJson(map);
        String str = DESUtil.encrypt(obj, Constants.DES_KEY);
        HashMap<String, Object> encryptRequest = new HashMap<>();
        encryptRequest.put("object", str);
        return encryptRequest;
    }

    public static EncryptRequest getDataPackParams(List<HistoryEntity> list1, List<RemarkEntity> list2) {
        String syncTime = Hawk.get(Constants.SYNCTIME);
        if (TextUtils.isEmpty(syncTime)) {
            syncTime = "0";
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("dataString", "");
        map.put("remarkList", list2);
        map.put("historyList", list1);
        map.put("lastSyncTime", syncTime);
        return paramsToBase64Salt(map);
    }

    public static Map<String, Object> getTicketInfo(int meetingId) {//门票信息
        HashMap<String, Object> map = new HashMap<>();
        map.put("meetingId", meetingId);        //会议id
        return map;
    }

    public static Map<String, Object> getSignInfo(int meetingId, String code, int checkpoint, String device) {//门票签到
        HashMap<String, Object> map = new HashMap<>();
        map.put("meetingId", meetingId);        //会议id
        map.put("ticketCode", code);            //二维码
        map.put("checkpointId", checkpoint);    //报到台id
        map.put("device", device);              //报到设备
        return map;
    }

    public static Map<String, Object> syncInfo(int checkpointId, String device, int meetingId, List<SyncTicketList> ticketList) {//同步数据
        HashMap<String, Object> map = new HashMap<>();
        map.put("checkpointId", checkpointId);       //报到台id
        map.put("device", device);                   //报到设备
        map.put("meetingId", meetingId);              //会议id
        map.put("ticketList", ticketList);           //门票列表

        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
        String json = gson.toJson(map);
        Log.d("test", "json-------------" + json);
        return map;
    }

    //会议数据
    public static Map<String, Object> getMettingList(int pageNum,int pageSize) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("pageNum",pageNum);
        map.put("pageSize",pageSize);//每一页的大小
        return map;
    }

    //界别数据
    public static Map<String, Object> getDivisonList(int meetingId,int pageNum,int pageSize) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("meetingId", meetingId);              //会议id
        map.put("pageNum",pageNum);
        map.put("pageSize",pageSize);//每一页的大小
        return map;
    }

    //备注信息修改
    public static Map<String, Object> updateRemark(String  ticketCode,String  remark,int meetingId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ticketCode",ticketCode);             //门票二维码
        map.put("remark",remark);                     //备注信息
        map.put("meetingId", meetingId);              //会议id
        return map;
    }

    //获取报到信息
    public static Map<String, Object> reportStatus(String  ticketCode,int meetingId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ticketCode",ticketCode);             //门票二维码
        map.put("meetingId", meetingId);              //会议id
        return map;
    }

}
