package com.registration.net;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.hawk.Hawk;
import com.registration.BuildConfig;
import com.registration.constants.Constants;
import com.registration.cookie.PersistentCookieJar;
import com.registration.cookie.SetCookieCache;
import com.registration.cookie.SharedPrefsCookiePersistor;
import com.registration.util.JsonHelper;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lai on 2019/7/3.
 */
public class ServiceGenerager {

    private static ServiceGenerager                        serviceGenerager;
    //cookie存储
    private static ConcurrentHashMap<String, List<Cookie>> cookieStore = new ConcurrentHashMap<>();

    public static ServiceGenerager getInstance() {
        if (serviceGenerager == null) {
            synchronized (ServiceGenerager.class) {
                if (serviceGenerager == null) {
                    serviceGenerager = new ServiceGenerager();
                }
            }
        }
        return serviceGenerager;
    }

    public Service createService() {
        if (Hawk.contains(Constants.URL)) {         //设置其他服务器
            String baseUrl = Hawk.get(Constants.URL);
            if (!TextUtils.isEmpty(baseUrl)) {
                return createService(Service.class, baseUrl);
            }
        }
        return createService(Service.class, Constants.BASE_URL);
    }


    //retrofit: 1. baseUrl(和@GET中所填写的value组成完整路径) 2. client 3. converterFactory（用于对象转化，本例因为服务器返回的是json格式的数组，所以设置了GsonConverterFactory）
    public static <S> S createService(Class<S> serviceClass, String baseUrl) {

        //通过OkHttpClient 可以配置很多东西，比如链接超时时间，缓存，拦截器等等。代码如下：
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //实际项目中，我们可能有一些公共的参数，如 ，设备信息，渠道，Token 之类的，每个接口都需要用，我们可以写一个拦截器，然后配置到OKHttpClient里，通过 builder.addInterceptor(basicParamsInterceptor) 添加
        builder.addInterceptor(new Interceptor() {//添加拦截器，用来修改即将发出去的请求
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                final String token = Hawk.get(Constants.TOKEN, "");
                if (!TextUtils.isEmpty(token) ) {
                    Request request = originalRequest.newBuilder()
                            .header("Authorization","Bearer " + token)
                            .method(originalRequest.method(), originalRequest.body())
                            .build();
                    return chain.proceed(request);
                }
                return chain.proceed(originalRequest);
            }
        });
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(logging);
        }
        //获取已保存的cookies
//        if (Hawk.contains(Constants.COOKIES)) {
//            //获取json字符串
//            String cookies = (String) Hawk.get(Constants.COOKIES);
//            //json 转化成 map
//            cookieStore = jsonStrToMap(cookies);
//            Log.d("test", "cookie-------" + cookies);
//        }
        OkHttpClient httpClient = builder
                //                .addInterceptor(logging)
                .connectTimeout(30, TimeUnit.SECONDS)//连接超时时间
                .readTimeout(30, TimeUnit.SECONDS)//写操作 超时时间
                .writeTimeout(60, TimeUnit.SECONDS)//读操作超时时间
                .retryOnConnectionFailure(true)         // 设置进行连接失败重试
//                .cookieJar(new CookieJar() {
//                    @Override
//                    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
//                        //可以做保存cookies操作  持久化
//                        cookieStore.put(url.host(), cookies);
//                        //将map转换成json
//                        Gson gson = new Gson();
//                        String cookie = gson.toJson(cookieStore);
//                        //保存gson
//                        Hawk.put(Constants.COOKIES, cookie);
//
//                        Log.d("test","cookie---"+cookie);
//                    }
//
//                    @Override
//                    public List<Cookie> loadForRequest(HttpUrl url) {
//                        //加载新的cookies
//                        List<Cookie> cookies = cookieStore.get(url.host());
//                        return cookies != null ? cookies : new ArrayList<Cookie>();
//                    }
//                })
                .build();

        Gson gson = new GsonBuilder().setLenient().create();//需添加这行代码，否则请求付款url时不会成功
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(serviceClass);
    }

    //json字符串转换为MAP
    public static ConcurrentHashMap<String, List<Cookie>> jsonStrToMap(String cookie) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
        Type type = new TypeToken<ConcurrentHashMap<String, List<Cookie>>>() {
        }.getType();
        return gson.fromJson(cookie, type);
    }

}
