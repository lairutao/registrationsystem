package com.registration.net;

/**
 * 返回信息
 */

public class ApiResponse<Response> {
    public String code="";
    public String message="";
    public Response data;
}