package com.registration.net;

import com.registration.moudle.CheckPointEntity;
import com.registration.moudle.MettingMoudle;
import com.registration.moudle.ReportMoudle;
import com.registration.moudle.SyncTicketList;
import com.registration.moudle.TicketInfo;
import com.registration.moudle.TransferPictureMoudle;
import com.registration.moudle.AuthMoudle;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * POST请求
 */
public interface Service {

//    @POST("v1/file/upload/base64Image")//上传Base64照片
//    Call<ApiResponse<String>> uploadImg(@Body EncryptRequest encryptRequest);

//    @POST("v1/ticket/bind")//code绑定门票
//    Call<ApiResponse<Object>> ticketBind(@Body EncryptRequest encryptRequest);

//    @POST("v1/ticket/bindByPassword")//会员编号加密码绑定门票
//    Call<ApiResponse<Object>> ticketBindByPassword(@Body EncryptRequest encryptRequest);

//    @POST("v1/course/select")//选课
//    Call<ApiResponse<Object>> courseSelect(@Body EncryptRequest encryptRequest);


//    @POST("v1/appUser/update")//更新用户信息
//    Call<ApiResponse> updateUserInfo(@Body EncryptRequest encryptRequest);

//    @POST("/v1/challenge/save")//ida提交数据
//    Call<ApiResponse<String>> challengeSave(@Body EncryptRequest encryptRequest);

//    @POST("/loginApi")//登录
//    Call<ApiResponse<String>> login(@QueryMap Map<String,Object> encryptRequest);

    @POST("/auth")//新登录接口
    Call<ApiResponse<AuthMoudle>> login(@QueryMap Map<String,Object> encryptRequest);

    @POST("/v2/ci/sync/gzip")//报到信息
    Call<ApiResponse<String>> getDataPack(@Body EncryptRequest encryptRequest);

    @POST("/check/in/ticket/list")//门票信息
    Call<ApiResponse<List<TicketInfo>>> getTicketInfo(@QueryMap Map<String,Object> encryptRequest);

    @POST("/check/in/checkpoint/list")//报到台
    Call<ApiResponse<List<CheckPointEntity>>> getCheckPointInfo(@QueryMap Map<String,Object> encryptRequest);

    @POST("/check/in/report")//签到       906：门票不存在, 908:队员不可单独报道，909：该门票已有报道记录。
    Call<ApiResponse<List<ReportMoudle>>> getReport(@QueryMap Map<String,Object> encryptRequest);

    @POST("/check/in/sync")//同步数据
    Call<ApiResponse<List<Integer>>> syncReport(@Body SyncTicketList encryptRequest);

    @GET("/meeting/list")//会议
    Call<ApiResponse<MettingMoudle>> getMettingList(@QueryMap Map<String,Object> encryptRequest);

    @GET("/division/list")//界别
    Call<ApiResponse<MettingMoudle>> getDivsionList(@QueryMap Map<String,Object> encryptRequest);

    @POST("/check/in/ticket/transferPicture")//门票图片
    Call<ApiResponse<String>> transferPicture(@Body TransferPictureMoudle transferPictureMoudle);

    @POST("/check/in/ticket/remark")//备注
    Call<ApiResponse<String>> remark(@QueryMap Map<String,Object> encryptRequest);

    @POST("/check/in/reportStatus")//查询报到信息
    Call<ApiResponse<List<ReportMoudle>>> reportStatus(@QueryMap Map<String,Object> encryptRequest);
}
