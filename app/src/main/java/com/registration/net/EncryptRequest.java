package com.registration.net;


public class EncryptRequest {

    public EncryptRequest(String object) {
        this.object = object;
    }

    public EncryptRequest(String strBase64, String sign) {
        this.object = strBase64;
        this.sign = sign;
    }

    String object;

    String sign;
}
