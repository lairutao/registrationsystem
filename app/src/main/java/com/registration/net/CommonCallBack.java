package com.registration.net;

import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.JsonSyntaxException;
import com.registration.util.JsonHelper;
import com.registration.util.StringUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by imm on 2017/4/12.
 * retrofit网络请求的回调
 */

public abstract class CommonCallBack<T extends ApiResponse> implements Callback<T> {

    public CommonCallBack() {
    }

    //    private Context context;
    //网络请求成功
    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        Log.d("CommonCallBack", "请求接口:"+call.request().url());

        if (response.isSuccessful()) {//服务器请求成功

            Log.d("CommonCallBack", "请求成功:" + JsonHelper.getGson().toJson(response.body()));
            String code = response.body().code;
            if ("200".equals(code)) {
                onSuc(code, response.body().message, response);//请求成功
            } else {
                if ("501".equals(code) || "502".equals(code)) {

                }
                onFail(code, response.body().message, response);
            }
            Log.d("result", "onResponse: >>>>>>>>" + code);
        } else {//http状态码错误会拿不到数据，服务端也不知道是什么错误

            onFail(String.valueOf(response.code()), "服务器异常", response);//失败信息
//            ToastUtils.showShort("服务器异常,请稍后重试");
        }
    }

    //网络请求失败
    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.d("CommonCallBack", "请求接口:"+call.request().url());
        Log.d("CommonCallBack", "网络请求失败:" + t.getMessage());
        String errorMessage = "";
        String code = "";
        //有view的情况
        if (t instanceof SocketTimeoutException) {
            Log.d("test", "onFailure: SocketTimeoutException网络连接超时");
            errorMessage = "网络连接超时";
            code = "501";
        } else if (t instanceof ConnectException || t instanceof HttpException) {
            Log.d("result", "onFailure: ConnectException请确认网络是否连接");
            errorMessage = "链接不上服务器";//链接不上服务器
            code = "404";
        }else if (t instanceof JsonSyntaxException|| t instanceof UnknownHostException) {
            Log.d("result", "onFailure: JsonSyntaxException服务器是否可用");
            errorMessage = "服务器数据异常，请确定服务器是否可用";//服务器不可用
            code = "500";
        } else {
            errorMessage = "服务器开小差了";
            code = "502";
        }
//        if (StringUtils.isNotEmpty(errorMessage)) {
//            ToastUtils.showShort(errorMessage);
//        }
        onFail(code, errorMessage, null);
    }

    public abstract void onSuc(String code, String errMsg, Response<T> response);

    public void onFail(String code, String errMsg, Response<T> response) {}
}
